module gitlab.com/golang-utils/config/v2

go 1.24.0

require (
	github.com/emersion/go-appdir v1.1.2
	github.com/pelletier/go-toml v1.8.1
	github.com/zs5460/art v0.2.0
	gitlab.com/golang-utils/dialog v0.2.1
	gitlab.com/golang-utils/fmtdate v1.0.2
	gitlab.com/golang-utils/fs v0.20.0
	gitlab.com/golang-utils/scaffold v1.15.3
	gitlab.com/golang-utils/version v1.0.4
)

require github.com/mitchellh/colorstring v0.0.0-20190213212951-d06e56a500db // indirect

require (
	github.com/atotto/clipboard v0.1.4 // indirect
	github.com/aymanbagabas/go-osc52/v2 v2.0.1 // indirect
	github.com/charmbracelet/bubbles v0.16.1 // indirect
	github.com/charmbracelet/bubbletea v0.24.2 // indirect
	github.com/charmbracelet/lipgloss v0.7.1 // indirect
	github.com/containerd/console v1.0.4-0.20230706203907-8f6c4e4faef5 // indirect
	github.com/google/uuid v1.3.0 // indirect
	github.com/lucasb-eyer/go-colorful v1.2.0 // indirect
	github.com/mattn/go-isatty v0.0.20 // indirect
	github.com/mattn/go-localereader v0.0.1 // indirect
	github.com/mattn/go-runewidth v0.0.16 // indirect
	github.com/moby/sys/mountinfo v0.7.1 // indirect
	github.com/muesli/ansi v0.0.0-20230316100256-276c6243b2f6 // indirect
	github.com/muesli/cancelreader v0.2.2 // indirect
	github.com/muesli/reflow v0.3.0 // indirect
	github.com/muesli/termenv v0.15.2 // indirect
	github.com/nsf/termbox-go v1.1.1 // indirect
	github.com/rivo/uniseg v0.4.7 // indirect
	github.com/schollz/progressbar/v3 v3.18.0
	gitlab.com/golang-utils/errors v0.0.2 // indirect
	gitlab.com/golang-utils/updatechecker v0.0.9 // indirect
	golang.org/x/sync v0.3.0 // indirect
	golang.org/x/sys v0.29.0 // indirect
	golang.org/x/term v0.28.0 // indirect
	golang.org/x/text v0.11.0 // indirect
)
