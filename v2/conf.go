package config

import (
	"encoding/json"
	"fmt"
	"os"
	"strings"

	"gitlab.com/golang-utils/fs"
	"gitlab.com/golang-utils/fs/path"
	"gitlab.com/golang-utils/scaffold"
)

var templateRaw = `
>>>{{.Progname}}/
>>>VERSION
v0.0.1

<<<VERSION
>>>go.mod
module {{.Progname}}

go 1.22

require (
    gitlab.com/golang-utils/config/v2 v{{.Version}}
)
<<<go.mod
>>>main.go
package main

import (
  _ "embed"
	"fmt"
	"os"
    "gitlab.com/golang-utils/config/v2"
    "{{.Progname}}/lib/{{.Progname}}"
)

//go:embed VERSION
var VERSION string

var (
    cfg = config.New("{{.Progname}}", VERSION, "here comes the description")
    argName = cfg.String("name", "enter your name", config.Shortflag('n'))

    cmd1 = cfg.Command("cmd", "description of command")
    argCmd1 = cmd1.Int("no", "description of number", config.Required())
)

func main() {
    err := run()

    if err != nil {
            fmt.Fprintf(os.Stderr, "ERROR: %v", err)
            os.Exit(1)
    }

    os.Exit(0)
}

func run() error {
    err := cfg.Run()

    if err != nil {
        return err
    }

    switch cfg.ActiveCommand() {
    case cmd1:
        return doCmd1()
    case nil:
		if argName.IsSet() {
			fmt.Printf("Hello, %s!\n", argName.Get())
		} else {
			println({{.Progname}}.New())
		}
    default:
		println(cfg.Usage())
    }

    return nil
}

func doCmd1() error {
	fmt.Fprintf(os.Stdout, "your number is %v\n", argCmd1.Get())
    return nil
}
<<<main.go
>>>lib/
>>>{{.Progname}}/
>>>{{.Progname}}.go
package {{.Progname}}

func New() string {
	return "Hello World"
}
<<<{{.Progname}}.go
<<<{{.Progname}}/
<<<lib/
<<<{{.Progname}}/
`

var propsJson = `
	{
		"Progname": %q,
		"Version": %q
	}
`

func scaffoldCMDLineProg(fsys fs.FS, progname string) error {
	rd := strings.NewReader(fmt.Sprintf(propsJson, progname, ParseVersion(VERSION).String()))
	return scaffold.RunFS(fsys, templateRaw, rd, os.Stdout)
}

func RunScaffold(fsys fs.FS, prog string) (err error) {
	if prog == "" {
		return fmt.Errorf("no program name given for scaffolding")
	}

	//prog := cleanProgname(optionProgram.Get())

	err = ValidateName(prog)

	if err != nil {
		return fmt.Errorf("invalid programname %q", prog)
	}

	prog = strings.ReplaceAll(prog, "-", "_")
	return scaffoldCMDLineProg(fsys, prog)
}

func (cmdConfig *Config) RunLocations() (locjson string, err error) {
	if cmdConfig == nil {
		return "", fmt.Errorf("missing command")
	}
	err = cmdConfig.Load(false)
	if err != nil {
		return "", fmt.Errorf("Can't load options: %s", err.Error())
	}
	locations := map[string][]string{}

	cmdConfig.EachValue(func(name string, value interface{}) {
		locations[name] = cmdConfig.Locations(name)
	})

	cmdConfig.EachCommand(func(cmdname string, ccConf *Config) {
		ccConf.EachValue(func(name string, value interface{}) {
			locations[cmdname+"."+name] = ccConf.Locations(name)
		})
	})

	var bf strings.Builder

	global := cmdConfig.FirstGlobalsFile()
	user := cmdConfig.UserFile()
	local := cmdConfig.LocalFile()

	var left string
	var right string

	for name, locs := range locations {
		left = name + "  "

		var vls string

		for i := len(locs) - 1; i >= 0; i-- {
			l := locs[i]
			switch l {
			case global.String():
				vls += "global > "
			case user.String():
				vls += "user > "
			case local.String():
				vls += "local > "
			default:
				vls += "default " + "(" + l + ")"
			}

		}

		right = vls

		bf.WriteString(pad(left, right) + "\n")
	}

	bf.WriteString("\n")
	return bf.String(), nil
}

func (cmdConfig *Config) RunRm(ty string) error {
	mc := cmdConfig.getMain()
	switch ty {
	case "user":
		mc.fsys.Delete(cmdConfig.UserFile().RootRelative(), false)
	case "local":
		mc.fsys.Delete(cmdConfig.LocalFile().RootRelative(), false)
	case "global":
		mc.fsys.Delete(cmdConfig.FirstGlobalsFile().RootRelative(), false)
	case "all":
		paths := []path.Relative{
			cmdConfig.UserFile().RootRelative(),
			cmdConfig.LocalFile().RootRelative(),
			cmdConfig.FirstGlobalsFile().RootRelative(),
		}

		for _, f := range paths {
			mc.fsys.Delete(f, false)
		}
	case "":
		return fmt.Errorf("location not set. possible values are 'local', 'global' or 'user'")
	default:
		return fmt.Errorf("'%s' is not a valid value for location option. possible values are 'local', 'global' or 'user'", ty)
	}
	return nil
}

func (mc *Config) catFile(file path.Relative) (s string, err error) {
	c, err := fs.ReadFile(mc.fsys, file)
	if err != nil {
		return "", err
	}
	return "-- " + mc.fsys.Abs(file).String() + " --\n" + string(c), nil
}

func (cmdConfig *Config) RunCat(ty string) (outstr string, err error) {
	mc := cmdConfig.getMain()
	switch ty {
	case "user":
		outstr, _ = mc.catFile(cmdConfig.UserFile().RootRelative())
	case "local":
		outstr, _ = mc.catFile(cmdConfig.LocalFile().RootRelative())
	case "global":
		outstr, _ = mc.catFile(cmdConfig.FirstGlobalsFile().RootRelative())
	case "all", "":
		paths := map[string]path.Relative{
			"user":   mc.UserFile().RootRelative(),
			"local":  mc.LocalFile().RootRelative(),
			"global": mc.FirstGlobalsFile().RootRelative(),
		}

		var bd strings.Builder

		for _, f := range paths {
			s, e := mc.catFile(f)

			if e == nil {
				bd.WriteString(s + "\n")
			}
		}

		outstr = bd.String()
	default:
		return "", fmt.Errorf("'%s' is not a valid value for type location. possible values are 'local', 'global' or 'user'", ty)
	}
	return
}

func (cmdConfig *Config) RunPath(ty string) (outstr string, err error) {
	mc := cmdConfig.getMain()
	switch ty {
	case "user":
		outstr = mc.fsys.Abs(mc.UserFile().RootRelative()).String()
	case "local":
		outstr = mc.fsys.Abs(mc.LocalFile().RootRelative()).String()
	case "global":
		outstr = mc.fsys.Abs(mc.FirstGlobalsFile().RootRelative()).String()
	case "all":
		paths := map[string]string{
			"user":   mc.fsys.Abs(mc.UserFile().RootRelative()).String(),
			"local":  mc.fsys.Abs(mc.LocalFile().RootRelative()).String(),
			"global": mc.fsys.Abs(mc.FirstGlobalsFile().RootRelative()).String(),
		}
		b, err := json.Marshal(paths)
		if err != nil {
			return "", fmt.Errorf("Can't print paths: %s", err.Error())
		}

		outstr = string(b)
	default:
		return "", fmt.Errorf("'%s' is not a valid value for location option. possible values are 'local', 'global' or 'user'", ty)
	}
	return
}

func (cmdConfig *Config) RunSet(key, val string, ty string, cmd string) error {
	setter := cmdConfig

	if key == "" {
		return fmt.Errorf("option parameter can't be empty for set")
	}

	if cmd != "" {
		var err error
		setter, err = cmdConfig.GetCommandConfig(cmd)
		if err != nil {
			return err
		}
		_ = setter
	}

	cmdConfig.Reset()

	switch ty {
	case "user":
		if err := cmdConfig.LoadUser(); err != nil {
			return fmt.Errorf("Can't load user config file: %s", err.Error())
		}

		if err := setter.Set(key, val, cmdConfig.UserFile().String()); err != nil {
			return fmt.Errorf("Can't set option %#v to value %#v: %s", key, val, err.Error())
		}
		if err := cmdConfig.SaveToUser(); err != nil {
			return fmt.Errorf("Can't save user config file: %s", err.Error())
		}
	case "local":
		if err := cmdConfig.LoadLocals(); err != nil {
			return fmt.Errorf("Can't load local config file: %s", err.Error())
		}
		if err := setter.Set(key, val, cmdConfig.LocalFile().String()); err != nil {
			return fmt.Errorf("Can't set option %#v to value %#v: %s", key, val, err.Error())
		}
		if err := cmdConfig.SaveToLocal(); err != nil {
			return fmt.Errorf("Can't save local config file: %s", err.Error())
		}
	case "global":
		if err := cmdConfig.LoadGlobals(); err != nil {
			return fmt.Errorf("Can't load global config file: %s", err.Error())
		}
		if err := setter.Set(key, val, cmdConfig.FirstGlobalsFile().String()); err != nil {
			return fmt.Errorf("Can't set option %#v to value %#v: %s", key, val, err.Error())
		}
		if err := cmdConfig.SaveToGlobals(); err != nil {
			return fmt.Errorf("Can't save global config file: %s", err.Error())
		}
	case "":
		return fmt.Errorf("location not set. possible values are 'local', 'global' or 'user'")
	default:
		return fmt.Errorf("'%s' is not a valid value for location option. possible values are 'local', 'global' or 'user'", ty)
	}
	return nil
}

func (cmdConfig *Config) RunUnset(key, ty string, cmd string) error {
	setter := cmdConfig

	if key == "" {
		return fmt.Errorf("option parameter can't be empty for set")
	}

	if cmd != "" {
		var err error
		setter, err = cmdConfig.GetCommandConfig(cmd)
		if err != nil {
			return err
		}
		_ = setter
	}

	cmdConfig.Reset()

	switch ty {
	case "user":
		if err := cmdConfig.LoadUser(); err != nil {
			return fmt.Errorf("Can't load user config file: %s", err.Error())
		}

		if err := setter.unset(key); err != nil {
			return fmt.Errorf("Can't unset option %#v: %s", key, err.Error())
		}

		if err := cmdConfig.SaveToUser(); err != nil {
			return fmt.Errorf("Can't save user config file: %s", err.Error())
		}
	case "local":
		if err := cmdConfig.LoadLocals(); err != nil {
			return fmt.Errorf("Can't load local config file: %s", err.Error())
		}

		if err := setter.unset(key); err != nil {
			return fmt.Errorf("Can't unset option %#v: %s", key, err.Error())
		}

		if err := cmdConfig.SaveToLocal(); err != nil {
			return fmt.Errorf("Can't save local config file: %s", err.Error())
		}
	case "global":
		if err := cmdConfig.LoadGlobals(); err != nil {
			return fmt.Errorf("Can't load global config file: %s", err.Error())
		}

		if err := setter.unset(key); err != nil {
			return fmt.Errorf("Can't unset option %#v: %s", key, err.Error())
		}

		if err := cmdConfig.SaveToGlobals(); err != nil {
			return fmt.Errorf("Can't save global config file: %s", err.Error())
		}
	case "":
		return fmt.Errorf("location not set. possible values are 'local', 'global' or 'user'")
	default:
		return fmt.Errorf("'%s' is not a valid value for location option. possible values are 'local', 'global' or 'user'", ty)
	}
	return nil
}

func (cmdConfig *Config) RunGet(key, cmd string) (out string, err error) {
	err = cmdConfig.Load(false)
	if err != nil {
		return "", fmt.Errorf("Can't load config options: %s", err.Error())
	}

	if cmd != "" {
		cmdConfig, err = cmdConfig.GetCommandConfig(cmd)
		if err != nil {
			return "", err
		}
	}

	if key == "" {
		var vals = map[string]interface{}{}
		cmdConfig.EachValue(func(name string, value interface{}) {
			vals[name] = value
		})
		var b []byte
		b, err = json.Marshal(vals)
		if err != nil {
			return "", fmt.Errorf("Can't print locations: %s", err.Error())
		}

		return string(b), nil
	} else {
		if !cmdConfig.IsOption(key) {
			return "", fmt.Errorf("unknown option %s", key)
		}

		val := cmdConfig.GetValue(key)
		return fmt.Sprintf("%v\n", val), nil
	}

}
