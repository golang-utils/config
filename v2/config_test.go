package config

import (
	"testing"
	"time"

	appdir "github.com/emersion/go-appdir"
	"gitlab.com/golang-utils/fmtdate"
	"gitlab.com/golang-utils/fs"
	"gitlab.com/golang-utils/fs/filesystems/mockfs"
	"gitlab.com/golang-utils/fs/path"
)

func init() {
	CONFIG_EXT = ".tmp"
}

func strPtr(s string) *string {
	return &s
}

func TestSet(t *testing.T) {
	var loc path.Local

	loc = path.MustLocal(`/`)

	fsys, err := mockfs.New(loc)

	if err != nil {
		t.Fatal(err)
	}

	dir := "testerer/"

	cfg, err := NewConfig("testapp", "a testapp", true, Filesystem(fsys), Version("v0.1.0"))
	if err != nil {
		t.Fatal(err.Error())
	}

	cfg.Dir("dir", "a directory test")
	cfg.DateTime("date", "hiho")

	cfg.Set("dir", dir, cfg.LocalFile().String())
	cfg.Set("date", "2024-10-02 17:23:21", cfg.LocalFile().String())

	err = cfg.SaveToLocal()

	if err != nil {
		t.Fatal(err)
	}

	bt, err := fs.ReadFile(fsys, cfg.LocalFile().RootRelative())

	exp := `date = "2024-10-02 17:23:21"
dir = "testerer/"
version = "0.1.0"
`
	got := string(bt)

	if got != exp {
		t.Errorf("expected: \n%q\ngot: \n%q\n", exp, got)
	}

}

func TestGet(t *testing.T) {
	var loc path.Local

	loc = path.MustLocal(`/`)

	fsys, err := mockfs.New(loc)

	if err != nil {
		t.Fatal(err)
	}

	cfg, err := NewConfig("testapp", "a testapp", true, Filesystem(fsys), Version("v0.1.0"))
	if err != nil {
		t.Fatal(err.Error())
	}

	argDir := cfg.Dir("dir", "a directory test")
	argDate := cfg.DateTime("date", "hiho")

	datestr := "2024-10-02 17:23:21"
	dirstr := "testerer/"

	exp := `date = "` + datestr + `"
dir = "` + dirstr + `"
version = "0.1.0"
`

	err = fs.WriteFile(fsys, cfg.LocalFile().RootRelative(), []byte(exp), true)
	if err != nil {
		t.Fatal(err.Error())
	}

	err = cfg.LoadLocals()

	if err != nil {
		t.Fatal(err.Error())
	}

	dte, err := fmtdate.ParseDateTime(datestr)

	if err != nil {
		t.Fatal(err.Error())
	}

	if got := argDate.Get(); got != dte {
		t.Errorf("got date: %v, but expected: %v", got, dte)
	}

	if got := path.Base(argDir.Get()); got != dirstr {
		t.Errorf("got dir: %v, but expected: %v", got, dirstr)
	}
}

func TestConfig(t *testing.T) {
	//	t.Skip()
	var appname = "testapp"

	tests := [...]struct {
		Option                          string
		Help                            string
		Type                            string
		Default                         interface{}
		Required                        bool
		Shortflag                       rune
		ENV                             string
		Global                          string
		Local                           string
		User                            string
		Arg                             string
		Command                         *string
		CommandHelp                     *string
		TopLevelDoubleWithDifferentType string // this is the other type
		expected                        interface{}
	}{
		{ // 0
			Option:   "name",
			Type:     "string",
			Help:     "Test default (subcommand)",
			Default:  "Donald",
			Command:  strPtr("register"),
			expected: "Donald",
		},
		{ // 1
			Option:   "name",
			Type:     "string",
			Help:     "Test default",
			Default:  "Donald",
			expected: "Donald",
		},
		{ // 2
			Option:   "name",
			Type:     "string",
			Help:     "Test global override",
			Default:  "Donald",
			Global:   "Daisy",
			expected: "Daisy",
		},
		{ // 3
			Option:   "name",
			Type:     "string",
			Help:     "Test global override (subcommand)",
			Default:  "Donald",
			Command:  strPtr("register"),
			Global:   "Daisy",
			expected: "Daisy",
		},
		{ // 4
			Option:   "name",
			Type:     "string",
			Help:     "Test user override",
			Default:  "Donald",
			Global:   "Daisy",
			User:     "Mickey",
			expected: "Mickey",
		},
		{ // 5
			Option:   "name",
			Type:     "string",
			Help:     "Test user override (subcommand)",
			Default:  "Donald",
			Command:  strPtr("register"),
			Global:   "Daisy",
			User:     "Mickey",
			expected: "Mickey",
		},
		{
			Option:   "name",
			Type:     "string",
			Help:     "Test local override",
			Default:  "Donald",
			Global:   "Daisy",
			User:     "Mickey",
			Local:    "Minnie",
			expected: "Minnie",
		},
		{
			Option:   "name",
			Type:     "string",
			Help:     "Test local override (subcommand)",
			Default:  "Donald",
			Global:   "Daisy",
			User:     "Mickey",
			Command:  strPtr("register"),
			Local:    "Minnie",
			expected: "Minnie",
		},
		{
			Option:   "name",
			Type:     "string",
			Help:     "Test env override",
			Default:  "Donald",
			Global:   "Daisy",
			User:     "Mickey",
			Local:    "Minnie",
			ENV:      "Batman",
			expected: "Batman",
		},
		{
			Option:   "name",
			Type:     "string",
			Help:     "Test env override (subcommand)",
			Default:  "Donald",
			Global:   "Daisy",
			User:     "Mickey",
			Command:  strPtr("register"),
			Local:    "Minnie",
			ENV:      "Batman",
			expected: "Batman",
		},
		{
			Option:   "name",
			Type:     "string",
			Help:     "Test args override",
			Default:  "Donald",
			Global:   "Daisy",
			User:     "Mickey",
			Local:    "Minnie",
			ENV:      "Batman",
			Arg:      "Superman",
			expected: "Superman",
		},
		{
			Option:   "name",
			Type:     "string",
			Help:     "Test args override (subcommand)",
			Default:  "Donald",
			Global:   "Daisy",
			User:     "Mickey",
			Local:    "Minnie",
			Command:  strPtr("register"),
			ENV:      "Batman",
			Arg:      "Superman",
			expected: "Superman",
		},
		{
			Option:   "age",
			Type:     "int",
			Help:     "Test int",
			Default:  2,
			Local:    "45",
			expected: 45,
		},
		{
			Option:   "height",
			Type:     "float64",
			Help:     "Test float64",
			Default:  1.85,
			Local:    "1.65",
			expected: 1.65,
		},
		{
			Option:   "male",
			Type:     "bool",
			Help:     "Test bool",
			Default:  true,
			Local:    "false",
			expected: false,
		},
		{
			Option:   "nustuff",
			Type:     "string",
			Help:     "Test nustuff",
			Local:    "got it",
			expected: "got it",
		},
		{
			Option:   "xmas",
			Type:     "date",
			Help:     "Test date",
			Local:    "2014-12-24",
			expected: time.Date(2014, 12, 24, 0, 0, 0, 0, time.UTC),
		},
		{
			Option:   "noon",
			Type:     "time",
			Help:     "Test time",
			Local:    "12:00:00",
			expected: time.Date(0, 1, 1, 12, 0, 0, 0, time.UTC),
		},
		{
			Option:   "xmasnoon",
			Type:     "datetime",
			Help:     "Test datetime",
			Local:    "2014-12-24 12:00:00",
			expected: time.Date(2014, 12, 24, 12, 0, 0, 0, time.UTC),
		},
		{
			Option:   "friends",
			Type:     "json",
			Help:     "Test json",
			Local:    `["Ben", "Bob", "Jil"]`,
			expected: `["Ben", "Bob", "Jil"]`,
		},

		{
			Option:   "name",
			Type:     "string",
			Help:     "Test local (no default)",
			Local:    "Donald",
			expected: "Donald",
		},
		{
			Option:   "name",
			Type:     "string",
			Help:     "Test local (no default, required)",
			Required: true,
			Local:    "Donald",
			expected: "Donald",
		},
		{
			Option:   "name",
			Type:     "string",
			Help:     "Test local (no default) (subcommand)",
			Command:  strPtr("register"),
			Local:    "Donald",
			expected: "Donald",
		},
		{
			Option:   "name",
			Type:     "string",
			Help:     "Test local (no default) (subcommand, required)",
			Command:  strPtr("register"),
			Required: true,
			Local:    "Donald",
			expected: "Donald",
		},
		{
			Option:   "name",
			Type:     "string",
			Help:     "Test user (no default)",
			User:     "Donald",
			expected: "Donald",
		},
		{
			Option:   "name",
			Type:     "string",
			Help:     "Test user (no default, required)",
			Required: true,
			User:     "Donald",
			expected: "Donald",
		},
		{
			Option:   "name",
			Type:     "string",
			Help:     "Test user (no default) (subcommand)",
			Command:  strPtr("register"),
			User:     "Donald",
			expected: "Donald",
		},
		{
			Option:   "name",
			Type:     "string",
			Help:     "Test user (no default) (subcommand, required)",
			Command:  strPtr("register"),
			Required: true,
			User:     "Donald",
			expected: "Donald",
		},
		{
			Option:   "name",
			Type:     "string",
			Help:     "Test global (no default)",
			Global:   "Donald",
			expected: "Donald",
		},
		{
			Option:   "name",
			Type:     "string",
			Help:     "Test global (no default, required)",
			Required: true,
			Global:   "Donald",
			expected: "Donald",
		},
		{
			Option:   "name",
			Type:     "string",
			Help:     "Test global (no default) (subcommand)",
			Command:  strPtr("register"),
			Global:   "Donald",
			expected: "Donald",
		},
		{
			Option:   "name",
			Type:     "string",
			Help:     "Test global (no default) (subcommand, required)",
			Command:  strPtr("register"),
			Global:   "Donald",
			Required: true,
			expected: "Donald",
		},
		{
			Option:                          "name",
			Type:                            "string",
			Help:                            "Test global (no default) (subcommand, required)",
			Command:                         strPtr("register"),
			Global:                          "Donald",
			Required:                        true,
			TopLevelDoubleWithDifferentType: "int",
			expected:                        "Donald",
			Shortflag:                       'n',
		},
	}

	var loc path.Local

	loc = path.MustLocal(`/`)

	fsys, err := mockfs.New(loc)

	if err != nil {
		t.Fatal(err)
	}

	for i, test := range tests {
		_ = i

		if i != 2 {
			continue
		}

		var cmd string

		if test.Command != nil {
			cmd = *test.Command
		}

		outercfg, er := NewConfig(appname, "a testapp", true, Filesystem(fsys), Version("v0.1.0"))
		if er != nil {
			t.Error(er.Error())
		}
		setters := []func(*Option){}

		if test.Default != nil {
			setters = append(setters, Default(test.Default))
		}

		if test.Required {
			setters = append(setters, Required())
		}

		if test.Shortflag != 0 {
			setters = append(setters, Shortflag(test.Shortflag))
		}

		var cfg = outercfg

		if test.Command != nil {

			var cmdHelp string
			if test.CommandHelp != nil {
				cmdHelp = *test.CommandHelp
			}

			if test.TopLevelDoubleWithDifferentType != "" {
				mysetters := []func(*Option){}
				if string(test.Shortflag) != "" {
					mysetters = append(mysetters, Shortflag(test.Shortflag))
				}
				cfg.MustNewOption(test.Option, test.TopLevelDoubleWithDifferentType, test.Help, mysetters)
			}

			cfg = cfg.Command(*test.Command, cmdHelp)
		}

		cfg.MustNewOption(test.Option, test.Type, test.Help, setters)
		outercfg.Reset()

		if test.Global != "" {
			if err := cfg.Set(test.Option, test.Global, GLOBAL_DIRS); err != nil {
				t.Fatal(err)
			}
		}

		if err := outercfg.SaveToGlobals(); err != nil {
			t.Fatal(err)
		}

		outercfg.Reset()

		if test.User != "" {
			userDir := appdir.New(appname).UserConfig()
			if err := cfg.Set(test.Option, test.User, userDir); err != nil {
				t.Fatal(err)
			}
		}

		if err := outercfg.SaveToUser(); err != nil {
			t.Fatal(err)
		}

		outercfg.Reset()

		if test.Local != "" {
			if err := cfg.Set(test.Option, test.Local, WORKING_DIR); err != nil {
				t.Fatal(err)
			}
		}

		if err := outercfg.SaveToLocal(); err != nil {
			t.Fatal(err)
		}

		outercfg.Reset()

		got := outercfg.values[test.Option]

		if cmd != "" {
			got = outercfg.commands[cmd].values[test.Option]
		}

		if got != nil {
			t.Errorf("[%v] %#v did not reset properly", i, test.Help)
		}

		var env []string

		if test.ENV != "" {
			envvar := transformToEnvVar2(appname, cmd, test.Option)
			env = []string{envvar + "=" + test.ENV}
		}

		var args []string

		if test.Arg != "" {
			if cmd != "" {
				args = append(args, cmd)
			}
			args = append(args, "--"+test.Option+"="+test.Arg)
		}

		if err := outercfg._load(true, args, env, testRunner(true)); err != nil {
			t.Fatal(err)
		}

		got = outercfg.values[test.Option]

		if cmd != "" {
			got = outercfg.commands[cmd].values[test.Option]
		}

		if got != test.expected {
			t.Errorf("[%v] %#v faied: test[%s] = %s, expected %s", i, test.Help, test.Option, got, test.expected)
		}
	}

}
