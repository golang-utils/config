package main

import (
	"fmt"
	"os"

	"gitlab.com/golang-utils/config/v2"
	"gitlab.com/golang-utils/fmtdate"
	"gitlab.com/golang-utils/fs/path"
)

var (
	cfg = config.New("example-app", "this is an example-app for config", config.AsciiArt("example-app"))

	extra  = cfg.Bool("extra-long", "extra-long is just a first \ntest option as a bool    ", config.Shortflag('x'), config.Default(false))
	second = cfg.String("second", "second is the second option and a string", config.Shortflag('s'), config.Default("2nd"))
	third  = cfg.Int("third", "test of an int", config.Default(200), config.Shortflag('t'))
	date   = cfg.Date("date", "here comes some date", config.Default("2022-12-12"))
	//date = cfg.Date("date", "here comes some date")
	// mydir = cfg.Path("pathx", "here comes the path", config.Default("."))
	mydir = cfg.Dir("dirx", "here comes the dir", config.Default("."))

	myfile = cfg.File("filex", "here comes the file", config.Default("hiho.txt"))
	myurl  = cfg.URL("urlx", "here comes the url", config.Default("http://www.google.com"))

	project = cfg.Command("project_test", "example project sub command")

	defaults = cfg.Command("set-defaults", "write a default configuration into the _config subdir")

	projectName  = project.String("name", "name of the project", config.Required())
	projectExtra = project.Int("extra-long", "extralong in project", config.Shortflag('x'), config.Required())
)

func main() {
	//	println("using config " + config.VERSION)

	err := cfg.Run()

	if err != nil {
		fmt.Fprintf(os.Stderr, "ERROR: %s\n", err)
		os.Exit(1)
	}

	loc := mydir.Get()

	fmt.Printf("dir is: %s (%s)\n", loc, path.ToSystem(loc))

	fl := myfile.Get()
	fmt.Printf("file is: %s (%s)\n", fl, path.ToSystem(fl))

	url := myurl.Get()
	fmt.Printf("url is: %s (%s)\n", url, url.String())

	fmt.Printf("date is: %s\n", fmtdate.FormatDate(date.Get()))

	if extra.Get() {
		fmt.Println("extra-long is true")
	} else {
		if !extra.IsSet() {
			fmt.Println("extra-long has not been set")
		} else {
			fmt.Println("extra is false")
		}
	}

	fmt.Printf("extra-long locations: %#v\n", cfg.Locations("extra-long"))

	fmt.Printf("second is: %#v\n", second.Get())
	fmt.Printf("second locations: %#v\n", cfg.Locations("second"))

	fmt.Printf("third is set to: %v\n", third.Get())
	fmt.Printf("third locations: %#v\n", cfg.Locations("third"))

	cmd := cfg.ActiveCommand()

	if cmd == nil {
		fmt.Println("no subcommand")
		return
	}
	switch cmd {
	case project:
		fmt.Printf("third still is: %v\n", third.Get())
		fmt.Printf("extra-long is: %v\n", projectExtra.Get())
		fmt.Println("project_test subcommand")
		fmt.Printf("project_test name is: %#v\n", projectName.Get())
		fmt.Printf("project_test locations: %#v\n", project.Locations("name"))
	case defaults:
		fmt.Println("setting defaults")
		opts := map[string]string{
			"name": "Harry",
		}

		err = project.SetLocalOptions(opts)
		if err != nil {
			fmt.Printf("ERROR: %v\n", err.Error())
		}
	default:
		panic("must not happen")
	}
}
