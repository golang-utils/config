package config

import (
	"os"

	appdir "github.com/emersion/go-appdir"
	"gitlab.com/golang-utils/fs/path"
)

var (
	CONFIG_DIR  string
	GLOBAL_DIRS string // colon separated list to look for
	WORKING_DIR string
	CONFIG_EXT  = ".conf"
)

func getEnv() []string {
	return os.Environ()
}

// globalsFile returns the global config file path for the given dir
func (c *Config) globalsFile(dir string) path.Local {
	loc := path.MustLocal(dir + "/")
	return loc.Join(c.appName()+"/", c.appName()+CONFIG_EXT)
}

// UserFile returns the user defined config file path
func (c *Config) UserFile() path.Local {
	loc := path.MustLocal(appdir.New(c.appName()).UserConfig() + "/")
	return loc.Join(c.appName() + CONFIG_EXT)
}

// LocalFile returns the local config file (inside the .config subdir of the current working dir)
func (c *Config) LocalFile() path.Local {
	loc := path.MustWD()
	return loc.Join(CONFIG_DIR+"/", c.appName()+"/", c.appName()+CONFIG_EXT)
}

// GlobalFile returns the path for the global config file in the first global directory
func (c *Config) FirstGlobalsFile() path.Local {
	return c.globalsFile(splitGlobals()[0])
}
