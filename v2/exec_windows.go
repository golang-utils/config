//go:build windows
// +build windows

package config

import (
	"os/exec"
)

func execCommand(c string) *exec.Cmd {
	return exec.Command("cmd.exe", "/C", c)
}

func OnWindows() bool {
	return true
}
