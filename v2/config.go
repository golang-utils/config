package config

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"net/url"
	"os"
	"os/exec"
	"runtime"
	"strings"
	"time"

	toml "github.com/pelletier/go-toml"

	"github.com/zs5460/art"
	"gitlab.com/golang-utils/fmtdate"
	"gitlab.com/golang-utils/fs"
	"gitlab.com/golang-utils/fs/filesystems/rootfs"
	"gitlab.com/golang-utils/fs/path"
	"gitlab.com/golang-utils/version"
)

/*
TODO

- enable last argument savgin können nicht gespeichert werden (option does not exist), beim setzen auf leeren string
  ist die config-datei dann nicht mehr valide
- es gibt auch ein problem beim speichern eines datums, dort wird das datumsformat nicht berücksichtigt
  und immer voller zeit gespeichert
- also fix the setting of multiple values (array of values)
  it should not be done like in the standard flags libs, but instead with a separation of
  values with #
  []int     =>   1#2#3#4
  []float64 =>   0.3#1.23#23.34
  []string  =>   Hello,World
  []date    =>   2012.12.12#2011.11.24
- we also need something like option select (from a range of options)
	or more select[getter] where getter must not be an array or a select or a multiselect
	- select int
	- select float
	- select string
	- select date
- and multiselect with the same things

- reorganize in a way, that we separate the Config object from the Command object:
	- there is the default Command that does not have a name
	- and there are the named Commands but they all work like the default command
	- everything that does not belong to a Command is in the Config (which probably
	  should be renamed to App)
	- for the config files, they probably go into a File object
	- and the Env probably goes into an Env object


*/

/*
TODO make more tests
TODO improve cmdline utility
TODO improve documentation
TODO add custom types (maybe we don't need the custom validators then)
for the custom type we could make type an interface (with marshall json etc)
like

type Getter[T] interface {
	Get() T
}

type Type[T] interface {
	// maybe not necessesary
	MarshallJSON() error
	UnmarshallJSON() error
	// TODO: maybe a TOML serialization/unserialization or we just use json there

	// This is important
	Parse(string) error

	// and this is important
	Getter() Getter[T]

	// maybe not neccessary
	String() string
}

then we could also offer Strings, Numbers, Floats, Bools, Dates, Times as parameters
and a CustomDate which has a format string which it expects
*/

type Config struct {
	helpIntro string
	app       string
	version   *version.Version
	spec      map[string]*Option
	values    map[string]interface{}
	locations map[string][]string

	// maps shortflag to option
	shortflags    map[string]string
	commands      map[string]*Config
	commandOrder  []string
	argsOrder     []string
	activeCommand *Config

	// only for subcommands
	skippedOptions map[string]bool
	relaxedOptions map[string]bool
	parent         *Config
	isMainConfig   bool

	cmdVersion         *Config
	cmdVersionArgShort BoolGetter

	cmdHelp           *Config
	cmdHelpArgCommand StringGetter

	cmdMan *Config

	cmdConfig                *Config
	cmdConfigArgAction       StringGetter
	cmdConfigArgCmd          StringGetter
	cmdConfigArgPathType     StringGetter
	cmdConfigArgOption       StringGetter
	cmdConfigArgValue        StringGetter
	cmdConfigArgScaffoldProg StringGetter
	cmdConfigArgScaffoldDir  StringGetter

	fsys fs.FS

	asciiArt string

	mango struct {
		concept       string
		bugs          string
		files         string
		authors       []string
		reportingBugs string
		history       string
		example       string
		copyright     string
		license       string
		documentation string
	}
}

type ConfigOption func(c *Config)

// Version set the version explicit.
// Otherwise the build version is taken from the VCS.
func Version(v string) ConfigOption {
	vv := ParseVersion(v)
	return func(c *Config) {
		c.version = vv
	}
}

func Filesystem(fsys fs.FS) ConfigOption {
	return func(c *Config) {
		c.fsys = fsys
	}
}

func NoAsciiArt() ConfigOption {
	return func(c *Config) {
		c.asciiArt = ""
	}
}

func AsciiArt(s string) ConfigOption {
	return func(c *Config) {
		c.asciiArt = s
	}
}

func Example(text string) ConfigOption {
	return func(c *Config) {
		c.mango.example = text
	}
}

func Copyright(text string) ConfigOption {
	return func(c *Config) {
		c.mango.copyright = text
	}
}

func License(text string) ConfigOption {
	return func(c *Config) {
		c.mango.license = text
	}
}

func Concept(text string) ConfigOption {
	return func(c *Config) {
		c.mango.concept = text
	}
}

func Bugs(text string) ConfigOption {
	return func(c *Config) {
		c.mango.bugs = text
	}
}

func Authors(authors ...string) ConfigOption {
	return func(c *Config) {
		c.mango.authors = authors
	}
}

func Documentation(text string) ConfigOption {
	return func(c *Config) {
		c.mango.documentation = text
	}
}

func ReportingBugs(text string) ConfigOption {
	return func(c *Config) {
		c.mango.reportingBugs = text
	}
}

func History(text string) ConfigOption {
	return func(c *Config) {
		c.mango.history = text
	}
}

// New is like NewConfig, but panics on errors.
func New(appname string, helpIntro string, opts ...ConfigOption) (c *Config) {
	c, err := NewConfig(appname, helpIntro, true)
	if err != nil {
		panic(err)
	}

	return c
}

// NewConfig creates a new *Config for the given appname and options
// It returns and error, if the appname does not conform to the naming rules.
//   - just lowercase letters are allowed
//   - it must have at least two characters
//   - it must not start with numbers
//   - it must not start or end with an underscore or a dash
//   - it must not have an underscore followed by an underscore or a dash
//   - it must not have a dash followed by a dash or an underscore
//
// For the main config mainConfig must be set to true
func NewConfig(app string, helpIntro string, mainConfig bool, opts ...ConfigOption) (c *Config, err error) {
	if err = ValidateName(app); err != nil {
		err = ErrInvalidAppName(app)
		return
	}

	c = &Config{}
	c.spec = map[string]*Option{}
	c.commands = map[string]*Config{}
	c.app = app
	c.version = version.BuildVersion()
	c.shortflags = map[string]string{}
	c.helpIntro = helpIntro
	c.isMainConfig = mainConfig
	if c.isMainConfig {
		c.asciiArt = app
	}
	c.Reset()

	for _, opt := range opts {
		opt(c)
	}

	if c.fsys == nil {
		c.fsys, err = rootfs.New()

		if err != nil {
			panic(err)
		}
	}

	return
}

func (c *Config) addToArgsOrder(argName string) {
	c.argsOrder = append(c.argsOrder, argName)
}

func (c *Config) EachSpec(fn func(name string, opt *Option)) {
	for k, opt := range c.spec {
		fn(k, opt)
	}
}

func (c *Config) EachValue(fn func(name string, val interface{})) {
	for k, val := range c.values {
		if k == "" {
			return
		}
		fn(k, val)
	}
}

func (c *Config) EachCommand(fn func(name string, val *Config)) {
	for k, val := range c.commands {
		fn(k, val)
	}
}

func (c *Config) SpecTree() map[string]*Option {
	stree := map[string]*Option{}

	for k, v := range c.spec {
		stree[k] = v
	}

	for n, cmd := range c.commands {
		for kk, vv := range cmd.spec {
			stree[n+"."+kk] = vv
		}
	}

	return stree
}

func (c *Config) lastArgName() string {
	if last, has := c.spec[""]; has {
		return last.LastArgName
	}
	return ""
}

// Skip skips all options of the parent command but the given
func (c *Config) SkipAllBut(except ...string) *Config {
	if !c.isCommand() {
		panic("can only Skip in subcommands")
	}

	ex := map[string]bool{}
	parentLastArgName := c.parent.lastArgName()

	for _, x := range except {
		if x != "" && x == parentLastArgName {
			ex[""] = true
		} else {
			ex[x] = true
		}
	}

	for k := range c.parent.spec {
		if ex[k] {
			continue
		}

		c.skippedOptions[k] = true
	}

	return c
}

func (c *Config) getMain() *Config {
	if c.isMainConfig {
		return c
	}

	return c.parent.getMain()
}

// Skip skips the given option of the parent command and is chainable
// It panics, if the given option is not a parent option of if the
// current config is no subcommand
func (c *Config) Skip(option string) *Config {
	if !c.isCommand() {
		panic("can only Skip in subcommands")
	}

	opt := option

	if option == c.parent.lastArgName() {
		opt = ""
	}

	c.skippedOptions[opt] = true
	return c
}

func (c *Config) Relax(option string) *Config {
	if !c.isCommand() {
		panic("can only Relax in subcommands")
	}

	opt := option

	if option == c.parent.lastArgName() {
		opt = ""
	}

	c.relaxedOptions[opt] = true
	return c
}

func (c *Config) GetCommandConfig(name string) (s *Config, err error) {
	var has bool
	s, has = c.commands[name]
	if !has {
		return nil, fmt.Errorf("no such command %q", name)
	}
	return s, nil
}

// Command returns a *Config for a command and panics on errors
func (c *Config) Command(name string, helpIntro string, opts ...ConfigOption) *Config {
	s, err := c.makeCommand(name, helpIntro, opts...)
	if err != nil {
		panic(err)
	}
	return s
}

// Sub returns a *Config for a subcommand.
// If name does not match to NameRegExp, an error is returned
func (c *Config) makeCommand(name string, helpIntro string, opts ...ConfigOption) (s *Config, err error) {
	if c.isCommand() {
		err = ErrSubCommand
		return
	}
	s, err = NewConfig(name, helpIntro, false, Version(c.version.String()))
	if err != nil {
		return
	}
	for _, opt := range opts {
		opt(s)
	}
	s.skippedOptions = map[string]bool{}
	s.relaxedOptions = map[string]bool{}

	s.parent = c
	c.commands[name] = s
	c.commandOrder = append(c.commandOrder, name)

	return s, nil
}

// addOption adds the given option, validates it and returns any error
func (c *Config) addOption(opt *Option) error {
	if opt.Name != "" {
		if err := ValidateName(opt.Name); err != nil {
			return ErrInvalidOptionName(opt.Name)
		}
	}

	if _, has := c.spec[opt.Name]; has {
		return ErrDoubleOption(opt.flagName())
	}
	c.spec[opt.Name] = opt
	if opt.Shortflag != "" {
		if _, has := c.shortflags[opt.Shortflag]; has {
			return ErrDoubleShortflag(opt.Shortflag)
		}
		c.shortflags[opt.Shortflag] = opt.Name
	}
	return nil
}

// Reset cleans the values, the locations and any current subcommand
func (c *Config) Reset() {
	//fmt.Println("reset called")
	c.values = map[string]interface{}{}
	//c.lastArg = nil
	c.locations = map[string][]string{}
	c.activeCommand = nil

	for _, cmd := range c.commands {
		cmd.Reset()
	}

}

// Location returns the locations where the option was set in the order of setting.
//
// The locations are tracked differently:
// - defaults are tracked by their %v printed value
// - environment variables are tracked by their name
// - config files are tracked by their path
// - cli args are tracked by their name
// - settings via Set() are tracked by the given location or the caller if that is empty
func (c *Config) Locations(option string) []string {
	if option == c.lastArgName() || option == "" {
		return nil
	}
	if err := ValidateName(option); err != nil {
		panic(InvalidNameError(option))
	}
	return c.locations[option]
}

// IsOption returns true, if the given option is allowed
func (c *Config) IsOption(option string) bool {
	if option == c.lastArgName() {
		option = ""
	}

	if option != "" {
		if err := ValidateName(option); err != nil {
			return false
		}
	}
	_, has := c.spec[option]
	return has
}

func (c *Config) setLastArg(arg string) error {
	spec, has := c.spec[""]

	if !has {
		if c.isCommand() {
			if c.skippedOptions[""] {
				return UnknownOptionError{c.version, ""}
			}
			if _, has := c.parent.spec[""]; has {
				return nil
			} else {
				return UnknownOptionError{c.version, ""}
			}
		} else {
			return UnknownOptionError{c.version, ""}
		}
	}

	if c.values[""] != nil {
		return fmt.Errorf("last argument already set to %#v", c.values[""])
	}

	out, err := stringToValue(spec.Type, arg)

	if err != nil {
		return InvalidValueError{*spec, arg}
	}

	if str, ok := out.(fmt.Stringer); ok {
		out = str.String()
	}

	c.values[""] = out

	return nil
}

// set sets the option to the value and validates the value returning any errors
func (c *Config) set(option string, value string, location string) error {
	//fmt.Printf("set called in command %q with option %q and value %q\n", c.app, option, value)
	if err := ValidateName(option); err != nil {
		return InvalidNameError(option)
	}
	spec, has := c.spec[option]

	if !has {
		return UnknownOptionError{c.version, option}
	}

	out, err := stringToValue(spec.Type, value)

	if err != nil {
		return InvalidValueError{*spec, value}
	}

	if str, ok := out.(fmt.Stringer); ok {
		out = str.String()
	}

	//fmt.Printf("opt: %q => %v\n", option, out)
	c.values[option] = out
	c.locations[option] = append(c.locations[option], location)
	return nil
}

// unset unsets the option
func (c *Config) unset(option string) error {
	//fmt.Printf("set called in command %q with option %q and value %q\n", c.app, option, value)
	if err := ValidateName(option); err != nil {
		return InvalidNameError(option)
	}
	spec, has := c.spec[option]

	_ = spec

	if !has {
		return UnknownOptionError{c.version, option}
	}

	delete(c.values, option)
	return nil
}

// Set sets the option to the value. Location is a hint from where the
// option setting was triggered. If the location is empty, the caller file
// and line is tracked as location.
func (c *Config) Set(option string, val string, location string) error {
	if location == "" {
		_, file, line, _ := runtime.Caller(0)
		location = fmt.Sprintf("%s:%d", file, line)
	}
	return c.set(option, val, location)
}

// setMap sets the given options and tracks the calling function as
// location
func (c *Config) setMap(options map[string]string) error {
	_, file, line, _ := runtime.Caller(1)
	location := fmt.Sprintf("%s:%d", file, line)

	for opt, val := range options {
		err := c.set(opt, val, location)
		if err != nil {
			return err
		}
	}
	return nil
}

// IsSet returns true, if the given option is set and false if not.
func (c Config) IsSet(option string) bool {
	if option != "" {
		if err := ValidateName(option); err != nil {
			panic(InvalidNameError(option))
		}
	}
	_, has := c.values[option]
	return has
}

// CheckMissing checks if mandatory values are missing inside the values map
// CheckMissing stops on the first error
func (c *Config) CheckMissing() error {
	empty := map[string]bool{}
	return c.checkMissing(empty, empty)
}

// CheckMissing checks if mandatory values are missing inside the values map
// CheckMissing stops on the first error
func (c *Config) checkMissing(skippedOptions map[string]bool, relaxedOptions map[string]bool) error {
	for k, spec := range c.spec {
		if spec != nil && spec.Required && spec.Default == nil {
			if _, has := skippedOptions[k]; has {
				continue
			}
			if _, has := relaxedOptions[k]; has {
				continue
			}

			if _, has := c.values[k]; !has {
				return MissingOptionError{c.version, *spec}
			}
		}
	}
	return nil
}

// ValidateValues validates only values that are set and not nil.
// It does not check for missing mandatory values (use CheckMissing for that)
// ValidateValues stops on the first error
func (c *Config) ValidateValues() error {
	for k, v := range c.values {
		if v == nil {
			continue
		}
		spec, has := c.spec[k]
		if !has {
			return UnknownOptionError{c.version, k}
		}
		if err := spec.ValidateValue(v); err != nil {
			return InvalidConfig{c.version, err}
		}
	}
	return nil
}

// CurrentSub returns the active command
func (c *Config) ActiveCommand() (s *Config) {
	return c.activeCommand
}

// isCommand checks if the *Config relongs to a subcommand
func (c *Config) isCommand() bool {
	return !c.isMainConfig
}

// MarshalJSON serializes the spec to JSON
func (c *Config) MarshalJSON() ([]byte, error) {
	stree := c.SpecTree()
	return json.Marshal(stree)
}

// UnmarshalJSON deserializes the spec from JSON
func (c *Config) UnmarshalJSON(data []byte) error {
	stree := map[string]*Option{}

	err := json.Unmarshal(data, &stree)

	if err != nil {
		return err
	}

	c.spec = map[string]*Option{}

	for k, v := range stree {
		ks := strings.Split(k, ".")
		switch len(ks) {
		case 1:
			c.spec[k] = v
		case 2:
			sub, has := c.commands[ks[0]]
			if !has {
				sub, err = c.makeCommand(ks[0], "")
				if err != nil {
					return fmt.Errorf("can't unmarshall command %q: %v", ks[0], err)
				}
			}
			sub.spec[ks[1]] = v
		default:
			return fmt.Errorf("invalid key for unmarshalling (contains more than one dot: %q", k)
		}

	}

	return nil
}

// appName returns the name of the app
func (c *Config) appName() string {
	if c.isCommand() {
		return c.parent.app
	}
	return c.app
}

func (c *Config) CommmandName() string {
	return c.commandName()
}

// commandName returns the name of the subcommand and the empty string, if there is no subcommand, the empty string is returned
func (c *Config) commandName() string {
	if c.isCommand() {
		return c.app
	}
	return ""
}

// Binary returns the path to the binary of the app
func (c *Config) Binary() (path string, err error) {
	return exec.LookPath(c.appName())
}

func (c *Config) Merge(rd io.Reader, location path.Relative) error {

	//fmt.Printf("merging values into %#v\n", c.values)

	wrapErr := func(err error) error {
		return InvalidConfigFileError{location.String(), c.version, err}
	}

	t, err := toml.LoadReader(rd)

	if err != nil {
		return wrapErr(err)
	}

	// TODO check, if it is still correct, to get it via Get?
	version := fmt.Sprintf("%v", t.Get("version"))

	differentVersions := version != c.version.String()

	setValue := func(subcommand string, key string, val string) error {
		if subcommand == "" {
			return c.set(key, val, location.String())
		} else {
			sub, has := c.commands[subcommand]
			if !has {
				return errors.New("unknown subcommand " + subcommand)
			} else {
				err = sub.set(key, val, location.String())
			}

			if err != nil {

				if differentVersions {
					return wrapErr(fmt.Errorf("value %#v of option %s, present in config for version %s is not valid for running version %s",
						val, key, version, c.version))
				} else {
					return wrapErr(err)
				}
			}
		}
		return nil
	}

	var getValString = func(ttt *toml.Tree, pth string) string {
		v := ttt.Get(pth)

		switch x := v.(type) {
		case time.Time:
			return x.Format(DateTimeGeneral)
		default:
			return fmt.Sprintf("%v", v)
		}

	}

	for _, k := range t.Keys() {
		if k == "version" {
			continue
		}

		vvvv := t.Get(k)

		if tt, isTree := vvvv.(*toml.Tree); isTree {
			for _, kk := range tt.Keys() {
				val := getValString(tt, kk)
				err = setValue(k, kk, val)
				if err != nil {
					return wrapErr(fmt.Errorf("can't set %q to %q for subcommand %q: %v", kk, val, k, err))
				}
			}
		} else {
			val := getValString(t, k)
			err = setValue("", k, val)
			if err != nil {
				return wrapErr(fmt.Errorf("can't set %q to %q: %v", k, val, err))
			}
		}
	}

	return nil
}

func (c *Config) MergeEnv2() error {
	return c._mergeEnv(getEnv())
}

func (c *Config) _mergeEnv(env []string) error {
	for _, pair := range env {
		if isEnvVar2(pair, c.app) {
			startVal := strings.Index(pair, "=")
			key, val := pair[0:startVal], pair[startVal+1:]
			val = strings.TrimSpace(val)

			if val == "" {
				return EmptyValueError(key)
			}

			app, cmd, option, err := transformEnvToNames2(key)

			// some variable not set by use
			if err != nil {
				continue
			}

			if app != c.app {
				continue
			}

			if cmd == "" {
				err := c.set(option, val, pair[:startVal])
				if err != nil {
					return InvalidConfigEnv{c.version, pair[:startVal], err}
				}
			} else {
				cconf, err := c.GetCommandConfig(cmd)
				if err != nil {
					return InvalidConfigEnv{c.version, pair[:startVal], fmt.Errorf("command not found: %q", cmd)}
				}

				err = cconf.set(option, val, pair[:startVal])
				if err != nil {
					return InvalidConfigEnv{c.version, pair[:startVal], err}
				}
			}

		}
	}
	return nil
}

// MergeArgs merges the os.Args into the config
// args like --a-key='a val' will correspond to the config value
// A_KEY=a val
// If the key is CONFIG_SPEC, MergeArgs will print the config spec as json
// and exit the program
// If any error happens the error will be printed to os.StdErr and the program exists will
// status code 1
// exiting the program. also if --config_spec is set the spec is directly written to the
// StdOut and the program is exiting. If --help is set, the help message is printed with the
// the help  messages for the config options. If --version is set, the version of the running app is returned
func (c *Config) MergeArgs() error {
	return c.mergeArgs(inputArgs())
}

func inputArgs() []string {
	return os.Args[1:]
}

func (c *Config) mergeArgs(args []string) error {
	empty := map[string]bool{}
	skipped := empty
	relaxed := empty
	if c.isCommand() {
		skipped = c.skippedOptions
		relaxed = c.relaxedOptions
	}
	_, err := c._mergeArgs(false, args, skipped, relaxed)
	return err
}

// RunArgs runs the config by just respecting the given args
func (c *Config) RunArgs(args ...string) error {
	c.prepareLoad()
	return c._loadArgs(args, nil, false, testRunner(false))
}

// RunArgsSilent runs the config by just respecting the given args and don't print
// anything for defaults
func (c *Config) RunArgsSilent(args ...string) error {
	c.prepareLoad()
	return c._loadArgs(args, nil, false, testRunner(true))
}

func (c *Config) usageOptionsArgLeft(optName string, addGeneral bool, skipped map[string]bool, relaxed map[string]bool) string {
	opt := c.spec[optName]

	if _, has := skipped[optName]; has {
		return ""
	}

	var left bytes.Buffer
	if _, has := relaxed[optName]; has || !opt.Required {
		if optName != "" {
			left.WriteString("[")
		}
	}

	if opt.Shortflag != "" {
		left.WriteString("-" + opt.Shortflag + ", ")
	}

	left.WriteString(opt.flagName())
	if opt.Default != nil {

		switch opt.Type {
		case "string":
			left.WriteString(fmt.Sprintf("='%s'", opt.Default))
		case "bool":
			if opt.Default.(bool) {
				left.WriteString("=true")
			} else {
				left.WriteString("=false")
			}
		case "json":
			left.WriteString(fmt.Sprintf("='%s'", opt.Default))
		case "time":
			defTime := convertToTime(opt.Name, opt.Default, "hh:mm:ss")
			left.WriteString(fmt.Sprintf("='%s'", fmtdate.Format("hh:mm:ss", defTime)))
		case "date":
			defDate := convertToTime(opt.Name, opt.Default, "YYYY-MM-DD")
			left.WriteString(fmt.Sprintf("='%s'", fmtdate.Format("YYYY-MM-DD", defDate)))
		case "datetime":
			defDateTime := convertToTime(opt.Name, opt.Default, "YYYY-MM-DD hh:mm:ss")
			left.WriteString(fmt.Sprintf("='%s'", fmtdate.Format("YYYY-MM-DD hh:mm:ss", defDateTime)))
		default:
			left.WriteString(fmt.Sprintf("=%v", opt.Default))

		}

	} else {
		if optName != "" {
			if opt.Type != "bool" {
				left.WriteString(fmt.Sprintf("=%s", convertOpttype(opt.Type)))
			}
		}
	}

	if _, has := relaxed[optName]; has || !opt.Required {
		if optName != "" {
			left.WriteString("]")
		}
	}

	return left.String()
}

func convertToTime(arg string, input any, format string) time.Time {
	switch v := input.(type) {
	case string:
		t, err := fmtdate.Parse(format, v)
		if err != nil {
			panic(fmt.Sprintf("invalid default for arg %s: %v", arg, input))
		}
		return t
	case time.Time:
		return v
	default:
		panic(fmt.Sprintf("invalid default for arg %s: %v", arg, input))
	}
}

func (c *Config) finalArgs() map[string]*Option {
	if !c.isCommand() {
		return c._finalArgs(true, map[string]bool{}, map[string]bool{})
	}

	parentArgs := c.parent._finalArgs(false, c.skippedOptions, c.relaxedOptions)
	my := c._finalArgs(false, map[string]bool{}, map[string]bool{})
	for k, m := range my {
		parentArgs[k] = m
	}
	return parentArgs
}

func (c *Config) _finalArgs(addGeneral bool, skipped map[string]bool, relaxed map[string]bool) map[string]*Option {

	var finalopts = map[string]*Option{}

	for _, optName := range c.argsOrder {

		opt := c.spec[optName]

		if _, has := skipped[optName]; has {
			continue
		}

		var newopt Option
		newopt.Name = opt.Name
		newopt.Default = opt.Default
		newopt.Help = opt.Help
		newopt.LastArgName = opt.LastArgName
		newopt.Required = opt.Required
		newopt.Shortflag = opt.Shortflag
		newopt.Type = opt.Type

		if _, has := relaxed[optName]; has || !opt.Required {
			newopt.Required = false
		}
		finalopts[optName] = &newopt
	}
	return finalopts

}

func (c *Config) usageOptions(addGeneral bool, skipped map[string]bool, relaxed map[string]bool) string {
	var optBf bytes.Buffer

	for _, optName := range c.argsOrder {

		opt := c.spec[optName]

		if _, has := skipped[optName]; has {
			continue
		}

		leftStr := c.usageOptionsArgLeft(optName, addGeneral, skipped, relaxed)
		optBf.WriteString("\n")
		optBf.WriteString(pad("  "+leftStr, opt.Help))
		optBf.WriteString("\n")
	}

	return optBf.String()
}

func (c *Config) lastArgHelpString(required bool) (lastArg string) {
	if sp, has := c.spec[""]; has {
		return sp.flagName()
	}
	return ""
}

func (c *Config) synopsis() string {
	var lastArg string

	options := c.UsageOptions()

	var optstr string

	if options != "" {
		optstr = " OPTION... "
	}

	if sp, has := c.spec[""]; has {
		lastArg = c.lastArgHelpString(sp.Required)
	}

	if c.isCommand() {
		return fmt.Sprintf(`%s %s%s%s`, c.appName(), c.commandName(), optstr, lastArg)
	}

	var cmdStr = ""

	if len(c.commands) > 0 {
		cmdStr = " [command]"
	}
	return fmt.Sprintf(`%s%s%s%s`, c.appName(), cmdStr, optstr, lastArg)
}

func (c *Config) argSyntax(optName string) string {
	if !c.isCommand() {
		return c.usageOptionsArgLeft(optName, true, map[string]bool{}, map[string]bool{})
	}

	_, has := c.spec[optName]

	if has {
		return c.usageOptionsArgLeft(optName, false, map[string]bool{}, map[string]bool{})
	}

	return c.parent.usageOptionsArgLeft(optName, false, c.skippedOptions, c.relaxedOptions)
}

func (c *Config) UsageOptions() string {
	if !c.isCommand() {
		return c.usageOptions(true, map[string]bool{}, map[string]bool{})
	}

	parentOpts := c.parent.usageOptions(false, c.skippedOptions, c.relaxedOptions)
	return c.usageOptions(false, map[string]bool{}, map[string]bool{}) + parentOpts
}

func (c *Config) Usage() string {
	/*
			usage: git [--version] [--help] [-C <path>] [-c name=value]
		           [--exec-path[=<path>]] [--html-path] [--man-path] [--info-path]
		           [-p|--paginate|--no-pager] [--no-replace-objects] [--bare]
		           [--git-dir=<path>] [--work-tree=<path>] [--namespace=<name>]
		           <command> [<args>]
	*/

	var commands string
	var options = c.UsageOptions()
	var helpintro string

	var lastArg string

	if sp, has := c.spec[""]; has {
		lastArg = c.lastArgHelpString(sp.Required)
	}

	if !c.isCommand() {

		if c.asciiArt != "" {
			helpintro = art.String(c.asciiArt)
		}

		helpintro = helpintro + "\n" + c.appName() + " v" + c.version.String() + "\n  "
		helpintro = helpintro + c.helpIntro
	} else {

		if lastArg == "" && !c.skippedOptions[""] {
			var required bool
			if sp, has := c.parent.spec[""]; has {
				required = sp.Required
				if c.relaxedOptions[""] {
					required = false
				}
				lastArg = c.parent.lastArgHelpString(required)
			}
		}
	}

	if c.isCommand() {
		if options == "" {
			return fmt.Sprintf(`
%s %s
  %s

usage: 
  %s %s %s

`, c.appName(), c.commandName(), c.helpIntro, c.appName(), c.commandName(), lastArg)
		}
		return fmt.Sprintf(`
%s %s
  %s

usage: 
  %s %s OPTION... %s

options:%s
`, c.appName(), c.commandName(), c.helpIntro, c.appName(), c.commandName(), lastArg, options)
	}

	var cmdStr string
	var generalStr string
	var usageSubs = c.usageSubcommands()

	/*
		generalcommand := map[string]string{
			"config-spec":      "prints the specification of the configurable options",
			"config-locations": "prints the locations of current configuration",
			"config-files":     "prints the locations of the config files",
		}

		for subCname, subHelp := range generalcommand {
			subcBf.WriteString(pad("  "+subCname, subHelp) + "\n")
		}
	*/
	if len(c.commands) > 0 {

		commands = "commands:\n" + usageSubs + "\nfor help about a specific command, run " +
			fmt.Sprintf("\n  %s help <command>", c.appName())
		cmdStr = " [command]"
		generalStr = ""
	}

	return fmt.Sprintf(`%s

usage: 
  %s%s OPTION... %s

%soptions:%s

%s
`, helpintro, c.appName(), cmdStr, lastArg, generalStr, options, commands)
}

func (c *Config) usageSubcommands() string {

	/*
		first we want to print the apps really specific subcommands in the time of arrival
		then we put help at the beginning,
		then version at the bottom and after it config and at the end scaffold
		or help at the end (should be maybe better)
	*/

	var cmds []*Config

	for _, cname := range c.commandOrder {
		subC := c.commands[cname]
		cmds = append(cmds, subC)
	}

	var subcBf bytes.Buffer

	for _, subC := range cmds {
		subcBf.WriteString(pad("  "+subC.CommmandName(), subC.helpIntro) + "\n")
		subcBf.WriteString("\n")
	}

	return subcBf.String()
}

func (c *Config) env_var2(optName string) string {
	if c.isMainConfig {
		return transformToEnvVar2(c.app, "", optName)
	} else {
		return transformToEnvVar2(c.parent.app, c.app, optName)
	}
}

func (c *Config) envVars() []string {
	v := []string{}
	for k := range c.spec {
		if k == "" {
			continue
		}
		v = append(v, c.env_var2(k))
	}
	return v
}

func (c *Config) _mergeArgs(ignoreUnknown bool, args []string, skippedOptions map[string]bool, relaxedOptions map[string]bool) (merged map[string]bool, err error) {
	merged = map[string]bool{}
	// prevent duplicates
	keys := map[string]bool{}
	var lastArgsIdx int = -1

	for i, pair := range args {
		wrapErr := func(err error) error {
			return InvalidConfigFlag{c.version, pair, err}
		}
		idx := strings.Index(pair, "=")
		var key, val string
		if idx != -1 {
			if !(idx < len(pair)-1) {
				err = wrapErr(fmt.Errorf("invalid argument syntax at %#v\n", pair))
				return
			}
			key, val = pair[:idx], pair[idx+1:]

			if val == "" {
				err = EmptyValueError(key)
				return
			}
		} else {
			key = pair
			val = "true"
		}

		argKey := key
		key = argToKey(argKey)

		if skippedOptions[key] {
			continue
		}

		if argKey != key {
			if lastArgsIdx >= 0 {
				err = wrapErr(fmt.Errorf("invalid option %s\n", args[lastArgsIdx]))
				return
			}
		} else {
			if lastArgsIdx >= 0 {
				err = wrapErr(fmt.Errorf("last argument already set to %q\n", args[lastArgsIdx]))
				return
			}
			lastArgsIdx = i
		}

		switch key {
		case argKey:
			_, has := c.spec[""]
			if ignoreUnknown && !has {
				continue
			}
			err = c.setLastArg(argKey)
			if err != nil {
				err = wrapErr(fmt.Errorf("invalid value %s: %s\n", key, err.Error()))
				return
			}
			merged[argKey] = true
		default:
			if sh, has := c.shortflags[key]; has {
				key = sh
			}

			if skippedOptions[key] {
				continue
			}

			if keys[key] {
				err = ErrDoubleOption("-" + key)
				return
			}

			_, has := c.spec[key]
			if ignoreUnknown && !has {
				continue
			}
			err = c.set(key, val, argKey)
			if err != nil {
				err = wrapErr(fmt.Errorf("invalid value for option %s: %s\n", key, err.Error()))
				return
			}
			merged[argKey] = true
			keys[key] = true
		}
	}

	if err = c.ValidateValues(); err != nil {
		return
	}
	err = c.checkMissing(skippedOptions, relaxedOptions)
	return
}

// GetURL returns the value of the option as url
func (c Config) GetURL(option string) *url.URL {
	if option != "" {
		if err := ValidateName(option); err != nil {
			panic(InvalidNameError(option))
		}
	}
	v, has := c.values[option]

	if has {
		switch vv := v.(type) {
		case string:
			u, err := url.Parse(vv)
			if err != nil {
				panic(fmt.Sprintf("invalid url %#v for option %s", v, option))
			}
			return u
		case *url.URL:
			return vv
		default:
			panic(fmt.Sprintf("invalid type %#v for option %s", v, option))
		}
	}

	u, _ := url.Parse("http://localhost:8080/")
	return u
	//return &url.URL{}
}

// GetFile returns the value of the option as file
func (c Config) GetFile(option string) path.Local {
	if option != "" {
		if err := ValidateName(option); err != nil {
			panic(InvalidNameError(option))
		}
	}
	v, has := c.values[option]

	if has {
		switch vv := v.(type) {
		case string:
			u, err := path.ParseFileFromSystem(vv)
			if err != nil {
				panic(fmt.Sprintf("invalid file %#v for option %s", v, option))
			}
			return u
		case path.Local:
			return vv
		default:
			panic(fmt.Sprintf("invalid type %#v for option %s", v, option))
		}
	}
	return path.MustWD().Join("new-file.tmp")
}

// GetDir returns the value of the option as path
func (c Config) GetDir(option string) path.Local {
	if option != "" {
		if err := ValidateName(option); err != nil {
			panic(InvalidNameError(option))
		}
	}
	v, has := c.values[option]

	if has {
		switch vv := v.(type) {
		case string:
			u, err := path.ParseDirFromSystem(vv)
			if err != nil {
				panic(fmt.Sprintf("invalid dir %#v for option %s", v, option))
			}
			return u
		case path.Local:
			return vv
		default:
			panic(fmt.Sprintf("invalid type %#v for option %s", v, option))
		}
	}
	return path.MustWD()
}

// GetBool returns the value of the option as bool
func (c Config) GetBool(option string) bool {
	if option != "" {
		if err := ValidateName(option); err != nil {
			panic(InvalidNameError(option))
		}
	}
	v, has := c.values[option]
	if has {
		return v.(bool)
	}
	return false
}

// GetFloat returns the value of the option as float64
func (c Config) GetFloat(option string) float64 {
	if option != "" {
		if err := ValidateName(option); err != nil {
			panic(InvalidNameError(option))
		}
	}
	v, has := c.values[option]
	if has {
		return v.(float64)
	}
	return 0
}

// GetInt returns the value of the option as int
func (c Config) GetInt(option string) int {
	if option != "" {
		if err := ValidateName(option); err != nil {
			panic(InvalidNameError(option))
		}
	}
	v, has := c.values[option]
	if has {
		return v.(int)
	}
	return 0
}

// GetValue returns the value of the option
func (c Config) GetValue(option string) interface{} {
	if option != "" {
		if err := ValidateName(option); err != nil {
			panic(InvalidNameError(option))
		}
	}
	v, has := c.values[option]
	if has {
		return v
	}
	return nil
}

// GetDateTime returns the value of the option as datetime
func (c Config) GetDateTime(option string) (t time.Time) {
	if option != "" {
		if err := ValidateName(option); err != nil {
			panic(InvalidNameError(option))
		}
	}
	v, has := c.values[option]
	if has {
		switch vv := v.(type) {
		case string:
			u, err := time.Parse(DateTimeFormat, vv)
			if err != nil {
				panic(fmt.Sprintf("invalid datetime %#v for option %s", v, option))
			}
			return u
		case time.Time:
			return vv
		default:
			panic(fmt.Sprintf("invalid type %#v for option %s", v, option))
		}
	}
	return time.Now()
}

// GetDate returns the value of the option as date
func (c Config) GetDate(option string) (t time.Time) {
	if option != "" {
		if err := ValidateName(option); err != nil {
			panic(InvalidNameError(option))
		}
	}
	v, has := c.values[option]
	if has {
		switch vv := v.(type) {
		case string:
			u, err := time.Parse(DateFormat, vv)
			if err != nil {
				panic(fmt.Sprintf("invalid date %#v for option %s", v, option))
			}
			return u
		case time.Time:
			return vv
		default:
			panic(fmt.Sprintf("invalid type %#v for option %s", v, option))
		}
	}
	return time.Now()
}

// GetTime returns the value of the option as time
func (c Config) GetTime(option string) (t time.Time) {
	if option != "" {
		if err := ValidateName(option); err != nil {
			panic(InvalidNameError(option))
		}
	}
	v, has := c.values[option]
	if has {
		switch vv := v.(type) {
		case string:
			u, err := time.Parse(TimeFormat, vv)
			if err != nil {
				panic(fmt.Sprintf("invalid time %#v for option %s", v, option))
			}
			return u
		case time.Time:
			return vv
		default:
			panic(fmt.Sprintf("invalid type %#v for option %s", v, option))
		}
	}
	return time.Now()
}

// GetString returns the value of the option as string
func (c Config) GetString(option string) string {
	if option != "" {
		if err := ValidateName(option); err != nil {
			panic(InvalidNameError(option))
		}
	}
	v, has := c.values[option]
	if has {
		return v.(string)
	}
	return ""
}

// GetJSON unmarshals the value of the option to val.
func (c Config) GetJSON(option string, val interface{}) error {
	if err := ValidateName(option); err != nil {
		panic(InvalidNameError(option))
	}
	v, has := c.values[option]
	if has {
		return json.Unmarshal([]byte(v.(string)), val)
	}
	return nil
}

func (c *Config) noValues() bool {
	if len(c.values) > 0 {
		return false
	}

	for _, cc := range c.commands {
		if len(cc.values) > 0 {
			return false
		}
	}

	return true
}

func (c *Config) WriteConfigFile(locfile path.Relative) (err error) {

	if c.isCommand() {
		return c.parent.WriteConfigFile(locfile)
	}

	if path.IsDir(locfile) {
		return fs.ErrExpectedFile
	}

	if errValid := c.ValidateValues(); errValid != nil {
		return errValid
	}

	if err != nil {
		return err
	}

	fs.MkDirAll(c.fsys, locfile.Dir())

	var backup []byte

	if c.fsys.Exists(locfile) {
		backup, _ = fs.ReadFile(c.fsys, locfile)
	}

	// don't write anything, if we have no config values
	if c.noValues() {
		// files exist, but will be deleted (no config values)
		c.fsys.Delete(locfile, false)
		// files does not exist, we have no values, so lets do nothing
		return nil
	}
	if backup == nil {
		backup = []byte{}
	}

	defer func() {
		if err != nil {
			_ = c.fsys.Delete(locfile, false)
			if len(backup) != 0 {
				fs.WriteFile(c.fsys, locfile, backup, false)
			}
		}
	}()

	err = c.writeConfigValues(c.fsys, locfile)
	return err
}

func (c *Config) writeConfigValues(fsys fs.FS, file path.Relative) error {

	m := map[string]interface{}{}
	m["version"] = c.version.String()

	for k, v := range c.values {
		if k == "" {
			continue
		}
		// do nothing for nil values
		if v == nil {
			continue
		}

		m[k] = v
	}

	for cmd, cmdConfig := range c.commands {

		switch cmdConfig {
		case c.cmdConfig:
			continue
		case c.cmdHelp:
			continue
		case c.cmdVersion:
			continue
		}

		subm := map[string]interface{}{}

		for kk, vv := range cmdConfig.values {
			if kk == "" {
				continue
			}
			if vv == nil {
				continue
			}

			subm[kk] = vv

		}

		if len(subm) > 0 {
			m[cmd] = subm
		}

	}

	var bf bytes.Buffer

	prom := toml.NewEncoder(&bf).PromoteAnonymous(true)
	if err := prom.Encode(m); err != nil {
		return err
	}

	return fsys.Write(file, fs.ReadCloser(&bf), true)
}
