//go:build darwin
// +build darwin

package config

/*
according to http://wiki.freepascal.org/Multiplatform_Programming_Guide#Configuration_files
/etc
/Users/user/.config/project1
*/

import (
	"os"
	"strings"
)

/*
func setUserDir() {
	home := os.Getenv("HOME")
	if home == "" {
		home = filepath.Join("/home", os.Getenv("USER"))
	}
	USER_DIR = filepath.Join(home + ".config")
}
*/

func setGlobalDir() {
	GLOBAL_DIRS = "/etc/.config"
	//GLOBAL_DIRS = "/etc"
}

func setWorkingDir() {
	wd, err := os.Getwd()
	if err != nil {
		wd = "."
	}

	WORKING_DIR = wd
}

func generalUserConfigDir() string {
	return "$HOME/Library/Application Support"
}

func splitGlobals() []string {
	return strings.Split(GLOBAL_DIRS, ":")
}

func init() {
	//	setUserDir()
	CONFIG_DIR = ".config"
	setGlobalDir()
	setWorkingDir()
}
