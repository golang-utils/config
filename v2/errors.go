package config

import (
	"errors"
	"fmt"

	"gitlab.com/golang-utils/version"
)

var (
	ErrInvalidShortflag = errors.New("invalid shortflag")
	ErrSubCommand       = errors.New("sub command is not supported")
	ErrMissingHelp      = errors.New("missing help text")
)

type InvalidVersionError string

func (e InvalidVersionError) Error() string {
	return fmt.Sprintf("invalid version: %q", string(e))
}

type EmptyValueError string

func (e EmptyValueError) Error() string {
	return fmt.Sprintf("invalid value: empty string for %#v", string(e))
}

type InvalidNameError string

func (e InvalidNameError) Error() string {
	return fmt.Sprintf("invalid name %#v", string(e))
}

type InvalidTypeError struct {
	Option
}

func (e InvalidTypeError) Error() string {
	return fmt.Sprintf("invalid type %#v for option %#v", e.Option.Type, e.Option.flagName())
}

type InvalidDefault struct {
	Option
}

func (e InvalidDefault) Error() string {
	return fmt.Sprintf("invalid default value %#v option %s of type %s", e.Option.Default, e.Option.flagName(), e.Option.Type)
}

type MissingOptionError struct {
	Version *version.Version
	Option
}

func (e MissingOptionError) Error() string {
	return fmt.Sprintf("required option %s not set", e.Option.flagName())
}

type InvalidConfigEnv struct {
	Version *version.Version
	EnvKey  string
	Err     error
}

func (e InvalidConfigEnv) Error() string {
	return fmt.Sprintf("env variable %s is not compatible with version %s: %s", e.EnvKey, e.Version, e.Err.Error())
}

type InvalidConfigFlag struct {
	Version *version.Version
	Flag    string
	Err     error
}

func (e InvalidConfigFlag) Error() string {
	return fmt.Sprintf("%s is not compatible with version %s: %s", e.Flag, e.Version, e.Err.Error())
}

type InvalidConfig struct {
	Version *version.Version
	Err     error
}

func (e InvalidConfig) Error() string {
	return fmt.Sprintf("config is not compatible with version %s: %s", e.Version, e.Err.Error())
}

type InvalidConfigFileError struct {
	ConfigFile string
	Version    *version.Version
	Err        error
}

func (e InvalidConfigFileError) Error() string {
	return fmt.Sprintf("config file %s is not compatible with version %s: %s", e.ConfigFile, e.Version, e.Err.Error())
}

type InvalidValueError struct {
	Option
	Value interface{}
}

func (e InvalidValueError) Error() string {
	return fmt.Sprintf("value %#v is invalid for option %s", e.Value, e.Option.flagName())
}

type ErrInvalidOptionName string

func (e ErrInvalidOptionName) Error() string {
	return fmt.Sprintf("invalid option name %s", string(e))
}

type ErrInvalidAppName string

func (e ErrInvalidAppName) Error() string {
	return fmt.Sprintf("invalid app name %s", string(e))
}

type UnknownOptionError struct {
	Version *version.Version
	Option  string
}

func (e UnknownOptionError) Error() string {
	return fmt.Sprintf("option %s does not exist", e.Option)
}

type ErrDoubleOption string

func (e ErrDoubleOption) Error() string {
	return fmt.Sprintf("option %s is set twice", string(e))
}

type ErrDoubleShortflag string

func (e ErrDoubleShortflag) Error() string {
	return fmt.Sprintf("shortflag %s is set twice", string(e))
}
