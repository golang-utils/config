package config

import (
	"net/url"
	"time"

	"gitlab.com/golang-utils/fs/path"
)

type URLGetter struct {
	opt *Option
	cfg *Config
}

func (b *URLGetter) Get() *url.URL {
	return b.cfg.GetURL(b.opt.Name)
}

func (b *URLGetter) IsSet() bool {
	return b.cfg.IsSet(b.opt.Name)
}

type FileGetter struct {
	opt *Option
	cfg *Config
}

func (b *FileGetter) Get() path.Local {
	return b.cfg.GetFile(b.opt.Name)
}

func (b *FileGetter) IsSet() bool {
	return b.cfg.IsSet(b.opt.Name)
}

type DirGetter struct {
	opt *Option
	cfg *Config
}

func (b *DirGetter) Get() path.Local {
	return b.cfg.GetDir(b.opt.Name)
}

func (b *DirGetter) IsSet() bool {
	return b.cfg.IsSet(b.opt.Name)
}

type BoolGetter struct {
	opt *Option
	cfg *Config
}

func (b *BoolGetter) Get() bool {
	return b.cfg.GetBool(b.opt.Name)
}

func (b *BoolGetter) IsSet() bool {
	return b.cfg.IsSet(b.opt.Name)
}

type IntGetter struct {
	opt *Option
	cfg *Config
}

func (b *IntGetter) IsSet() bool {
	return b.cfg.IsSet(b.opt.Name)
}

func (b *IntGetter) Get() int {
	return b.cfg.GetInt(b.opt.Name)
}

type FloatGetter struct {
	opt *Option
	cfg *Config
}

func (b *FloatGetter) IsSet() bool {
	return b.cfg.IsSet(b.opt.Name)
}

func (b *FloatGetter) Get() float64 {
	return b.cfg.GetFloat(b.opt.Name)
}

type StringGetter struct {
	opt *Option
	cfg *Config
}

func (b *StringGetter) IsSet() bool {
	return b.cfg.IsSet(b.opt.Name)
}

func (b *StringGetter) Get() string {
	return b.cfg.GetString(b.opt.Name)
}

type DateTimeGetter struct {
	opt *Option
	cfg *Config
}

func (b *DateTimeGetter) IsSet() bool {
	return b.cfg.IsSet(b.opt.Name)
}

func (b *DateTimeGetter) Get() time.Time {
	return b.cfg.GetDateTime(b.opt.Name)
}

type DateGetter struct {
	opt *Option
	cfg *Config
}

func (b *DateGetter) IsSet() bool {
	return b.cfg.IsSet(b.opt.Name)
}

func (b *DateGetter) Get() time.Time {
	return b.cfg.GetDate(b.opt.Name)
}

type TimeGetter struct {
	opt *Option
	cfg *Config
}

func (b *TimeGetter) IsSet() bool {
	return b.cfg.IsSet(b.opt.Name)
}

func (b *TimeGetter) Get() time.Time {
	return b.cfg.GetTime(b.opt.Name)
}

type JSONGetter struct {
	opt *Option
	cfg *Config
}

func (b *JSONGetter) IsSet() bool {
	return b.cfg.IsSet(b.opt.Name)
}

func (b *JSONGetter) Get(val interface{}) error {
	return b.cfg.GetJSON(b.opt.Name, val)
}
