//go:build !windows
// +build !windows

package config

import (
	"os/exec"
)

func execCommand(c string) *exec.Cmd {
	return exec.Command("/bin/sh", "-c", c)
}

func OnWindows() bool {
	return false
}
