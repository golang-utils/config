package config

import (
	"fmt"
	"testing"
)

func TestTransformEnvToNames2(t *testing.T) {
	tests := []struct {
		app    string
		cmd    string
		option string
		envvar string
	}{
		{"myapp", "show", "fast", "MYAPP_SHOW_OPTION_FAST"},
		{"myapp", "", "fast", "MYAPP_OPTION_FAST"},
		{"humpy-dumpy", "tiggly-wiggly-fast", "trigger-set", "HUMPY__DUMPY_TIGGLY__WIGGLY__FAST_OPTION_TRIGGER__SET"},
		{"my-app", "sh_ow", "f-as_t", "MY__APP_SH___OW_OPTION_F__AS___T"},
		{"my-ap_p", "", "f-as-t", "MY__AP___P_OPTION_F__AS__T"},
	}

	for _, test := range tests {

		gotApp, gotCmd, gotOption, err := transformEnvToNames2(test.envvar)

		if err != nil {
			t.Errorf("TransformEnvToNames2(%q) returned an error", test.envvar)
		}

		if gotApp != test.app || gotCmd != test.cmd || gotOption != test.option {
			t.Errorf("TransformEnvToNames2(%q) = %q,%q,%q; // want %q,%q,%q",
				test.envvar, gotApp, gotCmd, gotOption, test.app, test.cmd, test.option)
		}
	}

}

func TestTransformToEnvVar2(t *testing.T) {
	tests := []struct {
		app      string
		cmd      string
		option   string
		expected string
	}{
		{"myapp", "show", "fast", "MYAPP_SHOW_OPTION_FAST"},
		{"myapp", "", "fast", "MYAPP_OPTION_FAST"},
		{"humpy-dumpy", "tiggly-wiggly-fast", "trigger-set", "HUMPY__DUMPY_TIGGLY__WIGGLY__FAST_OPTION_TRIGGER__SET"},
		{"my-app", "sh_ow", "f-as_t", "MY__APP_SH___OW_OPTION_F__AS___T"},
		{"my-ap_p", "", "f-as-t", "MY__AP___P_OPTION_F__AS__T"},
	}

	for _, test := range tests {

		got := transformToEnvVar2(test.app, test.cmd, test.option)
		expected := test.expected

		if got != expected {
			t.Errorf("TransformToEnvVar2(%q,%q,%q) = %q; want %q",
				test.app, test.cmd, test.option, got, expected)
		}
	}

}

func TestValidateName2(t *testing.T) {

	tests := []struct {
		name  string
		valid bool
	}{
		{"ab", true},
		{"a1", true},
		{"aa", true},
		{"", false},
		{"a", false},
		{"01", false},
		{"A", false},
		{"aA", false},
		{"-aa", false},
		{"aa-", false},
		{"_aa", false},
		{"aa_", false},
		{"a_a", true},
		{"a-a", true},
		{"a_0", true},
		{"a-0", true},
		{"a_a_a", true},
		{"a-a-a", true},
		{"a_a-a", true},
		{"a_a-a", true},
		{"a_-aa", false},
		{"a-_aa", false},
		{"a__aa", false},
		{"a--aa", false},
	}

	for _, test := range tests {

		err := ValidateName(test.name)
		isValid := err == nil

		if isValid != test.valid {
			t.Errorf("ValidateName2(%v) = %v; want %v", test.name, isValid, test.valid)
		}
	}

}

func TestVersion(t *testing.T) {
	app := New("testapp", "help text")
	err := app.RunArgsSilent("version")
	if err != nil {
		t.Errorf("error: %v", err.Error())
	}

	if app.version != nil {
		t.Errorf("wrong version")
	}
}

func ExampleConfig() {
	app := New("testapp", "help text")
	verbose := app.Bool("verbose", "show verbose messages", Required())
	// real application would use
	// err := app.Run()
	err := app.RunArgs("--verbose")
	if err != nil {
		fmt.Printf("ERROR: %v", err)
	}
	fmt.Printf("verbose: %v", verbose.Get())
	// Output: verbose: true
}
