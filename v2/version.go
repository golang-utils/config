package config

import (
	_ "embed"

	"gitlab.com/golang-utils/version"
)

//go:embed VERSION
var VERSION string

func init() {
	ParseVersion(VERSION)
}

func ParseVersion(vers string) *version.Version {
	v, err := version.Parse(vers)
	if err != nil {
		return nil
	}
	return v
}
