package config

import (
	"fmt"
	"strings"

	"gitlab.com/golang-utils/dialog"
)

var skipCommands = map[string]bool{
	"config":  true,
	"version": true,
	"help":    true,
}

func newDialog(name string, opts ...dialog.Option) dialog.App {
	return dialog.New(name, opts...)
}

func (c *Config) newDialogConfig() *dialogConfig {
	var opts []dialog.Option

	opts = append(opts,
		//	dialog.WithBackNavigation(),
		dialog.WithFooter(),
		dialog.WithHeader(),
		dialog.WithDefaultGui(),
		//dialog.WithBreadcrumb(),
	)

	a := newDialog("configuration of "+c.appName(), opts...)

	return &dialogConfig{
		config: c,
		app:    a,
		values: map[[2]string]string{},
	}
}

type dialogConfig struct {
	config             *Config
	app                dialog.App
	currentCommand     *Config
	currentCommandName string
	currentCommandHelp string
	currentOption      *Option
	currentFile        string
	currentValue       string
	currentValueType   string
	currentValueHelp   string

	// 0-command 1-option: value
	values map[[2]string]string
}

func (d *dialogConfig) validateValue(s string) error {
	v, err := stringToValue(d.currentOption.Type, s)

	if err != nil {
		return err
	}
	return d.currentOption.ValidateValue(v)
}

func (d *dialogConfig) Run() error {
	d.currentCommand = d.config
	return d.app.Run(d.askForFile())
}

func (d *dialogConfig) context() string {
	var bd strings.Builder

	if d.currentFile != "" {
		bd.WriteString("    file: '" + d.currentFile + "'\n")
	}

	if d.currentCommandName != "" {
		bd.WriteString("    command: '" + d.currentCommandName + "'\n")
	}

	if d.currentCommandHelp != "" {
		bd.WriteString("    command info: '" + d.currentCommandHelp + "'\n")
	}

	if d.currentOption != nil {
		bd.WriteString("    option: '" + d.currentOption.Name + "'\n")
	}

	if d.currentValue != "" {
		bd.WriteString("    value: '" + d.currentValue + "'\n")
	}

	if d.currentValueType != "" {
		bd.WriteString("    type: '" + d.currentValueType + "'\n")
	}

	if d.currentValueHelp != "" {
		bd.WriteString("    help: '" + d.currentValueHelp + "'\n")
	}

	return bd.String() + "\n"
}

func (d *dialogConfig) loadValues() (err error) {
	d.config.Reset()

	switch d.currentFile {
	case "user":
		err = d.config.LoadUser()
	case "local":
		err = d.config.LoadLocals()
	case "global":
		err = d.config.LoadGlobals()
		//case "env vars":
		//err = d.config.MergeEnv2()
	}

	if err != nil {
		return err
	}

	d.config.EachValue(func(name string, value interface{}) {
		// don't use lastarg
		if name != "" {
			d.values[[2]string{"", name}] = fmt.Sprintf("%v", value)
		}
	})

	for cname, cmd := range d.config.commands {
		if skipCommands[cname] {
			continue
		}

		cmd.EachValue(func(name string, value interface{}) {
			// don't use lastarg
			if name != "" {
				d.values[[2]string{cname, name}] = fmt.Sprintf("%v", value)
			}
		})
	}

	return nil
}

func (d *dialogConfig) askForFile() dialog.Screen {
	d.currentFile = ""
	d.currentCommandName = ""
	d.currentCommandHelp = ""
	d.currentOption = nil
	d.currentValue = ""
	d.currentValueType = ""
	d.currentValueHelp = ""

	question := "Please select the configuration file"
	var menu = []string{"local", "user", "global"}

	sc := d.app.NewSelect(question, menu, func(chosen string) dialog.Screen {
		var err error
		if d.currentFile != chosen {
			d.currentFile = chosen
			err = d.loadValues()
		}

		if err != nil {
			sc := d.app.NewError(err.Error())
			sc.SetOptions(dialog.WithName("Error while loading values"))
		}

		return d.askForAction()
	})

	sc.SetOptions(
		dialog.WithValidator(dialog.NonEmpty),
		dialog.WithPrefilled("local"),
	)

	return sc
}

func (d *dialogConfig) askForCommand() dialog.Screen {
	d.currentCommandName = ""
	d.currentOption = nil
	d.currentCommandHelp = ""
	d.currentValue = ""
	d.currentValueType = ""
	d.currentValueHelp = ""

	question := d.context() + "Please select the (sub)command"
	without := "--without command--"
	var menu = []string{without}

	for name := range d.config.commands {
		if skipCommands[name] {
			continue
		}
		menu = append(menu, name)
	}

	sc := d.app.NewSelect(question, menu, func(chosen string) dialog.Screen {
		if chosen == without {
			d.currentCommandName = ""
			d.currentCommand = d.config
			d.currentCommandHelp = ""
		} else {
			d.currentCommandName = chosen
			d.currentCommand = d.config.commands[chosen]
			d.currentCommandHelp = shorten(d.currentCommand.helpIntro)
		}

		return d.askForOption()
	})

	sc.SetOptions(
		dialog.WithValidator(dialog.NonEmpty),
		dialog.WithPrefilled(without),
	)

	return sc
}

const maxlen = 25

func shorten(h string) string {
	if len(h) > maxlen {
		return h[:maxlen] + "..."
	}

	return h
}

func (d *dialogConfig) askForOption() dialog.Screen {
	d.currentOption = nil
	d.currentValue = ""
	d.currentValueType = ""
	d.currentValueHelp = ""

	question := d.context() + "Please select the option/flag"
	var menu = []string{}

	for name := range d.currentCommand.spec {
		// don't use lastargs
		if name != "" {
			menu = append(menu, name)
		}
	}

	if len(menu) == 0 {
		return d.app.NewError("no option found")
	}

	sc := d.app.NewSelect(question, menu, func(chosen string) dialog.Screen {
		if chosen == d.currentCommand.lastArgName() {
			chosen = ""
		}
		d.currentOption = d.currentCommand.spec[chosen]
		d.currentValue = shorten(d.currentVal())
		d.currentValueType = d.currentOption.Type
		d.currentValueHelp = shorten(d.currentOption.Help)

		return d.askForOptionAction()
	})

	sc.SetOptions(
		dialog.WithValidator(dialog.NonEmpty),
		dialog.WithPrefilled(menu[0]),
	)

	return sc
}

func (d *dialogConfig) askForOptionAction() dialog.Screen {
	question := d.context() + "What do you want to do with the option/flag?"
	var menu = []string{
		"1) show value",
		"2) show help",
		"3) set value",
		"4) unset value",
		"5) back to general actions",
	}

	sc := d.app.NewSelect(question, menu, func(chosen string) dialog.Screen {
		switch chosen {
		case "1) show value":
			return d.showValue()
		case "2) show help":
			return d.showHelp()
		case "3) set value":
			return d.changeValue()
		case "4) unset value":
			return d.unsetValue()
		case "5) back to general actions":
			return d.askForAction()
		default:
			panic("must not happen")
		}
	})

	sc.SetOptions(
		dialog.WithValidator(dialog.NonEmpty),
		dialog.WithPrefilled("3) set value"),
	)

	return sc
}

func (d *dialogConfig) askForAction() dialog.Screen {
	question := d.context() + "What do you want to do"
	var menu = []string{
		"1) show all values",
		"2) save to file",
		"3) delete all values",
		"4) select option/flag",
		"5) select command",
		"6) select config file",
	}

	sc := d.app.NewSelect(question, menu, func(chosen string) dialog.Screen {
		switch chosen {
		case "1) show all values":
			return d.showChanges()
		case "4) select option/flag":
			return d.askForOption()
		case "5) select command":
			return d.askForCommand()
		case "2) save to file":
			return d.saveChanges()
		case "3) delete all values":
			return d.dropChanges()
		case "6) select config file":
			return d.askForFile()
		default:
			panic("must not happen")
		}
	})

	sc.SetOptions(
		dialog.WithValidator(dialog.NonEmpty),
		dialog.WithPrefilled("1) show all values"),
	)

	return sc
}

func (d *dialogConfig) inspectValues() string {
	var bd strings.Builder

	m := map[string]map[string]string{}

	for k, v := range d.values {
		mm := m[k[0]]
		if mm == nil {
			mm = map[string]string{}
		}
		mm[k[1]] = v

		m[k[0]] = mm
	}

	if len(m[""]) > 0 {
		bd.WriteString("\n-- no command --\n")
	}

	for k, v := range m[""] {
		bd.WriteString("    " + k + ": " + v + "\n")
	}

	for kk, mm := range m {
		if kk == "" {
			continue
		}

		bd.WriteString("\ncommand: '" + kk + "' --\n")

		for k, v := range mm {
			bd.WriteString("    " + k + ": " + v + "\n")
		}
	}

	return bd.String() + "\n"
}

func (d *dialogConfig) showChanges() dialog.Screen {
	question := "Here are the current values:\n\n" + d.inspectValues() + "\npress ENTER to continue"
	sc := d.app.NewOutput(question, d.askForAction)

	sc.SetOptions()

	return sc
}

func (d *dialogConfig) dropChanges() dialog.Screen {
	d.values = map[[2]string]string{}
	question := "The values are deleted " + "\npress ENTER to continue"
	sc := d.app.NewOutput(question, d.askForAction)

	sc.SetOptions()

	return sc
}

func (d *dialogConfig) applyChanges() (err error) {
	var loc = "env"

	d.config.Reset()

	switch d.currentFile {
	case "user":
		loc = d.config.UserFile().String()
	case "local":
		loc = d.config.LocalFile().String()
	case "global":
		loc = d.config.FirstGlobalsFile().String()
	}

	for k, v := range d.values {
		if k[0] == "" {
			// not sure, if this is correct
			/*
				if d.config.lastArgName() == v {
					v = ""
				}
			*/
			err = d.config.set(k[1], v, loc)
		} else {
			cmd := d.config.commands[k[0]]
			// not sure, if this is correct
			/*
				if cmd.lastArgName() == v {
					v = ""
				}
			*/
			err = cmd.set(k[1], v, loc)
		}

		if err != nil {
			return err
		}
	}

	switch d.currentFile {
	case "user":
		return d.config.SaveToUser()
	case "local":
		return d.config.SaveToLocal()
	case "global":
		return d.config.SaveToGlobals()
	}
	return nil
}

func (d *dialogConfig) saveChanges() dialog.Screen {
	question := "saved successfully" + "\npress ENTER to continue"
	err := d.applyChanges()
	if err != nil {
		question = "error while saving: " + err.Error()
	}

	sc := d.app.NewOutput(question, d.askForAction)

	sc.SetOptions()

	return sc
}

func (d *dialogConfig) currentVal() string {
	if d.currentCommand == nil {
		return ""
	}
	if d.currentOption == nil {
		return ""
	}
	return d.values[[2]string{d.currentCommandName, d.currentOption.Name}]
}

func (d *dialogConfig) showValue() dialog.Screen {
	question := d.context() + "value is: \n" + d.currentVal() + "\n"
	sc := d.app.NewOutput(question, func() dialog.Screen {
		return d.askForOptionAction()
	})

	sc.SetOptions()

	return sc
}

func (d *dialogConfig) showHelp() dialog.Screen {
	question := d.context() + "Help: \n" + d.currentOption.Help + "\n"
	sc := d.app.NewOutput(question, func() dialog.Screen {
		return d.askForOptionAction()
	})

	sc.SetOptions()

	return sc
}

func (d *dialogConfig) unsetValue() dialog.Screen {
	d.currentValue = ""
	delete(d.values, [2]string{d.currentCommandName, d.currentOption.Name})

	question := "successfully unsetted" + "\npress ENTER to continue"
	sc := d.app.NewOutput(question, d.askForAction)

	sc.SetOptions()

	return sc
}

func (d *dialogConfig) changeValue() dialog.Screen {
	d.currentValue = ""
	question := d.context() + "value:"
	sc := d.app.NewInput(question, func(inp string) dialog.Screen {
		d.currentValue = shorten(inp)
		name := d.currentOption.Name
		d.values[[2]string{d.currentCommandName, name}] = inp
		return d.askForOptionAction()
	})

	var valid dialog.ValidatorFunc = func(vs interface{}) error {
		return d.validateValue(vs.(string))
	}

	sc.SetOptions(
		dialog.WithValidator(valid),
		dialog.WithPrefilled(d.currentVal()),
	)

	return sc
}
