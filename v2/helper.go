package config

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"net/url"
	"os"
	"os/exec"
	"regexp"
	"strconv"
	"strings"
	"time"

	"gitlab.com/golang-utils/fs/path"
)

func convertOpttype(optType string) string {
	switch optType {
	case "dir":
		return "<dir>"
	case "url":
		return "<url>"
	case "file":
		return "<file>"
	case "bool":
		return ""
	case "int":
		return "<integer>"
	case "float64":
		return "<float>"
	case "string":
		return "''"
	case "json":
		return "<json>"
	case "time":
		return "<hh:mm:ss>"
	case "datetime":
		return "<YYYY-MM-DD hh:mm:ss>"
	case "date":
		return "<YYYY-MM-DD>"
	}
	panic("should not happend")
}

var leftWidth = 32
var totalWidth = 80

func pad(left string, right string) string {
	var bf bytes.Buffer

	numSpaces := leftWidth - len([]rune(left))
	bf.WriteString(left)

	for i := 0; i < numSpaces; i++ {
		bf.WriteString(" ")
	}

	spaceRight := totalWidth - 1 - leftWidth
	right = strings.Replace(right, "\n", " \n ", -1)
	arr := strings.Split(right, " ")

	spaceRightAvailable := spaceRight
	for _, word := range arr {
		w := strings.TrimSpace(word)
		if w == "" && word != "\n" {
			continue
		}
		if len(w) > spaceRightAvailable || word == "\n" {
			bf.WriteString("\n")
			for i := 0; i < leftWidth; i++ {
				bf.WriteString(" ")
			}
			spaceRightAvailable = spaceRight
		}

		bf.WriteString(w + " ")
		spaceRightAvailable -= len(w) + 1
	}
	return bf.String()
}

func runCMDs(wd string, cmds ...*exec.Cmd) error {
	for _, c := range cmds {
		c.Dir = wd
		_, err := c.CombinedOutput()
		if err != nil {
			return err
		}
	}

	return nil
}

const (
	DateFormat      = "2006-01-02"
	TimeFormat      = "15:04:05"
	DateTimeFormat  = "2006-01-02 15:04:05"
	DateTimeGeneral = "2006-01-02 15:04:05 -0700 MST"
)

var (
	nameRegExp2     = regexp.MustCompile("^[a-z][-_a-z0-9]+$")
	versionRegexp   = regexp.MustCompile("^[a-z0-9-.]+$")
	shortflagRegexp = regexp.MustCompile("^[a-z]$")
)

func validateShortflag(shortflag string) error {
	if shortflag == "" || shortflagRegexp.MatchString(shortflag) {
		return nil
	}
	return ErrInvalidShortflag
}

/*
rules for names:

	must not start with numbers
	may not have more than an underscore in a row
	may not have more than a dash in a row
	may not start or end with an underscore
	may not start or end with a dash
	may not have an underscore followed by a dash
	may not have a dash followed by an underscore
	other than that, just lowercase letters are allowed
	we need at least two characters
*/
func ValidateName(name string) (err error) {
	switch {
	case len(name) < 2:
	case name[0] == '-':
	case name[len(name)-1] == '-':
	case name[0] == '_':
	case name[len(name)-1] == '_':
	case !nameRegExp2.MatchString(name):
	case strings.Contains(name, "__"):
	case strings.Contains(name, "--"):
	case strings.Contains(name, "_-"):
	case strings.Contains(name, "-_"):
	default:
		return nil
	}

	return InvalidNameError(name)
}

// TransformToEnvVar2 transforms an option to a EnvVar name
// it panics, if app, cmd or option have invalid names (you need to
// check before, if they are valid)
// cmd is left empty, if no command is set
func transformToEnvVar2(app, cmd, option string) string {
	if err := ValidateName(app); err != nil {
		panic("invalid app name: " + app)
	}

	if cmd != "" {
		if err := ValidateName(cmd); err != nil {
			panic("invalid cmd name: " + cmd)
		}
	}

	if err := ValidateName(option); err != nil {
		panic("invalid option name: " + option)
	}

	sep := "OPTION"

	app = strings.ReplaceAll(app, "_", "#")

	var prefix = app

	if cmd == "" {
		prefix = prefix + "_" + sep
	} else {
		cmd = strings.ReplaceAll(cmd, "_", "#")
		prefix = prefix + "_" + cmd + "_" + sep
	}

	option = strings.ReplaceAll(option, "_", "#")

	envvar := prefix + "_" + option
	envvar = strings.ReplaceAll(envvar, "-", "__")
	envvar = strings.ReplaceAll(envvar, "#", "___")

	return strings.ToUpper(envvar)
}

func isEnvVar2(env, app string) bool {
	app = strings.ReplaceAll(app, "_", "#")
	app = strings.ReplaceAll(app, "-", "__")
	app = strings.ReplaceAll(app, "#", "___")
	app = strings.ToUpper(app)
	return strings.HasPrefix(env, app+"_")
}

// TransformEnvToNames2 transforms and env variable to app cmd and option
func transformEnvToNames2(envvar string) (app, cmd, option string, err error) {

	/*
		first split by separator ___
		then you have the names
		and for each name, first replace __ with _ and then _ with -
	*/

	envvar = strings.ReplaceAll(envvar, "___", "#")
	envvar = strings.ReplaceAll(envvar, "__", "-")
	envvar = strings.ToLower(envvar)

	a := strings.Split(envvar, "_")

	switch len(a) {
	case 3:
		app = a[0]
		// a[1] == "C"
		option = a[2]
	case 4:
		app = a[0]
		cmd = a[1]
		// a[2] == "C"
		option = a[3]
	default:
		err = fmt.Errorf("non conformant env variable")
		return
	}

	app = strings.ReplaceAll(app, "#", "_")

	if cmd != "" {
		cmd = strings.ReplaceAll(cmd, "#", "_")
	}

	option = strings.ReplaceAll(option, "#", "_")

	return
}

func validateVersion(version string) error {
	if !versionRegexp.MatchString(version) {
		return InvalidVersionError(version)
	}
	return nil
}

// ValidateType checks if the given type is valid.
// If it does, nil is returned, otherwise
// ErrInvalidType is returned
func ValidateType(option Option) error {
	switch option.Type {
	case "bool", "int", "float64", "string", "datetime", "date", "time", "json", "dir", "file", "url":
		return nil
	default:
		return InvalidTypeError{option}
	}
}

var delim = []byte("\n$")

func stringToValue(typ string, in string) (out interface{}, err error) {
	switch typ {
	case "bool":
		return strconv.ParseBool(in)
	case "int":
		i, e := strconv.Atoi(in)
		return int(i), e
	case "float64":
		fl, e := strconv.ParseFloat(in, 64)
		return float64(fl), e
	case "datetime":
		_, e := time.Parse(DateTimeGeneral, in)
		if e != nil {
			_, e = time.Parse(DateTimeFormat, in)
		}
		return in, e
	case "date":
		_, e := time.Parse(DateTimeGeneral, in)
		if e != nil {
			_, e = time.Parse(DateFormat, in)
		}
		return in, e
	case "time":
		_, e := time.Parse(DateTimeGeneral, in)
		if e != nil {
			_, e = time.Parse(TimeFormat, in)
		}
		return in, e
	case "string":
		return in, nil
	case "dir":
		_, e := path.ParseDirFromSystem(in)
		return in, e
	case "file":
		_, e := path.ParseFileFromSystem(in)
		return in, e
	case "url":
		_, e := url.Parse(in)
		return in, e
	case "json":
		var v interface{}
		e := json.Unmarshal([]byte(in), &v)
		if e != nil {
			return nil, e
		}
		return in, nil
	default:
		return nil, errors.New("unknown type " + typ)
	}

}

func keyToArg(key string) string {
	return "--" + key
}

func argToKey(arg string) string {
	return strings.TrimLeft(arg, "-")
}

func err2Stderr(err error) {
	if err != nil {
		fmt.Fprintf(os.Stderr, "Error: %s\n", err)
		os.Exit(1)
	}
}
