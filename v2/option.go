package config

import (
	"encoding/json"
	"fmt"
	"net/url"
	"time"

	"gitlab.com/golang-utils/fs/path"
)

// shortcut for MustNewOption of type url
func (c *Config) URL(name, helpText string, opts ...func(*Option)) URLGetter {
	return URLGetter{
		opt: c.MustNewOption(name, "url", helpText, opts),
		cfg: c,
	}
}

// LastURL receives the last argument as a url
func (c *Config) LastURL(name, helpText string, opts ...func(*Option)) URLGetter {
	opt := c.MustNewOption("", "url", helpText, opts)
	opt.LastArgName = name
	return URLGetter{
		opt: opt,
		cfg: c,
	}
}

// shortcut for MustNewOption of type dir
func (c *Config) Dir(name, helpText string, opts ...func(*Option)) DirGetter {
	return DirGetter{
		opt: c.MustNewOption(name, "dir", helpText, opts),
		cfg: c,
	}
}

// LastDir receives the last argument as a dir
func (c *Config) LastDir(name, helpText string, opts ...func(*Option)) DirGetter {
	opt := c.MustNewOption("", "dir", helpText, opts)
	opt.LastArgName = name
	return DirGetter{
		opt: opt,
		cfg: c,
	}
}

// shortcut for MustNewOption of type file
func (c *Config) File(name, helpText string, opts ...func(*Option)) FileGetter {
	return FileGetter{
		opt: c.MustNewOption(name, "file", helpText, opts),
		cfg: c,
	}
}

// LastFile receives the last argument as a file
func (c *Config) LastFile(name, helpText string, opts ...func(*Option)) FileGetter {
	opt := c.MustNewOption("", "file", helpText, opts)
	opt.LastArgName = name
	return FileGetter{
		opt: opt,
		cfg: c,
	}
}

// shortcut for MustNewOption of type bool
func (c *Config) Bool(name, helpText string, opts ...func(*Option)) BoolGetter {
	return BoolGetter{
		opt: c.MustNewOption(name, "bool", helpText, opts),
		cfg: c,
	}
}

// LastBool receives the last argument as a boolean
func (c *Config) LastBool(name, helpText string, opts ...func(*Option)) BoolGetter {
	//c.lastArgName = name
	opt := c.MustNewOption("", "bool", helpText, opts)
	opt.LastArgName = name
	return BoolGetter{
		opt: opt,
		cfg: c,
	}
}

// shortcut for MustNewOption of type int
func (c *Config) Int(name, helpText string, opts ...func(*Option)) IntGetter {
	return IntGetter{
		opt: c.MustNewOption(name, "int", helpText, opts),
		cfg: c,
	}
}

// LastInt receives the last argument as an int
func (c *Config) LastInt(name, helpText string, opts ...func(*Option)) IntGetter {
	//c.lastArgName = name
	opt := c.MustNewOption("", "int", helpText, opts)
	opt.LastArgName = name
	return IntGetter{
		opt: opt,
		cfg: c,
	}
}

// shortcut for MustNewOption of type float64
func (c *Config) Float(name, helpText string, opts ...func(*Option)) FloatGetter {
	return FloatGetter{
		opt: c.MustNewOption(name, "float64", helpText, opts),
		cfg: c,
	}
}

// LastFloat receives the last argument as a float32
func (c *Config) LastFloat(name, helpText string, opts ...func(*Option)) FloatGetter {
	//c.lastArgName = name
	opt := c.MustNewOption("", "float64", helpText, opts)
	opt.LastArgName = name
	return FloatGetter{
		opt: opt,
		cfg: c,
	}
}

// shortcut for MustNewOption of type string
func (c *Config) String(name, helpText string, opts ...func(*Option)) StringGetter {
	return StringGetter{
		opt: c.MustNewOption(name, "string", helpText, opts),
		cfg: c,
	}
}

// LastString receives the last argument as a string
func (c *Config) LastString(name, helpText string, opts ...func(*Option)) StringGetter {
	//c.lastArgName = name
	opt := c.MustNewOption("", "string", helpText, opts)
	opt.LastArgName = name
	return StringGetter{
		opt: opt,
		cfg: c,
	}
}

// shortcut for MustNewOption of type datetime
func (c *Config) DateTime(name, helpText string, opts ...func(*Option)) DateTimeGetter {
	return DateTimeGetter{
		opt: c.MustNewOption(name, "datetime", helpText, opts),
		cfg: c,
	}
}

// LastDateTime receives the last argument as a datetime
func (c *Config) LastDateTime(name, helpText string, opts ...func(*Option)) DateTimeGetter {
	//c.lastArgName = name
	opt := c.MustNewOption("", "datetime", helpText, opts)
	opt.LastArgName = name
	return DateTimeGetter{
		opt: opt,
		cfg: c,
	}
}

func (c *Config) Date(name, helpText string, opts ...func(*Option)) DateGetter {
	return DateGetter{
		opt: c.MustNewOption(name, "date", helpText, opts),
		cfg: c,
	}
}

// LastDate receives the last argument as a date
func (c *Config) LastDate(name, helpText string, opts ...func(*Option)) DateGetter {
	//c.lastArgName = name
	opt := c.MustNewOption("", "date", helpText, opts)
	opt.LastArgName = name
	return DateGetter{
		opt: opt,
		cfg: c,
	}
}

func (c *Config) Time(name, helpText string, opts ...func(*Option)) TimeGetter {
	return TimeGetter{
		opt: c.MustNewOption(name, "time", helpText, opts),
		cfg: c,
	}
}

// LastTime receives the last argument as a time
func (c *Config) LastTime(name, helpText string, opts ...func(*Option)) TimeGetter {
	//c.lastArgName = name
	opt := c.MustNewOption("", "time", helpText, opts)
	opt.LastArgName = name
	return TimeGetter{
		opt: opt,
		cfg: c,
	}
}

// shortcut for MustNewOption of type json
func (c *Config) JSON(name, helpText string, opts ...func(*Option)) JSONGetter {
	return JSONGetter{
		opt: c.MustNewOption(name, "json", helpText, opts),
		cfg: c,
	}
}

func Required() func(*Option) {
	return func(o *Option) { o.Required = true }
}

func Default(val interface{}) func(*Option) {
	return func(o *Option) { o.Default = val }
}

func Shortflag(s rune) func(*Option) {
	return func(o *Option) { o.Shortflag = string(s) }
}

/*
TODO
create this function to allow handling of stdin
func (c *Config) MustHandleStdIn(helpText string, opts []func(*Option)) *Option {

}
*/

// panics for invalid values
func (c *Config) MustNewOption(name, type_, helpText string, opts []func(*Option)) *Option {
	o, err := c.NewOption(name, type_, helpText, opts)
	if err != nil {
		panic(err)
	}
	return o
}

// adds a new option
func (c *Config) NewOption(name, type_, helpText string, opts []func(*Option)) (*Option, error) {
	o := &Option{Name: name, Type: type_, Help: helpText}

	for _, s := range opts {
		s(o)
	}

	c.addToArgsOrder(name)

	if o.Name == "" && o.Shortflag != "" {
		return nil, fmt.Errorf("shortflag is not allowed for last arg")
	}

	if o.Name == "" && o.Default != nil {
		return nil, fmt.Errorf("default value is not allowed for last arg")
	}

	if err := o.Validate(); err != nil {
		return nil, err
	}

	if err := c.addOption(o); err != nil {
		return nil, err
	}
	return o, nil
}

type Option struct {
	// Name must consist of words that are joined by the underscore character _
	// Each word must consist of uppercase letters [A-Z] and may have numbers
	// A word must consist of two ascii characters or more.
	// A name must at least have one word
	// If the option is the flag-less last argument, Name is empty
	Name string `json:"name"`

	// Required indicates, if the Option is required
	Required bool `json:"required"`

	// Type must be one of "bool","int","float64","string","datetime","json", "dir", "file", "url"
	Type string `json:"type"`

	// The Help string is part of the documentation
	Help string `json:"help"`

	// The Default value for the Config. The value might be nil for optional Options.
	// Otherwise, it must have the same type as the Type property indicates
	Default interface{} `json:"default,omitempty"`

	// A Shortflag for the Option. Shortflags may only be used for commandline flags
	// They must be a single lowercase ascii character
	Shortflag string `json:"shortflag,omitempty"`

	// LastArgName is only set if the Option is the flag-less last argument (then Name is empty)
	LastArgName string `json:"lastargname,omitempty"`
}

func (c Option) flagName() string {
	if c.Name == "" {
		name := c.LastArgName
		if name == "" {
			name = c.Type
		}
		if c.Required {
			return "<" + name + ">"
		} else {
			return "[" + name + "]"
		}
	}

	return "--" + c.Name
}

// ValidateDefault checks if the default value is valid.
// If it does, nil is returned, otherwise
// ErrInvalidDefault is returned or a json unmarshalling error if the type is json
func (c Option) ValidateDefault() error {
	if c.Default == nil {
		return nil
	}
	err := c.ValidateValue(c.Default)
	if err != nil {
		return InvalidDefault{c}
	}
	return nil
}

// ValidateValue checks if the given value is valid.
// If it does, nil is returned, otherwise
// ErrInvalidValue is returned or a json unmarshalling error if the type is json
func (c Option) ValidateValue(val interface{}) error {
	invalidErr := InvalidValueError{c, val}
	// value may only be nil for optional Options
	if val == nil && c.Required {
		return invalidErr
	}

	if val == nil {
		return nil
	}
	switch ty := val.(type) {
	case bool:
		if c.Type != "bool" {
			return invalidErr
		}
	case int:
		if c.Type != "int" {
			return invalidErr
		}
	case float64:
		if c.Type != "float64" {
			return invalidErr
		}

	case string:
		switch c.Type {

		case "date":
			_, err := time.Parse(DateFormat, ty)
			return err

		case "datetime":
			_, err := time.Parse(DateTimeFormat, ty)
			return err

		case "time":
			_, err := time.Parse(TimeFormat, ty)
			return err

		case "string":
			return nil

		case "json":
			var v any
			return json.Unmarshal([]byte(ty), &v)

		case "dir":
			_, err := path.ParseDirFromSystem(ty)
			return err

		case "file":
			_, err := path.ParseFileFromSystem(ty)
			return err

		case "url":
			_, err := path.ParseRemote(ty)
			return err

		default:
			return invalidErr
		}

	case path.Local:
		switch c.Type {
		case "dir", "file":
			return nil
		default:
			return invalidErr
		}

	case *url.URL:
		if c.Type != "url" {
			return invalidErr
		}
		return nil

	case time.Time:

		switch c.Type {
		case "date", "time", "datetime":
			// ok
		default:
			return invalidErr
		}

	default:
		return invalidErr
	}
	return nil
}

// Validate checks if the Option is valid.
// If it does, nil is returned, otherwise
// the error is returned
func (c Option) Validate() error {
	if c.Name != "" {
		if err := ValidateName(c.Name); err != nil {
			return err
		}
	}
	if err := ValidateType(c); err != nil {
		return err
	}
	if err := c.ValidateDefault(); err != nil {
		return err
	}
	if c.Help == "" {
		return ErrMissingHelp
	}
	return nil
}
