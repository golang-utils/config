# Layout

All man pages follow a common layout that is optimized for presentation on a simple ASCII text display, possibly without any form of highlighting or font control. Sections present may include:[21]: MANUAL STRUCTURE 

## NAME

The name of the command or function, followed by a one-line description of what it does.

## SYNOPSIS

In the case of a command, a formal description of how to run it and what command line options it takes. For program functions, a list of the parameters the function takes and which header file contains its declaration.

## DESCRIPTION

A textual description of the functioning of the command or function.

## EXAMPLES

Some examples of common usage.

## SEE ALSO

A list of related commands or functions.

-------------
Other sections may be present, but these are not well standardized across man pages. Common examples include: OPTIONS, EXIT STATUS, RETURN VALUE, ENVIRONMENT, BUGS, FILES, AUTHOR, REPORTING BUGS, HISTORY and COPYRIGHT.

