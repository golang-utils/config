package man

import (
	"bytes"
	"errors"
	"fmt"
	"io"
	"sort"
	"strings"
	"time"

	"gitlab.com/golang-utils/fmtdate"
)

// ManPage represents a man page generator.
type ManPage struct {
	Root          Command
	Version       string
	sections      []Section
	Concept       string
	License       string
	Bugs          string
	Files         string
	Authors       []string
	ReportingBugs string
	History       string
	Copyright     string
	Documentation string
}

// stolen from https://github.com/muesli/roff/blob/main/roff.go
func escapeText(s string) string {
	s = strings.ReplaceAll(s, `\`, `\e`)
	s = strings.ReplaceAll(s, ".", "\\&.")
	return s
}

// stolen from https://github.com/muesli/roff/blob/main/roff.go
func Text(text string) string {
	var bf bytes.Buffer
	inList := false
	for i, s := range strings.Split(text, "\n") {
		if i > 0 && !inList {
			// start a new paragraph if we're not in a list
			bf.WriteString("\n.PP\n")
		}

		if strings.HasPrefix(s, "*") {
			// list item
			if !inList {
				// start a new indented list if we're not in one
				bf.WriteString("\n.RS -1")
				inList = true
			}

			bf.WriteString(fmt.Sprintf("\n.IP"+" \\(bu 3\n%s", escapeText(strings.TrimSpace(s[1:]))))
		} else {
			// regular text
			if inList {
				// end the list if we're in one
				bf.WriteString("\n.RE")
				inList = false
			}

			bf.WriteString(escapeText(s))
		}
	}

	return bf.String()
}

// Command represents a command.
type Command struct {
	Name        string
	isRoot      bool
	description string
	synopsis    string
	Example     string
	flags       map[string]Flag
	subCommands map[string]*Command
}

func (c *Command) writeTo(mp *ManPage, wr io.Writer) {
	if !c.isRoot {
		fmt.Fprintf(wr, `.SH %q`+"\n", "Command "+strings.ToUpper(c.Name))
		fmt.Fprintln(wr, ".PP")
		fmt.Fprintln(wr, Text(c.description))
		fmt.Fprintln(wr, ".PP")
		fmt.Fprintln(wr, `\fBSYNOPSIS\fR`)
		fmt.Fprintln(wr, ".RS 4")
		fmt.Fprintln(wr, Text(c.synopsis))
		fmt.Fprintln(wr, ".RE")
		fmt.Fprintln(wr, ".PP")

		if len(c.flags) > 0 {
			fmt.Fprintln(wr, `\fBOPTIONS\fR`)
			fmt.Fprintln(wr, ".RS 4")

			names := []string{}

			for k := range c.flags {
				names = append(names, k)
			}

			sort.Strings(names)

			for _, name := range names {
				fl := c.flags[name]
				fl.writeTo(wr)
			}

			fmt.Fprintln(wr, ".RE")
			fmt.Fprintln(wr, ".PP")
		}

		if c.Example != "" {
			fmt.Fprintln(wr, `\fBEXAMPLE\fR`)
			fmt.Fprintln(wr, ".RS 4")
			fmt.Fprintln(wr, Text(c.Example))
			fmt.Fprintln(wr, ".RE")
		}

	} else {
		fmt.Fprintln(wr, ".SH NAME")
		fmt.Fprintf(wr, `\fB%s\fR`+"\n", escapeText(c.Name))
		fmt.Fprintln(wr, ".SH SYNOPSIS")
		fmt.Fprintln(wr, Text(c.synopsis))
		fmt.Fprintln(wr, ".SH DESCRIPTION")
		fmt.Fprintln(wr, Text(c.description))

		if len(c.flags) > 0 {
			fmt.Fprintln(wr, ".SH OPTIONS")
			//	fmt.Fprintln(wr, ".RS 4")

			names := []string{}

			for k := range c.flags {
				names = append(names, k)
			}

			sort.Strings(names)

			for _, name := range names {
				fl := c.flags[name]
				fl.writeTo(wr)
			}

			//fmt.Fprintln(wr, ".RE")
			fmt.Fprintln(wr, ".PP")
		}
		/*
		   .SH NAME
		   \fBscaffold-v1\&.8\&.9\fR
		   .SH SYNOPSIS
		   scaffold [command] OPTION\&.\&.\&.
		   .SH DESCRIPTION
		   scaffold creates files and directories based on a template and json input.
		   .SH OPTIONS
		   \fB[-t, --template='scaff\&.templ']\fR
		   .RS 4
		   the file where the template resides
		   .RE
		   .PP

		*/

		if mp.Concept != "" {
			fmt.Fprintln(wr, ".SH CONCEPT")
			fmt.Fprintln(wr, Text(mp.Concept))
		}

		if c.Example != "" {
			fmt.Fprintln(wr, ".SH EXAMPLE")
			fmt.Fprintln(wr, Text(c.Example))
		}
	}

	if len(c.subCommands) > 0 {
		fmt.Fprintln(wr, ".SH COMMANDS")

		names := []string{}

		for k := range c.subCommands {
			names = append(names, k)
		}

		sort.Strings(names)

		var bf bytes.Buffer

		for _, name := range names {
			fmt.Fprintf(&bf, `* %s - %s`+"\n", name, c.subCommands[name].description)
		}

		fmt.Fprintln(wr, Text(bf.String()))
		fmt.Fprintln(wr, ".PP")

		for _, name := range names {
			subc := c.subCommands[name]
			subc.writeTo(mp, wr)
			//fmt.Fprintln(wr, ".PP")
		}

	}

	/*
	   .SH "COMMAND config"
	   .sp 2
	   .PP
	   show the configuration of scaffold
	   .PP
	   \fBSYNOPSIS\fR
	   .RS 4
	   scaffold config OPTION\&.\&.\&. <action>
	   .RE
	   .PP
	   \fBOPTIONS\fR
	   .RS 4
	*/
}

// Flag represents a flag.
type Flag struct {
	Name        string
	EnvVar      string
	Syntax      string
	Description string
}

func (f Flag) writeTo(wr io.Writer) {
	fmt.Fprintf(wr, `\fB%s\fR`+"\n", escapeText(f.Syntax))
	fmt.Fprintln(wr, ".PP")
	fmt.Fprintf(wr, `\fB%s\fR`+"\n", escapeText(f.EnvVar))
	fmt.Fprintln(wr, ".PP")
	fmt.Fprintln(wr, ".RS 4")
	fmt.Fprintln(wr, Text(f.Description))
	fmt.Fprintln(wr, ".RE")
	fmt.Fprintln(wr, ".PP")
	fmt.Fprintln(wr, ".PP")

	/*
	   .SH OPTIONS
	   \fB[-t, --template='scaff\&.templ']\fR
	   .RS 4
	   the file where the template resides
	   .RE
	   .PP
	   \fB[-d, --data=”]\fR
	   .RS 4
	   the file in JSON format that will be fed into the template\&. [STDIN] signifies that the data
	   .RE
	   .PP
	   \fB[--dir='\&.']\fR
	   .RS 4
	   directory that is the target/root of the file creations
	   .RE
	*/
}

func NewFlag(name, syntax, descr, envvar string) Flag {
	return Flag{
		Name:        name,
		EnvVar:      envvar,
		Syntax:      syntax,
		Description: descr,
	}
}

// Section represents a section.
type Section struct {
	Name string
	Text string
}

// NewManPage returns a new ManPage generator instance.
func NewManPage(name string, version string, synopsis, description string) *ManPage {
	root := NewCommand(name, synopsis, description)
	root.isRoot = true
	return &ManPage{
		Root:    *root,
		Version: version,
	}
}

func (m *ManPage) WriteTo(wr io.Writer) {
	fmt.Fprintf(wr,
		`.TH %[1]s %[2]d "%[4]s" "%[3]s" "%[5]s"`+"\n",
		strings.ToUpper(escapeText(m.Root.Name)),
		1,
		escapeText(m.Root.Name),
		escapeText(fmtdate.FormatDate(time.Now())),
		escapeText(m.Root.Name)+"-v"+escapeText(m.Version))

	if m.Copyright != "" {
		fmt.Fprintln(wr, `.SH COPYRIGHT`)
		fmt.Fprintln(wr, escapeText(m.Copyright))
	}

	if m.License != "" {
		fmt.Fprintln(wr, `.SH LICENSE`)
		fmt.Fprintln(wr, Text(m.License))
	}

	if m.ReportingBugs != "" {
		fmt.Fprintln(wr, `.SH REPORTING BUGS`)
		fmt.Fprintln(wr, Text(m.ReportingBugs))
	}

	m.Root.writeTo(m, wr)

	for _, sec := range m.sections {
		fmt.Fprintf(wr, `.SH %q`+"\n", sec.Name)
		fmt.Fprintln(wr, Text(sec.Text))
	}

	if m.Documentation != "" {
		fmt.Fprintln(wr, `.SH DOCUMENTATION`)
		fmt.Fprintln(wr, Text(m.Documentation))
	}

	if m.Bugs != "" {
		fmt.Fprintln(wr, `.SH BUGS`)
		fmt.Fprintln(wr, Text(m.Bugs))
	}

	if m.Files != "" {
		fmt.Fprintln(wr, `.SH FILES`)
		fmt.Fprintln(wr, Text(m.Files))
	}

	if m.History != "" {
		fmt.Fprintln(wr, `.SH HISTORY`)
		fmt.Fprintln(wr, Text(m.History))
	}

	if len(m.Authors) > 0 {
		fmt.Fprintln(wr, `.SH AUTHORS`)
		var bf bytes.Buffer

		for _, auth := range m.Authors {
			fmt.Fprintf(&bf, `* %s`+"\n", auth)
		}

		fmt.Fprintln(wr, Text(bf.String()))
		fmt.Fprintln(wr, ".PP")
	}

}

// NewCommand returns a new Command.
func NewCommand(name string, synopsis, description string) *Command {
	return &Command{
		Name:        name,
		description: description,
		synopsis:    synopsis,
		flags:       make(map[string]Flag),
		subCommands: make(map[string]*Command),
	}
}

// AddSection adds a section to the man page.
func (m *ManPage) AddSection(section string, text string) {
	m.sections = append(m.sections, Section{
		Name: section,
		Text: text,
	})
}

// AddFlag adds a flag to the command.
func (m *Command) AddFlag(f Flag) error {
	if _, found := m.flags[f.Name]; found {
		return errors.New("duplicate flag: " + f.Name)
	}

	m.flags[f.Name] = f
	return nil
}

// AddCommand adds a sub-command.
func (m *Command) AddSubCommand(c *Command) error {
	if _, found := m.subCommands[c.Name]; found {
		return errors.New("duplicate subCommands: " + c.Name)
	}

	m.subCommands[c.Name] = c
	return nil
}
