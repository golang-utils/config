package config

import (
	"bytes"
	"fmt"
	"path/filepath"
	"strings"

	"gitlab.com/golang-utils/config/v2/man"
	"gitlab.com/golang-utils/fs/path"
)

/*
manPage := mango.NewManPage(1, "mango", "a man-page generator").
        WithLongDescription("mango is a man-page generator for Go.\n"+
            "Features:\n"+
            "* User-friendly\n"+
            "* Plugable").
        WithSection("Copyright", "(C) 2022 Christian Muehlhaeuser.\n"+
            "Released under MIT license.")
*/

func (c *Config) ManPage() string {
	// https://www.cyberciti.biz/faq/howto-use-linux-unix-man-pages/#:~:text=man%20page%20sections&text=Section%20%23%201%20%3A%20User%20command%20(,(usually%20found%20in%20%2Fdev)
	// executable programs or shell commands are user commands and always section 1
	manPage := man.NewManPage(c.appName(), c.version.String(), c.synopsis(), c.helpIntro)

	if c.mango.example != "" {
		manPage.Root.Example = c.mango.example
	}

	if c.mango.concept != "" {
		manPage.Concept = c.mango.concept
	}

	if c.mango.bugs != "" {
		manPage.Bugs = c.mango.bugs
	}

	if len(c.mango.authors) > 0 {
		manPage.Authors = c.mango.authors
	}

	if c.mango.reportingBugs != "" {
		manPage.ReportingBugs = c.mango.reportingBugs
	}

	if c.mango.documentation != "" {
		manPage.Documentation = c.mango.documentation
	}

	if c.mango.history != "" {
		manPage.History = c.mango.history
	}

	if c.mango.copyright != "" {
		manPage.Copyright = c.mango.copyright
	}

	if c.mango.license != "" {
		manPage.License = c.mango.license
	}

	manPage.Root.Name = c.appName()
	manPage.Files = fmt.Sprintf(`configuration files:
* global: %s
* user: %s
* local: %s
`,
		path.ToSystem(c.FirstGlobalsFile()),
		filepath.Join(generalUserConfigDir(), c.appName(), c.appName()+CONFIG_EXT),
		filepath.Join(".", CONFIG_DIR, c.appName(), c.appName()+CONFIG_EXT),
	)

	c.addFlags(&manPage.Root)
	c.addCommands(&manPage.Root)
	var bf bytes.Buffer
	manPage.WriteTo(&bf)
	return bf.String()
}

func (c *Config) addCommands(mc *man.Command) {
	for _, data := range c.commands {
		switch data {
		// skip the integrated commands
		case c.cmdConfig, c.cmdHelp, c.cmdMan, c.cmdVersion:
			continue
		}

		var ms = man.NewCommand(data.commandName(), data.synopsis(), strings.TrimSpace(data.helpIntro))
		ms.Example = data.mango.example
		data.addFlags(ms)

		err := mc.AddSubCommand(ms)
		if err != nil {
			panic(err.Error())
		}
	}

}

func (c *Config) addFlags(mc *man.Command) {

	finalargs := c.finalArgs()

	for n, data := range finalargs {
		syntax := c.argSyntax(n)

		var name = data.Name
		if name == "" {
			name = data.LastArgName
		}
		var m = man.NewFlag(name, syntax, strings.TrimSpace(data.Help), c.env_var2(name))

		// makes it --name instead of -name
		//m.PFlag = true
		err := mc.AddFlag(m)
		if err != nil {
			panic(err.Error())
		}
	}

}
