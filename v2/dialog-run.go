package config

import (
	"fmt"
	"strings"

	"gitlab.com/golang-utils/dialog"
)

/*
TODO

  wir brauchen ein Dialog interface zum setzen der optionen bei der ausführung (statt der commando zeile)
  könnte unter dem command ui voreingestellt werden. hei was wäre das geil...

  im Grunde kann das recht einfach funktionieren:

	- es wird normal geladen (außer die args)
	- es wird zunächst der command gewählt (wenn welche vorhanden sind)
	- dann werden alle nicht belegten, obligatorischen optionen abgefragt
	- dann gibt es noch die möglichkeit, andere optionen zu sehen (aus dem gleichen command) und ggf. zu ändern
	- dann wird die volle config (zum gewählten command) angezeigt und kann dann ausgeführt werden (oder wieder geändert,
	  oder abgebrochen)
	- bei abbruch wird das programm verlassen, ansonsten werden die optionen entsprechend gesetzt
	  und das programm setzt fort, als wäre hätte es die argumente aus der shell bekommen.

 - wenn irgendwie geprüft werden kann, ob das programm aus einer shell heraus, oder durch doppelklick
   gestartet wurde, könnte man das verwenden, um die GUI entsprechend an oder abzuschalten
   dazu muss dann auch dieser windows-hack zum doppelklick reingebracht werden

*/

func (c *Config) newDialogRun() *dialogRun {
	var opts []dialog.Option

	opts = append(opts,
		//	dialog.WithBackNavigation(),
		dialog.WithFooter(),
		dialog.WithHeader(),
		dialog.WithDefaultGui(),
		//dialog.WithBreadcrumb(),
	)

	a := newDialog("configuration of "+c.appName(), opts...)

	return &dialogRun{
		config: c,
		app:    a,
		values: map[[2]string]string{},
	}
}

type dialogRun struct {
	config             *Config
	app                dialog.App
	currentCommand     *Config
	currentCommandName string
	currentCommandHelp string
	currentOption      *Option
	currentValue       string
	currentValueType   string
	currentValueHelp   string

	// 0-command 1-option: value
	values map[[2]string]string
}

func (d *dialogRun) validateValue(s string) error {
	v, err := stringToValue(d.currentOption.Type, s)

	if err != nil {
		return err
	}
	return d.currentOption.ValidateValue(v)
}

func (d *dialogRun) Run() error {
	d.currentCommand = d.config
	return d.app.Run(d.askForCommand())
}

func (d *dialogRun) context() string {
	var bd strings.Builder

	if d.currentCommandName != "" {
		bd.WriteString("    command: '" + d.currentCommandName + "'\n")
	}

	if d.currentCommandHelp != "" {
		bd.WriteString("    command info: '" + d.currentCommandHelp + "'\n")
	}

	if d.currentOption != nil {
		bd.WriteString("    option: '" + d.currentOption.Name + "'\n")
	}

	if d.currentValue != "" {
		bd.WriteString("    value: '" + d.currentValue + "'\n")
	}

	if d.currentValueType != "" {
		bd.WriteString("    type: '" + d.currentValueType + "'\n")
	}

	if d.currentValueHelp != "" {
		bd.WriteString("    help: '" + d.currentValueHelp + "'\n")
	}

	return bd.String() + "\n"
}

func (d *dialogRun) loadValues() (err error) {
	//d.config.Load()

	d.config.EachValue(func(name string, value interface{}) {
		// lastarg
		if name == "" {
			name = d.config.lastArgName()
		}
		d.values[[2]string{"", name}] = fmt.Sprintf("%v", value)
	})

	for cname, cmd := range d.config.commands {
		if skipCommands[cname] {
			continue
		}

		cmd.EachValue(func(name string, value interface{}) {
			// lastarg
			if name == "" {
				name = cmd.lastArgName()
			}
			d.values[[2]string{cname, name}] = fmt.Sprintf("%v", value)
		})
	}

	return nil
}

func (d *dialogRun) askForCommand() dialog.Screen {
	d.currentCommandName = ""
	d.currentOption = nil
	d.currentCommandHelp = ""
	d.currentValue = ""
	d.currentValueType = ""
	d.currentValueHelp = ""

	question := d.context() + "Please select the (sub)command"
	without := "--without command--"
	var menu = []string{without}

	for name := range d.config.commands {
		if skipCommands[name] {
			continue
		}
		menu = append(menu, name)
	}

	sc := d.app.NewSelect(question, menu, func(chosen string) dialog.Screen {
		if chosen == without {
			d.currentCommandName = ""
			d.currentCommand = d.config
			d.currentCommandHelp = ""
		} else {
			d.currentCommandName = chosen
			d.currentCommand = d.config.commands[chosen]
			d.currentCommandHelp = shorten(d.currentCommand.helpIntro)
		}

		return d.askForOption()
	})

	sc.SetOptions(
		dialog.WithValidator(dialog.NonEmpty),
		dialog.WithPrefilled(without),
	)

	return sc
}

func (d *dialogRun) askForOption() dialog.Screen {
	d.currentOption = nil
	d.currentValue = ""
	d.currentValueType = ""
	d.currentValueHelp = ""

	question := d.context() + "Please select the option/flag"
	var menu = []string{}

	for name := range d.currentCommand.spec {
		// don't use lastargs
		if name != "" {
			menu = append(menu, name)
		}
	}

	if len(menu) == 0 {
		return d.app.NewError("no option found")
	}

	sc := d.app.NewSelect(question, menu, func(chosen string) dialog.Screen {
		if chosen == d.currentCommand.lastArgName() {
			chosen = ""
		}
		d.currentOption = d.currentCommand.spec[chosen]
		d.currentValue = shorten(d.currentVal())
		d.currentValueType = d.currentOption.Type
		d.currentValueHelp = shorten(d.currentOption.Help)

		return d.askForOptionAction()
	})

	sc.SetOptions(
		dialog.WithValidator(dialog.NonEmpty),
		dialog.WithPrefilled(menu[0]),
	)

	return sc
}

func (d *dialogRun) askForOptionAction() dialog.Screen {
	question := d.context() + "What do you want to do with the option/flag?"
	var menu = []string{
		"1) show value",
		"2) show help",
		"3) set value",
		"4) back to general actions",
	}

	sc := d.app.NewSelect(question, menu, func(chosen string) dialog.Screen {
		switch chosen {
		case "1) show value":
			return d.showValue()
		case "2) show help":
			return d.showHelp()
		case "3) set value":
			return d.changeValue()
		case "4) back to general actions":
			return d.askForAction()
		default:
			panic("must not happen")
		}
	})

	sc.SetOptions(
		dialog.WithValidator(dialog.NonEmpty),
		dialog.WithPrefilled("3) set value"),
	)

	return sc
}

func (d *dialogRun) askForAction() dialog.Screen {
	question := d.context() + "What do you want to do"
	var menu = []string{
		"1) show all values",
		"2) reset all values",
		"3) select option/flag",
		"4) select command",
		"5) execute program with current options",
	}

	sc := d.app.NewSelect(question, menu, func(chosen string) dialog.Screen {
		switch chosen {
		case "1) show all values":
			return d.showValues()
		case "3) select option/flag":
			return d.askForOption()
		case "4) select command":
			return d.askForCommand()
		case "2) reset all values":
			return d.resetValues()
		case "5) execute program with current options":
			return d.runProgram()
		default:
			panic("must not happen")
		}
	})

	sc.SetOptions(
		dialog.WithValidator(dialog.NonEmpty),
		dialog.WithPrefilled("1) show all values"),
	)

	return sc
}

func (d *dialogRun) inspectValues() string {
	var bd strings.Builder

	m := map[string]map[string]string{}

	for k, v := range d.values {
		mm := m[k[0]]
		if mm == nil {
			mm = map[string]string{}
		}
		mm[k[1]] = v

		m[k[0]] = mm
	}

	if len(m[""]) > 0 {
		bd.WriteString("\n-- no command --\n")
	}

	for k, v := range m[""] {
		bd.WriteString("    " + k + ": " + v + "\n")
	}

	for kk, mm := range m {
		if kk == "" {
			continue
		}

		bd.WriteString("\ncommand: '" + kk + "' --\n")

		for k, v := range mm {
			bd.WriteString("    " + k + ": " + v + "\n")
		}
	}

	return bd.String() + "\n"
}

func (d *dialogRun) showValues() dialog.Screen {
	question := "Here are the current values:\n\n" + d.inspectValues() + "\npress ENTER to continue"
	sc := d.app.NewOutput(question, d.askForAction)

	sc.SetOptions()

	return sc
}

func (d *dialogRun) resetValues() dialog.Screen {
	d.values = map[[2]string]string{}
	d.Run()
	d.loadValues()
	question := "The values are deleted " + "\npress ENTER to continue"
	sc := d.app.NewOutput(question, d.askForAction)

	sc.SetOptions()

	return sc
}

func (d *dialogRun) currentVal() string {
	if d.currentCommand == nil {
		return ""
	}
	if d.currentOption == nil {
		return ""
	}
	return d.values[[2]string{d.currentCommandName, d.currentOption.Name}]
}

func (d *dialogRun) showValue() dialog.Screen {
	question := d.context() + "value is: \n" + d.currentVal() + "\n"
	sc := d.app.NewOutput(question, func() dialog.Screen {
		return d.askForOptionAction()
	})

	sc.SetOptions()

	return sc
}

func (d *dialogRun) showHelp() dialog.Screen {
	question := d.context() + "Help: \n" + d.currentOption.Help + "\n"
	sc := d.app.NewOutput(question, func() dialog.Screen {
		return d.askForOptionAction()
	})

	sc.SetOptions()

	return sc
}

func (d *dialogRun) changeValue() dialog.Screen {
	d.currentValue = ""
	question := d.context() + "value:"
	sc := d.app.NewInput(question, func(inp string) dialog.Screen {
		d.currentValue = shorten(inp)
		name := d.currentOption.Name
		d.values[[2]string{d.currentCommandName, name}] = inp
		return d.askForOptionAction()
	})

	var valid dialog.ValidatorFunc = func(vs interface{}) error {
		return d.validateValue(vs.(string))
	}

	sc.SetOptions(
		dialog.WithValidator(valid),
		dialog.WithPrefilled(d.currentVal()),
	)

	return sc
}

func (d *dialogRun) runProgram() dialog.Screen {
	sc := d.app.NewOutput("If you press enter the program will be executed", func() dialog.Screen {
		return nil
	})

	sc.SetOptions()

	return sc
}
