package main

import (
	"fmt"
	"os"
	"gitlab.com/golang-utils/config/v2"
	"#progname/lib/#progname"
)

var (
	cfg = config.New("#progname", config.Version{0,0,1}, "here comes the description")
	arg = cfg.String("arg", "description of arg", config.Required(), config.Shortflag('a'))
	
	cmd1 = cfg.Command("cmd", "description of command")
	argCmd1 = cmd1.Int("no", "description of number")
)

func main() {
	err := run()
	
	if err != nil {
		fmt.Fprintf(os.Stderr, "ERROR: %v", err)
		os.Exit(1)
	}
	
	os.Exit(0)
}

func run() error {
	
	err := cfg.Run()
	
	if err != nil {
		return err
	}
	
	switch cfg.ActiveCommand() {
	case cmd1:
		return doCmd1()
	case nil:
		_ = #progname.New()
	default:
	}
	
	return nil
}

func doCmd1() error {
	return nil
}