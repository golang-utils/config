package config

import (
	"fmt"
	"os"
	"reflect"
	"runtime"
	"time"

	"github.com/schollz/progressbar/v3"
)

func getFunctionName(i interface{}) string {
	return runtime.FuncForPC(reflect.ValueOf(i).Pointer()).Name()
}

// NewSpinner returns a progressbar of github.com/schollz/progressbar/v3
// that is a spinner
// Output defaults to stdout
func NewSpinner(description string, options ...progressbar.Option) *progressbar.ProgressBar {
	var opts = []progressbar.Option{
		progressbar.OptionSetWriter(os.Stdout),
		progressbar.OptionClearOnFinish(),
		progressbar.OptionSetRenderBlankState(true),
		progressbar.OptionSetElapsedTime(false),
		progressbar.OptionSetDescription(description),
	}
	opts = append(opts, options...)
	return progressbar.NewOptions64(-1, opts...)
}

// NewProgressbar returns a progressbar of github.com/schollz/progressbar/v3
// Output defaults to stderr
func NewProgressbar(max int64, description string, options ...progressbar.Option) *progressbar.ProgressBar {
	var opts = []progressbar.Option{
		progressbar.OptionSetWriter(os.Stderr),
		progressbar.OptionSetWidth(10),
		progressbar.OptionThrottle(65 * time.Millisecond),
		progressbar.OptionShowCount(),
		progressbar.OptionShowIts(),
		progressbar.OptionOnCompletion(func() {
			fmt.Fprint(os.Stderr, "\n")
		}),
		progressbar.OptionSpinnerType(14),
		progressbar.OptionFullWidth(),
		progressbar.OptionSetRenderBlankState(true),
		progressbar.OptionSetDescription(description),
	}

	opts = append(opts, options...)
	return progressbar.NewOptions64(max, opts...)
}

// NewProgressbarBytes is NewProgressbar for bytes
func NewProgressbarBytes(maxBytes int64, description string, options ...progressbar.Option) *progressbar.ProgressBar {
	options = append(options, progressbar.OptionShowBytes(true))
	return NewProgressbar(maxBytes, description, options...)
}
