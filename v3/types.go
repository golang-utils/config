package config

import (
	"encoding/json"
	"errors"
	"fmt"
	"path/filepath"
	"strconv"
	"strings"
	"time"

	"gitlab.com/golang-utils/fmtdate"
)

type TypeHandler[T any] interface {
	Parse(s string) (T, error)
	ToString(T) string
	Type() string
}

type String struct{}

var _ TypeHandler[string] = String{}

func (String) ToString(s string) string {
	return s
}

func (String) Type() string {
	return "<string>"
}

func (String) Parse(s string) (string, error) {
	return s, nil
}

type Json map[string]any

func (j Json) String() string {
	bt, err := json.Marshal(j)

	if err != nil {
		return err.Error()
	}

	return string(bt)
}

func (t Json) MustParse(s string) Json {
	j, err := t.Parse(s)
	if err != nil {
		panic(err.Error())
	}
	return j
}

func (Json) Parse(s string) (Json, error) {
	m := Json{}
	err := json.Unmarshal([]byte(s), &m)
	if err != nil {
		return nil, err
	}

	return m, nil
}

func (Json) ToString(s Json) string {
	return s.String()
}

func (Json) Type() string {
	return "<json-map>"
}

var _ TypeHandler[Json] = Json{}

type Path struct {
	File string
	Dirs []string
	Ext  string
}

func (t Path) String() string {
	dir := strings.Join(t.Dirs, "/")
	s := filepath.Join(dir, t.File)
	return filepath.FromSlash(s)
}

func (Path) Parse(s string) (Path, error) {
	var t Path
	s = filepath.ToSlash(s)
	t.File = filepath.Base(s)
	dir := filepath.Dir(s)
	t.Dirs = strings.Split(dir, string(filepath.Separator))
	if t.File != "" {
		t.Ext = strings.ToLower(filepath.Ext(t.File))
		if len(t.Ext) > 1 {
			t.Ext = t.Ext[1:]
		}
	}
	return t, nil
}

func (Path) ToString(s Path) string {
	return s.String()
}

func (Path) Type() string {
	return "<path>"
}

var _ TypeHandler[Path] = Path{}

/*
type CustomType interface {
	Parse(s string) (CustomType, error)
	String() string
}
*/

type Int struct{}

func (Int) Parse(s string) (int, error) {
	return strconv.Atoi(s)
}

var _ TypeHandler[int] = Int{}

func (Int) ToString(s int) string {
	return fmt.Sprintf("%v", s)
}

func (Int) Type() string {
	return "<int>"
}

var _ TypeHandler[float64] = Float{}

type Float struct{}

func (Float) Parse(s string) (float64, error) {
	return strconv.ParseFloat(s, 64)
}

func (Float) ToString(s float64) string {
	return fmt.Sprintf("%v", s)
}

func (Float) Type() string {
	return "<float>"
}

var _ TypeHandler[bool] = Bool{}

type Bool struct{}

func (Bool) Parse(s string) (bool, error) {
	switch strings.ToLower(s) {
	case "", "on", "t", "true":
		return true, nil
	case "off", "f", "false":
		return false, nil
	default:
		return false, fmt.Errorf("invalid value for boolean: %s", s)
	}
}

func (Bool) ToString(s bool) string {
	return fmt.Sprintf("%v", s)
}

func (Bool) Type() string {
	return "<bool>"
}

var _ TypeHandler[string] = Select{}

type Select struct {
	Choices       []string
	InputPossible bool
}

func (opt Select) Parse(s string) (string, error) {
	for _, o := range opt.Choices {
		if o == s {
			return s, nil
		}
	}
	if opt.InputPossible {
		return s, nil
	}
	return "", fmt.Errorf("invalid choice %q, available are %q", s, strings.Join([]string(opt.Choices), ","))
}

func (Select) ToString(s string) string {
	return s
}

func (s Select) Type() string {
	return fmt.Sprintf("<%s>", strings.Join(s.Choices, "|"))
}

var _ TypeHandler[[]string] = MultiSelect{}

func (MultiSelect) ToString(s []string) string {
	return strings.Join(s, ",")
}

func (s MultiSelect) Type() string {
	return fmt.Sprintf("<%s>", strings.Join(s.Choices, ","))
}

type MultiSelect struct {
	Choices       []string
	InputPossible bool
}

func (opt MultiSelect) Parse(s string) (res []string, err error) {
	choices := strings.Split(s, ",")

	var errs []error

	for _, choice := range choices {
		var found bool
		for _, o := range opt.Choices {
			if choice == o {
				found = true
				break
			}

		}

		if !found && !opt.InputPossible {
			errs = append(errs, fmt.Errorf("invalid choice %q", choice))
		} else {
			res = append(res, choice)
		}
	}

	if len(errs) > 0 {
		errs = append([]error{fmt.Errorf("invalid choices, available are %q", strings.Join([]string(opt.Choices), ","))}, errs...)
		return nil, errors.Join(errs...)
	}

	return res, nil
}

var _ TypeHandler[[]string] = Strings{}

func (Strings) ToString(s []string) string {
	return strings.Join(s, ",")
}

func (s Strings) Type() string {
	return "<string,...>"
}

type Strings struct{}

func (Strings) Parse(s string) ([]string, error) {
	return strings.Split(s, ","), nil
}

var _ TypeHandler[[]int] = Ints{}

type Ints struct{}

func (Ints) ToString(ints []int) string {
	var s string

	for _, i := range ints {
		s += fmt.Sprintf("%v,", i)
	}

	if len(s) > 1 {
		return s[:len(s)-1]
	}

	return ""
}

func (s Ints) Type() string {
	return "<int,...>"
}

func (Ints) Parse(s string) (res []int, err error) {
	ss := strings.Split(s, ",")

	var errs []error

	for i, str := range ss {
		v, _err := strconv.Atoi(str)

		if _err != nil {
			errs = append(errs, fmt.Errorf("no int at pos %v: %q", i, str))
		} else {
			res = append(res, v)
		}
	}

	if len(errs) > 0 {
		return nil, errors.Join(errs...)
	}

	return res, nil
}

var _ TypeHandler[[]float64] = Floats{}

type Floats struct{}

func (Floats) ToString(flts []float64) string {
	var s string

	for _, i := range flts {
		s += fmt.Sprintf("%v,", i)
	}

	if len(s) > 1 {
		return s[:len(s)-1]
	}

	return ""
}

func (s Floats) Type() string {
	return "<float,...>"
}

func (Floats) Parse(s string) (res []float64, err error) {
	ss := strings.Split(s, ",")

	var errs []error

	for i, str := range ss {
		v, _err := strconv.ParseFloat(str, 64)

		if _err != nil {
			errs = append(errs, fmt.Errorf("no float at pos %v: %q", i, str))
		} else {
			res = append(res, v)
		}
	}

	if len(errs) > 0 {
		return nil, errors.Join(errs...)
	}

	return res, nil
}

var _ TypeHandler[Time] = Datetime("YYYY")

type Datetime string

func (d Datetime) MustParse(s string) Time {
	t, err := d.Parse(s)
	if err != nil {
		panic(err.Error())
	}
	return t
}

func (Datetime) ToString(tf Time) string {
	return tf.String()
}

func (s Datetime) Type() string {
	return string(s)
}

func (d Datetime) Parse(s string) (t Time, err error) {
	var tm time.Time
	tm, err = fmtdate.Parse(string(d), s)
	if err != nil {
		return
	}
	return Time{Time: tm, Format: string(d)}, nil
}

type Time struct {
	time.Time
	Format string
}

func (t Time) String() string {
	var t0 time.Time
	if t.Time == t0 {
		return ""
	}
	return fmtdate.Format(t.Format, t.Time)
}
