package config

import (
	"bytes"
	"fmt"
	"regexp"
	"strings"

	"os"

	"github.com/zs5460/art"

	"gitlab.com/golang-utils/fs"
	"gitlab.com/golang-utils/fs/filesystems/rootfs"

	//	"gitlab.com/golang-utils/dialog"
	"gitlab.com/golang-utils/version"
)

func (a *App) SetVersion(maj, min, patch int) *App {
	a.Version = version.Version{uint16(maj), uint16(min), uint16(patch)}
	return a
}

func (a *App) SetHelp(str string) *App {
	a.Description = str
	return a
}

func (a *App) WithAsciiArt() *App {
	a.asciiArt = true
	return a
}

// func NewApp(mainfn func() error, name string, opts ...appOption) *App {
func NewApp(mainfn func(*App) error, name string) *App {

	a := &App{
		Name:       name,
		cmdMap:     map[string]int{},
		cmdActions: map[string]func(*App) error{},
	}

	args := os.Args

	if len(args) > 0 {
		a.args = os.Args[1:]
	}

	err := a.checkAppName()

	if err != nil {
		panic("invalid name for app: " + err.Error())
	}

	/*
		for _, opt := range opts {
			opt(a)
		}
	*/

	a.Command = newCommand(a, nil, "", mainfn)
	a.integratedCommands.config = a.NewCommand(runConfig, "config").SetHelp("set and get configurations").SkipAllBut()
	a.integratedCommands.help = a.NewCommand(runHelp, "help").SetHelp("information about usage, options and commands").SkipAllBut()
	a.integratedCommands.help.String(a.integratedCommands.argHelpCommand, "command").SetHelp("prints the help of the given command").SetAsLast()
	a.integratedCommands.version = a.NewCommand(runVersion, "version").SetHelp("show the version").SkipAllBut()
	a.integratedCommands.version.Bool(a.integratedCommands.argVersionShort, "short").SetHelp("just show the version").SetShortflag('s')
	return a
}

type App struct {
	Name        string
	Description string
	Version     version.Version
	asciiArt    bool

	*Command
	Commands   []*Command
	cmdMap     map[string]int
	cmdActions map[string]func(*App) error
	run        *run
	values     values

	integratedCommands struct {
		config          *Command
		help            *Command
		argHelpCommand  *string
		version         *Command
		argVersionShort *bool
	}

	args []string
}

func (a *App) checkAppName() error {
	if len(a.Name) > MaxLenAppName {
		return fmt.Errorf("invalid app name %s: more than %v character", a.Name, MaxLenAppName)
	}
	return ValidateName(a.Name)
}

func (a *App) GetCommandByName(name string) *Command {
	if i, has := a.cmdMap[name]; has {
		return a.Commands[i]
	}

	return nil
}

func (a *App) addCommand(c *Command, fn func(a *App) error) {
	if c == nil {
		panic("command muss not be nil")
	}
	if _, has := a.cmdMap[c.Name()]; has {
		panic("command with name " + c.Name() + " is already defined")
	}

	a.Commands = append(a.Commands, c)
	a.cmdMap[c.Name()] = len(a.Commands) - 1
	a.cmdActions[c.Name()] = fn
}

func (a *App) NewCommand(fn func(a *App) error, name string) *Command {
	return newCommand(a, a.Command, name, fn)
}

func (a *App) checkCommandName(name string) error {
	if len(name) > MaxLenCommandName {
		return fmt.Errorf("invalid command name %s: more than %v character", name, MaxLenCommandName)
	}
	return ValidateName(name)
}

func (a *App) DefaultValues() values {
	var vals = values{}

	for _, c := range a.Commands {
		for _, opt := range c.options() {
			def := opt.Default()
			if def != nil {
				vals[valueIndex{Command: c.Name(), Option: opt.Name()}] =
					fmt.Sprintf("%v", def)
			}
		}
	}

	return vals

}

func (a *App) Reset() {
	a.run = nil
	a.values = values{}
}

func runHelp(a *App) error {
	var h string
	if a.integratedCommands.help == nil {
		h = a.mainUsage()
	} else {
		h = a.cmdUsage(*a.integratedCommands.argHelpCommand)
	}

	var headerLines int = 0

	err := page(h, headerLines)
	if err != nil {
		return err
	}

	return nil
}

func runConfig(a *App) error {
	fmt.Println("do the config stuff")
	return nil
}

func runVersion(a *App) error {
	if a.integratedCommands.argVersionShort != nil && *a.integratedCommands.argVersionShort {
		fmt.Println(a.Version.String())
	} else {
		fmt.Printf("%s %s\n", a.Name, a.Version)
	}
	return nil
}

func (a *App) SetArgs(args []string) *App {
	a.args = args
	return a
}

func (a *App) Run() (err error) {
	a.values, _, err = a.newRun(a.args).LoadAll()
	if err != nil {
		return err
	}

	fn, has := a.cmdActions[a.ActiveCommand().Name()]

	if has && fn != nil {
		return fn(a)
	}

	return nil
}

func (a *App) ActiveCommand() *Command {
	if a.run == nil {
		return nil
	}

	if a.run.Args == nil {
		return nil
	}
	return a.run.Args.Command
}

func (a *App) newRun(args []string) *run {
	fsys, err := rootfs.New()
	if err != nil {
		panic(err.Error())
	}
	return a.newRunFS(args, fsys)
}

func (a *App) newRunFS(args []string, fsys fs.FS) *run {
	a.run = newRun(a, args, fsys)
	return a.run
}

func (a *App) EnvVar(o optionInterface) string {
	app := a.Name
	cmd := o.Command()
	option := o.Name()
	if err := ValidateName(app); err != nil {
		panic("invalid app name: " + app)
	}

	if cmd != "" {
		if err := ValidateName(cmd); err != nil {
			panic("invalid cmd name: " + cmd)
		}
	}

	if err := ValidateName(option); err != nil {
		panic("invalid option name: " + option)
	}

	sep := "OPTION"

	app = strings.ReplaceAll(app, "_", "#")

	var prefix = app

	if cmd == "" {
		prefix = prefix + "_" + sep
	} else {
		cmd = strings.ReplaceAll(cmd, "_", "#")
		prefix = prefix + "_" + cmd + "_" + sep
	}

	option = strings.ReplaceAll(option, "_", "#")

	envvar := prefix + "_" + option
	envvar = strings.ReplaceAll(envvar, "-", "__")
	envvar = strings.ReplaceAll(envvar, "#", "___")

	return strings.ToUpper(envvar)
}

func (a *App) usageEnvVars(cmd *Command) string {
	var bd strings.Builder

	bd.WriteString("~~~~~~~~~~~~ ENVIRONMENTAL VARIABLES ~~~~~~~~~~~~\n\n")

	for _, opt := range cmd.allOptions() {
		key := a.EnvVar(opt)
		val := a.run.Env.Value(key)
		// file               CAT_OPTION_FILE="myfile"
		bd.WriteString(pad(opt.Name(), fmt.Sprintf("%s=%q", key, val)) + "\n")
	}

	return bd.String()
}

/*
		usage: git [--version] [--help] [-C <path>] [-c name=value]
	           [--exec-path[=<path>]] [--html-path] [--man-path] [--info-path]
	           [-p|--paginate|--no-pager] [--no-replace-objects] [--bare]
	           [--git-dir=<path>] [--work-tree=<path>] [--namespace=<name>]
	           <command> [<args>]
*/

func (c *App) Usage() string {
	return c.mainUsage()
}

func (c *App) CommandUsage(cmd string) string {
	return c.cmdUsage(cmd)
}

func lastOptionArg(o optionInterface) string {
	if o.Required() {
		return "<" + o.Name() + ">"
		//return o.Name()
	} else {
		return "[" + o.Name() + "]"
	}
}

func (c *App) mainUsage() string {
	var (
		commands     string
		commandInfo  string
		helpintro    string
		cmdStr       string
		envVariables string = c.usageEnvVars(c.Command)
	)

	//lastArg, requiredOptions, optionalOptions := c.usage()
	lastArg, requiredOptions, optionalOptions := c.Command.usage2()

	if requiredOptions != "" {
		requiredOptions = "\n~~~~~~~~~~~~ REQUIRED ~~~~~~~~~~~~~~~~~~~~~~~~~~~\n" + requiredOptions
	}

	if optionalOptions != "" {
		optionalOptions = "\n~~~~~~~~~~~~ OPTIONAL ~~~~~~~~~~~~~~~~~~~~~~~~~~~\n" + optionalOptions
	}

	if c.asciiArt {
		helpintro = art.String(c.Name)
	}

	helpintro += "\n" + c.Name + " v" + c.Version.String()

	if len(c.Commands) > 0 {
		commands = "\n~~~~~~~~~~~~ COMMANDS ~~~~~~~~~~~~~~~~~~~~~~~~~~~\n\n" +
			c.usageSubcommands()
		commandInfo = "\n\n~~~~~~~~~~~~ show help about a command: ~~~~~~~~~\n\n" +
			fmt.Sprintf("%s help <command>", c.Name)
		cmdStr = " [command]"
	}

	/*
	   cat v0.2.1

	   ## ACTION ##

	   show the content of a file

	   ## USAGE ##

	   cat <command> <options...> <file>

	   ## REQUIRED ##

	   file               (type: path)
	                      The file that should be printed.

	   ## OPTIONAL ##

	   --show-type 	      (type: bool, default: false)
	    -t                Show the file type instead of the content.

	   --ignore-bin       (type: bool, default: true)
	    -i                Don't print the content, if the file is binary.

	   ## COMMANDS ##

	   config             show and edit config files of cat
	   help               show the help of cat
	   version            show the version of cat

	   ## CONFIG FILES ##

	   machine            [x] .....
	   user               [ ] .....
	   local              [x] ./_config/cat/cat.conf

	   ## ENVIRONMENTAL VARIABLES ##

	   file               CAT_OPTION_FILE="myfile"
	   type               CAT_OPTION_TYPE=""
	   ignore-bin         CAT_OPTION_IGNORE__BIN=""

	   ## show help about a command: ##

	   cat help <command>
	*/

	return fmt.Sprintf(
		`%s

~~~~~~~~~~~~ FUNCTION ~~~~~~~~~~~~~~~~~~~~~~~~~~~

%s

~~~~~~~~~~~~ USAGE ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

%s%s --<option>=<value>... %s
%s%s%s~~~~~~~~~~~~ CONFIGURATION FILES ~~~~~~~~~~~~~~~~ 

%s
%s%s
`,
		helpintro,
		// action,
		c.Description,
		// usage
		c.Name, cmdStr, lastArg,
		// required options
		requiredOptions,
		// optional options
		optionalOptions,
		// commands
		commands,
		// config files
		c.usageConfigurationFiles(),
		// env vars
		envVariables,
		// subcommands
		commandInfo)
}

func (a *App) usageConfigurationFiles() string {
	var (
		hasGlobal = "[ ] "
		hasUser   = "[ ] "
		hasLocal  = "[ ] "
	)

	if a.run.GlobalConfig.Exists() {
		hasGlobal = "[x] "
	}

	if a.run.UserConfig.Exists() {
		hasUser = "[x] "
	}

	if a.run.LocalConfig.Exists() {
		hasLocal = "[x] "
	}

	s := ""
	s += pad(hasGlobal+"global", "") + a.run.GlobalConfig.Location() + "\n"
	s += pad(hasUser+"user", "") + a.run.UserConfig.Location() + "\n"
	s += pad(hasLocal+"local", "") + a.run.LocalConfig.Location() + "\n"
	return s
}

func (c *App) cmdUsage(cmd string) string {
	if cmd == "" {
		panic("empty command not allowed")
	}

	mycmd := c.GetCommandByName(cmd)
	if mycmd == nil {
		panic("unknown command " + cmd)
	}

	envVariables := c.usageEnvVars(mycmd)

	helpintro := ""

	if c.asciiArt {
		helpintro = art.String(c.Name)
	}

	helpintro += "\n" + c.Name + " " + mycmd.Name()

	//lastArg, options := mycmd.usage()
	lastArg, requiredOptions, optionalOptions := mycmd.usage2()

	if requiredOptions != "" {
		requiredOptions = "\n~~~~~~~~~~~~ REQUIRED ~~~~~~~~~~~~~~~~~~~~~~~~~~~\n" + requiredOptions
	}

	if optionalOptions != "" {
		optionalOptions = "\n~~~~~~~~~~~~ OPTIONAL ~~~~~~~~~~~~~~~~~~~~~~~~~~~\n" + optionalOptions
	}

	if requiredOptions == "" && optionalOptions == "" {
		return fmt.Sprintf(
			`
%s

~~~~~~~~~~~~ FUNCTION ~~~~~~~~~~~~~~~~~~~~~~~~~~~

%s

~~~~~~~~~~~~ USAGE ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 
%s %s %s

`,
			helpintro,
			// function
			mycmd.Help(),
			c.Name, mycmd.Name(), lastArg)
	}
	return fmt.Sprintf(
		`
%s

~~~~~~~~~~~~ FUNCTION ~~~~~~~~~~~~~~~~~~~~~~~~~~~

%s

~~~~~~~~~~~~ USAGE ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

%s %s --<option>=<value>... %s
%s%s
%s
`,
		helpintro,
		mycmd.Help(),
		c.Name, mycmd.Name(), lastArg,
		requiredOptions,
		optionalOptions,
		envVariables,
	)
}

func (c *App) usageSubcommands() string {
	var subcBf bytes.Buffer

	for _, subC := range c.Commands {
		if subC.Name() == "" {
			continue
		}
		subcBf.WriteString(pad(subC.Name(), subC.Help()) + "\n")
		subcBf.WriteString("\n")
	}

	return subcBf.String()
}

func (a *App) SaveToGlobalConfig(opts ...optionInterface) error {
	return a.saveToConfigFile("global", opts...)
}

func (a *App) SaveToLocalConfig(opts ...optionInterface) error {
	return a.saveToConfigFile("local", opts...)
}

func (a *App) SaveToUserConfig(opts ...optionInterface) error {
	return a.saveToConfigFile("user", opts...)
}

func (a *App) saveToConfigFile(file string, opts ...optionInterface) error {
	var vals = values{}

	for _, opt := range opts {
		vals[valueIndex{Command: opt.Command(), Option: opt.Name()}] = opt.valueString()
	}

	switch file {
	case "global":
		return a.run.GlobalConfig.Save(vals)
	case "user":
		return a.run.UserConfig.Save(vals)
	case "local":
		return a.run.LocalConfig.Save(vals)
	default:
		panic("unknown config file " + file)
	}

}

/*
rules for names:

	must not start with numbers
	may not have more than an underscore in a row
	may not have more than a dash in a row
	may not start or end with an underscore
	may not start or end with a dash
	may not have an underscore followed by a dash
	may not have a dash followed by an underscore
	other than that, just lowercase letters are allowed
	we need at least two characters
*/
func ValidateName(name string) (err error) {
	switch {
	case len(name) < 2:
	case name[0] == '-':
	case name[len(name)-1] == '-':
	case name[0] == '_':
	case name[len(name)-1] == '_':
	case !nameRegExp2.MatchString(name):
	case strings.Contains(name, "__"):
	case strings.Contains(name, "--"):
	case strings.Contains(name, "_-"):
	case strings.Contains(name, "-_"):
	default:
		return nil
	}

	return InvalidNameError(name)
}

var nameRegExp2 = regexp.MustCompile("^[a-z][-_a-z0-9]+$")

type InvalidNameError string

func (e InvalidNameError) Error() string {
	return fmt.Sprintf("invalid name %#v", string(e))
}
