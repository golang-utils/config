package config

import (
	"bytes"
	"fmt"
)

type Command struct {
	name          string
	help          string
	_options      []optionInterface
	_optMap       map[string]int
	_skipped      map[string]bool
	skippedAllBut map[string]bool
	_relaxed      map[string]bool
	lastOption    string
	_app          *App
	mainCommand   *Command
}

/*
type Command interface {
	Name() string
	addOption(o optionInterface)
	HasLastOption() bool
	GetLastOption() optionInterface
	allOptions() (all []optionInterface)
	GetOptionByName(name string) optionInterface
	Skip(opts ...string) Command
	SkipAllBut(except ...string) Command
	getSkipped() map[string]bool
	Relax(opts ...string) Command
	findByShortFlag(s string) optionInterface
	app() *App
	usage2() (lastArg, requiredOptions, optionalOptions string)
	Help() string
	options() []optionInterface
	relaxed() map[string]bool
	optMap() map[string]int
	setAsLastOption(name string) Command
}
*/

func newCommand(a *App, maincmd *Command, name string, fn func(*App) error) *Command {
	c := &Command{
		name:     name,
		_optMap:  map[string]int{},
		_skipped: map[string]bool{},
		_relaxed: map[string]bool{},
		_app:     a,
	}

	if a.Command != nil {
		err := a.checkCommandName(c.Name())
		if err != nil {
			panic(err.Error())
		}
	}

	c._app = a
	c.mainCommand = maincmd
	a.addCommand(c, fn)
	return c
}

func (c Command) Help() string {
	return c.help
}

func (c *Command) SetHelp(str string) *Command {
	c.help = str
	return c
}

func (c *Command) relaxed() map[string]bool {
	return c._relaxed
}

func (c *Command) optMap() map[string]int {
	return c._optMap
}

func (c *Command) options() []optionInterface {
	return c._options
}

func (c Command) Name() string {
	return c.name
}

func (c *Command) app() *App {
	return c._app
}

func (c *Command) setAsLastOption(optName string) *Command {
	if c.lastOption != "" {
		panic("last argument for command " + c.name + " already set to " + c.lastOption)
	}
	c.lastOption = optName
	return c
}

func (c *Command) HasLastOption() bool {
	return c.lastOption != ""
}

func (c *Command) GetLastOption() optionInterface {
	return c.GetOptionByName("")
}

type wrappedOption struct {
	optionInterface
	required bool
}

func (w *wrappedOption) Required() bool {
	return w.required
}

var _ optionInterface = &wrappedOption{}

func (c *Command) allOptions() (all []optionInterface) {
	if c.mainCommand == nil {
		return c.options()
	}
	skipped := c.getSkipped()
	relaxed := c.relaxed()

	for _, opt := range c.mainCommand.options() {
		if skipped[opt.Name()] {
			continue
		}

		if relaxed[opt.Name()] {
			all = append(all, &wrappedOption{optionInterface: opt, required: false})
			continue
		}

		all = append(all, opt)
	}

	all = append(all, c.options()...)

	return all
}

func (c *Command) getMyDirectOptionByName(name string) optionInterface {
	if name == "" {
		name = c.lastOption
	}

	if i, has := c.optMap()[name]; has {
		return c._options[i]
	}

	return nil
}

func (c *Command) GetOptionByName(name string) optionInterface {
	opt := c.getMyDirectOptionByName(name)

	if c.mainCommand == nil {
		return opt
	}

	if opt != nil {
		return opt
	}

	skipped := c.getSkipped()

	if skipped[name] {
		return nil
	}

	opt = c.mainCommand.getMyDirectOptionByName(name)

	if opt != nil && c._relaxed[name] {
		return &wrappedOption{optionInterface: opt, required: false}
	}

	return opt
}

func (c *Command) Skip(opts ...string) *Command {
	for _, opt := range opts {
		c._skipped[opt] = true
	}

	return c
}

func (c *Command) SkipAllBut(except ...string) *Command {
	c.skippedAllBut = map[string]bool{}
	for _, x := range except {
		c.skippedAllBut[x] = true
	}

	return c
}

func findOption(opt optionInterface, isLastArg bool, args map[string]string) (foundTimes int, key, value string, err error) {
	if isLastArg {
		v, has := args[""]
		if has {
			foundTimes++
			value = v
			key = ""
		}
	}

	keys := []string{"-" + opt.Name(), "--" + opt.Name(), "-" + opt.ShortFlag(), "--" + opt.ShortFlag()}

	for _, k := range keys {
		v, has := args[k]
		if has {
			foundTimes++
			value = v
			key = k
		}
	}

	if foundTimes > 1 {
		err = fmt.Errorf("go option %q %v times via args", opt.Name(), foundTimes)
	}
	return
}

func (c *Command) checkMissing(vals values) (err error) {
	// allOptions returns the direct options and the parent (inherited) options
	// and wrapps the required parent ones, that are relaxed, so that only the
	// finally required ones are required
	opts := c.allOptions()

	for _, opt := range opts {
		if opt.Required() {
			_, has := vals[valueIndex{Command: opt.Command(), Option: opt.Name()}]
			if !has {
				return fmt.Errorf("required option %v not set", opt.Name())
			}
		}
	}

	return nil
}

// key of the arg map can be --name or -name or -n (shortkey)
func (c *Command) loadArgs(args map[string]string) (vals values, err error) {
	if args == nil || len(args) == 0 {
		return
	}

	lastOption := c.GetLastOption()

	vals = values{}

	// allOptions returns the direct options and the parent (inherited) options
	opts := c.allOptions()
	found := map[string]bool{}

	for _, opt := range opts {

		lastArg := false
		if lastOption != nil && lastOption.Name() == opt.Name() {
			lastArg = true
		}

		foundTimes, key, value, err := findOption(opt, lastArg, args)
		if err != nil {
			return vals, err
		}

		if foundTimes == 1 {
			found[key] = true
			vals[valueIndex{Command: opt.Command(), Option: opt.Name()}] = value
		}
	}

	for key := range args {
		if found[key] {
			continue
		}

		return vals, fmt.Errorf("unknown option %v", key)
	}

	return vals, nil
}

func (c *Command) getSkipped() map[string]bool {
	sk := map[string]bool{}

	if c.mainCommand == nil {
		return sk
	}

	if c.skippedAllBut != nil && c.mainCommand != nil {

		for _, o := range c.mainCommand.options() {
			if c.skippedAllBut[o.Name()] {
				continue
			}

			sk[o.Name()] = true
		}
	} else {
		sk = c._skipped
	}

	return sk
}

func (c *Command) Relax(opts ...string) *Command {
	for _, opt := range opts {
		c._relaxed[opt] = true
	}

	return c
}

func (c *Command) checkOptionName(name string) error {
	if len(name) > MaxLenOptionName {
		return fmt.Errorf("invalid option name %s: more than %v character", name, MaxLenOptionName)
	}
	return ValidateName(name)
}

func (c *Command) addOption(o optionInterface) {
	err := c.checkOptionName(o.Name())

	if err != nil {
		panic(err.Error())
	}

	if c._optMap == nil {
		c._optMap = map[string]int{}
	}

	if _, has := c._optMap[o.Name()]; has {
		panic("option with name " + o.Name() + " is already defined in command " + c.name)
	}

	c._options = append(c._options, o)
	c._optMap[o.Name()] = len(c._options) - 1
}

func (c *Command) findByShortFlag(s string) optionInterface {
	for _, op := range c._options {
		if op.ShortFlag() == s {
			return op
		}
	}
	return nil
}

func (c *Command) usage2() (lastArg, requiredOptions, optionalOptions string) {
	lastOption := c.GetLastOption()

	if lastOption != nil {
		lastArg = lastOptionArg(lastOption)
	}

	requiredOptions = c._usage2(lastOption, true)
	optionalOptions = c._usage2(lastOption, false)
	return
}

func (c *Command) _usage2(lastOption optionInterface, required bool) string {

	var optBf bytes.Buffer

	var lastOptionName string

	if lastOption != nil && lastOption.Required() == required {
		lastOptionName = lastOption.Name()

		u := &usage{
			isLastOption: true,
			option:       lastOption,
		}

		ostr := u.String()
		if ostr != "" {
			optBf.WriteString(ostr)
		}
	}

	for _, opt := range c.allOptions() {
		if lastOptionName == opt.Name() {
			continue
		}

		if opt.Required() != required {
			continue
		}

		u := &usage{
			isLastOption: false,
			option:       opt,
		}
		ostr := u.String()
		if ostr != "" {
			optBf.WriteString(ostr)
		}
	}

	return optBf.String()
}
