package config

// String allows any string input
func (c *Command) String(target *string, name string) *Option[string] {
	o := &Option[string]{
		name:      name,
		target:    target,
		validator: nil,
		type_:     String{},
		command:   c,
	}

	c.addOption(o)
	return o
}

func (c *Command) Json(target *Json, name string) *Option[Json] {
	o := &Option[Json]{
		name:      name,
		target:    target,
		validator: nil,
		type_:     Json{},
		command:   c,
	}

	c.addOption(o)
	return o
}

// Custom creates a custom option for any given type
func Custom[T any](c *Command, target *T, name string, type_ TypeHandler[T]) *Option[T] {
	o := &Option[T]{
		name:      name,
		validator: nil,
		target:    target,
		type_:     type_,
		command:   c,
	}

	c.addOption(o)
	return o
}

// Path allows input that is a path
func (c *Command) Path(target *Path, name string) *Option[Path] {
	o := &Option[Path]{
		name:      name,
		target:    target,
		validator: nil,
		type_:     Path{},
		command:   c,
	}

	c.addOption(o)
	return o
}

// Int allows input that is a integer
func (c *Command) Int(target *int, name string) *Option[int] {
	o := &Option[int]{
		name:      name,
		validator: nil,
		target:    target,
		type_:     Int{},
		command:   c,
	}

	c.addOption(o)
	return o
}

// Float allows input that is a floating number
func (c *Command) Float(target *float64, name string) *Option[float64] {
	o := &Option[float64]{
		name:      name,
		validator: nil,
		target:    target,
		type_:     Float{},
		command:   c,
	}

	c.addOption(o)
	return o
}

// Bool allows input that is a boolean (true/false)
func (c *Command) Bool(target *bool, name string) *Option[bool] {
	o := &Option[bool]{
		name:      name,
		validator: nil,
		target:    target,
		type_:     Bool{},
		command:   c,
	}

	c.addOption(o)
	return o
}

// Select offers a range of choices to select from
func (c *Command) Select(target *string, name string, choices []string) *Option[string] {
	o := &Option[string]{
		name:      name,
		target:    target,
		validator: nil,
		type_:     Select{Choices: choices},
		command:   c,
	}

	c.addOption(o)
	return o
}

// SelectInput offers a range of choices to select from, but also a user defined string can be used
func (c *Command) SelectInput(target *string, name string, choices []string) *Option[string] {
	o := &Option[string]{
		name:      name,
		validator: nil,
		target:    target,
		type_:     Select{Choices: choices, InputPossible: true},
		command:   c,
	}

	c.addOption(o)
	return o
}

// MultiSelect offers a range of choices to select multiple from (separated by #)
func (c *Command) MultiSelect(target *[]string, name string, choices []string) *Option[[]string] {
	o := &Option[[]string]{
		name:      name,
		validator: nil,
		target:    target,
		type_:     MultiSelect{Choices: choices},
		command:   c,
	}

	c.addOption(o)
	return o
}

// MultiSelectInput offers a range of choices to select multiple from (separated by #), but also user defined strings can be used
func (c *Command) MultiSelectInput(target *[]string, name string, choices []string) *Option[[]string] {
	o := &Option[[]string]{
		name:      name,
		validator: nil,
		target:    target,
		type_:     MultiSelect{Choices: choices, InputPossible: true},
		command:   c,
	}

	c.addOption(o)
	return o
}

// Time allows a time input in the format defined by TimeFormat
func (c *Command) Time(target *Time, name string) *Option[Time] {
	o := &Option[Time]{
		name:      name,
		target:    target,
		validator: nil,
		type_:     TimeFormat,
		command:   c,
	}

	c.addOption(o)
	return o
}

// TimeShort allows a time input in the format defined by Time
func (c *Command) TimeShort(target *Time, name string) *Option[Time] {
	o := &Option[Time]{
		name:      name,
		target:    target,
		validator: nil,
		type_:     TimeFormatShort,
		command:   c,
	}

	c.addOption(o)
	return o
}

// Date allows a date input in the format defined by DateFormat
func (c *Command) Date(target *Time, name string) *Option[Time] {
	o := &Option[Time]{
		name:      name,
		validator: nil,
		target:    target,
		type_:     DateFormat,
		command:   c,
	}

	c.addOption(o)
	return o
}

// DateTime allows a date/time input in the format defined by the given format string (MS Excel style)
func (c *Command) DateTime(target *Time, name, format string) *Option[Time] {
	o := &Option[Time]{
		name:      name,
		validator: nil,
		target:    target,
		type_:     Datetime(format),
		command:   c,
	}

	c.addOption(o)
	return o
}

// Ints allows input of multiple ints, separated by #
func (c *Command) Ints(target *[]int, name string) *Option[[]int] {
	o := &Option[[]int]{
		name:      name,
		validator: nil,
		target:    target,
		type_:     Ints{},
		command:   c,
	}

	c.addOption(o)
	return o
}

// Floats allows input of multiple floats, separated by #
func (c *Command) Floats(target *[]float64, name string) *Option[[]float64] {
	o := &Option[[]float64]{
		name:      name,
		target:    target,
		validator: nil,
		type_:     Floats{},
		command:   c,
	}

	c.addOption(o)
	return o
}

// Strings allows input that is a list of strings, separated by #
func (c *Command) Strings(target *[]string, name string) *Option[[]string] {
	o := &Option[[]string]{
		name:      name,
		target:    target,
		validator: nil,
		type_:     Strings{},
		command:   c,
	}

	c.addOption(o)
	return o
}
