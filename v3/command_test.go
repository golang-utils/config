package config

import (
	//	"fmt"
	"testing"
)

func TestCommandGetOptionByName(t *testing.T) {

	var (
		argInherited      = "inherited"
		argInheritedReq   = "inherited-req"
		argRelaxed        = "relaxed"
		argEnforced       = "enforced"
		argOverwritten    = "overwritten"
		argOverwrittenReq = "overwritten-req"
		argSkipped        = "skipped"
		argSkippedReq     = "skipped-req"
		argNew            = "new"
		argNewReq         = "new-req"
	)

	app := NewApp(nil, "testapp")
	app.String(nil, argInherited)
	app.String(nil, argInheritedReq).SetRequired()
	app.Int(nil, argRelaxed).SetRequired()
	app.Float(nil, argEnforced)
	app.Float(nil, argOverwritten)
	app.Float(nil, argOverwrittenReq)
	app.Bool(nil, argSkipped)
	app.Bool(nil, argSkippedReq)

	cmd := app.NewCommand(nil, "lib")
	cmd.String(nil, argNew)
	cmd.String(nil, argNewReq)
	cmd.Float(nil, argEnforced).SetRequired()
	cmd.Int(nil, argOverwritten)
	cmd.Int(nil, argOverwrittenReq).SetRequired()
	cmd.Relax(argRelaxed)
	cmd.Skip(argSkipped, argSkippedReq)

	all := cmd.allOptions()

	tests := []struct {
		optName  string
		found    bool
		required bool
		command  bool
	}{
		{argInherited, true, false, false},
		{argInheritedReq, true, true, false},
		{argRelaxed, true, false, false},
		{argEnforced, true, true, true},
		{argOverwritten, true, false, true},
		{argOverwrittenReq, true, true, true},
		{argSkipped, false, false, false},
		{argSkippedReq, false, false, false},
	}

	for i, test := range tests {
		opt := cmd.GetOptionByName(test.optName)

		if test.found && opt == nil {
			t.Fatalf("[%v] option %v not found for command %v", i, test.optName, cmd.Name())
		}

		if !test.found {
			opt = app.GetOptionByName(test.optName)
			if opt == nil {
				t.Fatalf("[%v] option %v not found for app", i, test.optName)
			}
		}

		if test.required != opt.Required() {
			not := ""
			if !test.required {
				not = " not"
			}
			t.Errorf("[%v] option %v should%s be required", i, test.optName, not)
		}

		if test.command != (opt.Command() != "") {
			not := ""
			if !test.command {
				not = " not"
			}
			t.Errorf("[%v] option %v should%s be part of the command", i, test.optName, not)
		}

	}

	_ = all
}

func TestCommandGetAllOptions(t *testing.T) {

	var (
		argInherited      = "inherited"
		argInheritedReq   = "inherited-req"
		argRelaxed        = "relaxed"
		argEnforced       = "enforced"
		argOverwritten    = "overwritten"
		argOverwrittenReq = "overwritten-req"
		argSkipped        = "skipped"
		argSkippedReq     = "skipped-req"
		argNew            = "new"
		argNewReq         = "new-req"
	)

	app := NewApp(nil, "testapp")
	app.String(nil, argInherited)
	app.String(nil, argInheritedReq).SetRequired()
	app.Int(nil, argRelaxed).SetRequired()
	app.Float(nil, argEnforced)
	app.Float(nil, argOverwritten)
	app.Float(nil, argOverwrittenReq)
	app.Bool(nil, argSkipped)
	app.Bool(nil, argSkippedReq)

	cmd := app.NewCommand(nil, "lib")
	cmd.String(nil, argNew)
	cmd.String(nil, argNewReq)
	cmd.Float(nil, argEnforced).SetRequired()
	cmd.Int(nil, argOverwritten)
	cmd.Int(nil, argOverwrittenReq).SetRequired()
	cmd.Relax(argRelaxed)
	cmd.Skip(argSkipped, argSkippedReq)

	all := cmd.allOptions()

	//skipped := lib.getSkipped()
	//fmt.Printf("skipped: %#v\n", skipped)

	//fmt.Printf("_skipped: %#v\n", lib._skipped)

	allMap := map[string]bool{}
	allRequired := map[string]bool{}
	allIsCommand := map[string]bool{}

	for _, opt := range all {
		allMap[opt.Name()] = true
		allRequired[opt.Name()] = opt.Required()
		allIsCommand[opt.Name()] = opt.Command() != ""
	}

	tests := []struct {
		optName  string
		found    bool
		required bool
		command  bool
	}{
		{argInherited, true, false, false},
		{argInheritedReq, true, true, false},
		{argRelaxed, true, false, false},
		{argEnforced, true, true, true},
		{argOverwritten, true, false, true},
		{argOverwrittenReq, true, true, true},
		{argSkipped, false, false, false},
		{argSkippedReq, false, false, false},
	}

	for i, test := range tests {
		//opt := lib.GetOptionByName(test.optName)

		if test.found != allMap[test.optName] {
			not := ""
			if !test.found {
				not = " not"
			}
			t.Errorf("[%v] option %v should%s be found for command %v", i, test.optName, not, cmd.Name())
			continue
		}

		if !test.found {
			continue
		}

		if test.required != allRequired[test.optName] {
			not := ""
			if !test.required {
				not = " not"
			}
			t.Errorf("[%v] option %v should%s be required", i, test.optName, not)
		}

		if test.command != allIsCommand[test.optName] {
			not := ""
			if !test.command {
				not = " not"
			}
			t.Errorf("[%v] option %v should%s be part of the command", i, test.optName, not)
		}

	}

	_ = all
}
