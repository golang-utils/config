package config

import (
	"bytes"
	"fmt"
	"os"

	"github.com/noborus/ov/oviewer"
)

func page(s string, headerLines int) error {
	doc, err := oviewer.NewDocument()
	if err != nil {
		return err
	}

	if err := doc.ReadAll(bytes.NewBufferString(s)); err != nil {
		return err
	}

	ov, err := oviewer.NewOviewer(doc)
	if err != nil {
		return err
	}

	if headerLines > 0 {
		ov.Config.General.Header = headerLines
	}
	ov.Config.General.WrapMode = true
	ov.Config.QuitSmall = true

	if err := ov.Run(); err != nil {
		return err
	}

	if ov.Config.IsWriteOriginal {
		fmt.Fprintf(os.Stdout, s)
	}
	return nil
}
