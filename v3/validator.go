package config

type Validator[T any] interface {
	Validate(T) error
}
