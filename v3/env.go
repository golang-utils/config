package config

import (
	"fmt"
	"os"
	"strings"
)

type env struct {
	App *App
	env []string
}

func getEnv() []string {
	return os.Environ()
}

func newEnv(a *App) *env {
	return &env{
		App: a,
		env: getEnv(),
	}
}

func (e *env) Location() string {
	return "ENV"
}

var _ valueSource = &env{}

func (f *env) Value(envkey string) string {
	for _, pair := range f.env {
		startVal := strings.Index(pair, "=")
		key, val := pair[0:startVal], pair[startVal+1:]
		val = strings.TrimSpace(val)

		if envkey == key {
			return val
		}
	}
	return ""
}

func (f *env) Load() (vals values, err error) {

	vals = values{}
	for _, pair := range f.env {
		if f.IsEnvVar(pair) {
			//fmt.Printf("Env: %#v\n", pair)

			startVal := strings.Index(pair, "=")
			key, val := pair[0:startVal], pair[startVal+1:]
			val = strings.TrimSpace(val)

			if val == "" {
				//return EmptyValueError(key)
				return nil, fmt.Errorf("empty value for option %s", key)
			}

			option, err := f.ParseOption(key)

			// some variable not set by use
			if err != nil {
				continue
			}

			// location would be pair[:startVal] (the env-var-name)
			vals[valueIndex{Command: option.Command(), Option: option.Name()}] = val
		}
	}
	return vals, nil
}

func (e *env) IsEnvVar(pair string) bool {
	app := e.App.Name
	app = strings.ReplaceAll(app, "_", "#")
	app = strings.ReplaceAll(app, "-", "__")
	app = strings.ReplaceAll(app, "#", "___")
	app = strings.ToUpper(app)
	return strings.HasPrefix(pair, app+"_")
}

// ParseOption returns an option from an envvar, or returns an error when the option
// could not be found/parsed
func (e *env) ParseOption(envvar string) (optionInterface, error) {
	app, cmd, opt, err := envToNames(envvar)

	if err != nil {
		return nil, err
	}

	if strings.ToLower(app) != strings.ToLower(e.App.Name) {
		return nil, fmt.Errorf("app name not matching")
	}

	c := e.App.GetCommandByName(cmd)

	if c == nil {
		return nil, fmt.Errorf("command not found")
	}

	o := c.GetOptionByName(opt)

	if o == nil {
		return nil, fmt.Errorf("option not found")
	}

	return o, nil
}

// envToNames transforms and env variable to app cmd and option
func envToNames(envvar string) (app, cmd, option string, err error) {

	/*
		first split by separator ___
		then you have the names
		and for each name, first replace __ with _ and then _ with -
	*/

	envvar = strings.ReplaceAll(envvar, "___", "#")
	envvar = strings.ReplaceAll(envvar, "__", "-")
	envvar = strings.ToLower(envvar)

	a := strings.Split(envvar, "_")

	switch len(a) {
	case 3:
		app = a[0]
		// a[1] == "C"
		option = a[2]
	case 4:
		app = a[0]
		cmd = a[1]
		// a[2] == "C"
		option = a[3]
	default:
		err = fmt.Errorf("non conformant env variable")
		return
	}

	app = strings.ReplaceAll(app, "#", "_")

	if cmd != "" {
		cmd = strings.ReplaceAll(cmd, "#", "_")
	}

	option = strings.ReplaceAll(option, "#", "_")

	return
}
