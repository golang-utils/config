package config

import (
	//	"encoding/json"
	//"errors"
	//"strconv"
	"os"
	"strings"

	"gitlab.com/golang-utils/dialog"
)

type configDialog struct {
	App                *App
	runner             *run
	dialogApp          dialog.App
	values             values
	currentCommand     *Command
	currentCommandName string
	currentCommandHelp string
	currentOption      optionInterface
	currentFile        string
	currentValue       string
	currentValueType   string
	currentValueHelp   string
}

var skipCommands = map[string]bool{
	"config":  true,
	"version": true,
	"help":    true,
}

func newDialog(app *App) *configDialog {
	var opts []dialog.Option

	opts = append(opts,
		//	dialog.WithBackNavigation(),
		dialog.WithFooter(),
		dialog.WithHeader(),
		dialog.WithDefaultGui(),
		//dialog.WithBreadcrumb(),
	)

	a := dialog.New("configuration of "+app.Name, opts...)

	dc := &configDialog{
		App:       app,
		dialogApp: a,
		values:    values{},
	}

	dc.runner = dc.App.newRun(os.Args)
	return dc
}

/*
func stringToValue(typ string, in string) (out interface{}, err error) {
	switch typ {
	case "bool":
		return strconv.ParseBool(in)
	case "int":
		i, e := strconv.Atoi(in)
		return int(i), e
	case "float64":
		fl, e := strconv.ParseFloat(in, 64)
		return float64(fl), e
	case "datetime":
		d, e := DateTimeGeneral.Parse(in)
		if e != nil {
			return DateTimeFormat.Parse(in)
		}
		return d, nil
	case "date":
		d, e := DateTimeGeneral.Parse(in)
		if e != nil {
			return DateFormat.Parse(in)
		}
		return d, nil
	case "time":
		d, e := DateTimeGeneral.Parse(in)
		if e != nil {
			return TimeFormat.Parse(in)
		}
		return d, nil
	case "string":
		return in, nil
	case "json":
		var v interface{}
		err = json.Unmarshal([]byte(in), &v)
		if err != nil {
			return nil, err
		}
		return in, nil
	default:
		return nil, errors.New("unknown type " + typ)
	}
}
*/

func (d *configDialog) validateValue(s string) error {
	/*
		v, err := stringToValue(d.currentOption.Type(), s)

		if err != nil {
			return err
		}
	*/
	//get := d.currentOption.Value()
	//return d.currentOption.ValidateValue(v)
	return d.currentOption.Set(s)
}

func (d *configDialog) Run() error {
	d.currentCommand = d.App.ActiveCommand()
	return d.dialogApp.Run(d.askForFile())
}

func (d *configDialog) context() string {
	var bd strings.Builder

	if d.currentFile != "" {
		bd.WriteString("    file: '" + d.currentFile + "'\n")
	}

	if d.currentCommandName != "" {
		bd.WriteString("    command: '" + d.currentCommandName + "'\n")
	}

	if d.currentCommandHelp != "" {
		bd.WriteString("    command info: '" + d.currentCommandHelp + "'\n")
	}

	if d.currentOption != nil {
		bd.WriteString("    option: '" + d.currentOption.Name() + "'\n")
	}

	if d.currentValue != "" {
		bd.WriteString("    value: '" + d.currentValue + "'\n")
	}

	if d.currentValueType != "" {
		bd.WriteString("    type: '" + d.currentValueType + "'\n")
	}

	if d.currentValueHelp != "" {
		bd.WriteString("    help: '" + d.currentValueHelp + "'\n")
	}

	return bd.String() + "\n"
}

func (d *configDialog) loadValues() (err error) {
	d.App.Reset()

	switch d.currentFile {
	case "user":
		d.values, err = d.runner.UserConfig.Load()
	case "local":
		d.values, err = d.runner.LocalConfig.Load()
	case "global":
		d.values, err = d.runner.GlobalConfig.Load()
		//case "env vars":
		//err = d.config.MergeEnv2()
	}

	if err != nil {
		return err
	}

	/*
		d.config.EachValue(func(name string, value interface{}) {
			// don't use lastarg
			if name != "" {
				d.values[[2]string{"", name}] = fmt.Sprintf("%v", value)
			}
		})
	*/

	/*
		for _, cmd := range d.App.Commands {
			cname := cmd.Name()
			if skipCommands[cname] {
				continue
			}


			cmd.EachValue(func(name string, value interface{}) {
				// don't use lastarg
				if name != "" {
					d.values[[2]string{cname, name}] = fmt.Sprintf("%v", value)
				}
			})
		}
	*/

	return nil
}

func (d *configDialog) askForFile() dialog.Screen {
	d.currentFile = ""
	d.currentCommandName = ""
	d.currentCommandHelp = ""
	d.currentOption = nil
	d.currentValue = ""
	d.currentValueType = ""
	d.currentValueHelp = ""

	question := "Please select the configuration file"
	var menu = []string{"local", "user", "global"}

	sc := d.dialogApp.NewSelect(question, menu, func(chosen string) dialog.Screen {
		var err error
		if d.currentFile != chosen {
			d.currentFile = chosen
			err = d.loadValues()
		}

		if err != nil {
			sc := d.dialogApp.NewError(err.Error())
			sc.SetOptions(dialog.WithName("Error while loading values"))
		}

		return d.askForAction()
	})

	sc.SetOptions(
		dialog.WithValidator(dialog.NonEmpty),
		dialog.WithPrefilled("local"),
	)

	return sc
}

func (d *configDialog) askForCommand() dialog.Screen {
	d.currentCommandName = ""
	d.currentOption = nil
	d.currentCommandHelp = ""
	d.currentValue = ""
	d.currentValueType = ""
	d.currentValueHelp = ""

	question := d.context() + "Please select the (sub)command"
	without := "--without command--"
	var menu = []string{without}

	for _, cmd := range d.App.Commands {
		name := cmd.Name()
		if skipCommands[name] {
			continue
		}
		menu = append(menu, name)
	}

	sc := d.dialogApp.NewSelect(question, menu, func(chosen string) dialog.Screen {
		if chosen == without {
			d.currentCommandName = ""
			d.currentCommand = d.App.ActiveCommand()
			d.currentCommandHelp = ""
		} else {
			idx := d.App.cmdMap[chosen]
			d.currentCommandName = chosen
			d.currentCommand = d.App.Commands[idx]
			d.currentCommandHelp = shorten(d.currentCommand.Help())
		}

		return d.askForOption()
	})

	sc.SetOptions(
		dialog.WithValidator(dialog.NonEmpty),
		dialog.WithPrefilled(without),
	)

	return sc
}

const maxlen = 25

func shorten(h string) string {
	if len(h) > maxlen {
		return h[:maxlen] + "..."
	}

	return h
}

func (d *configDialog) askForOption() dialog.Screen {
	d.currentOption = nil
	d.currentValue = ""
	d.currentValueType = ""
	d.currentValueHelp = ""

	question := d.context() + "Please select the option/flag"
	var menu = []string{}

	for _, opt := range d.currentCommand.options() {
		name := opt.Name()
		// don't use lastargs
		if name != "" {
			menu = append(menu, name)
		}
	}

	if len(menu) == 0 {
		return d.dialogApp.NewError("no option found")
	}

	sc := d.dialogApp.NewSelect(question, menu, func(chosen string) dialog.Screen {
		/*
			if chosen == d.currentCommand.lastArgName() {
				chosen = ""
			}
		*/
		idx := d.currentCommand.optMap()[chosen]
		d.currentOption = d.currentCommand.options()[idx]
		d.currentValue = shorten(d.currentVal())
		d.currentValueType = d.currentOption.Type()
		d.currentValueHelp = shorten(d.currentOption.Help())

		return d.askForOptionAction()
	})

	sc.SetOptions(
		dialog.WithValidator(dialog.NonEmpty),
		dialog.WithPrefilled(menu[0]),
	)

	return sc
}

func (d *configDialog) askForOptionAction() dialog.Screen {
	question := d.context() + "What do you want to do with the option/flag?"
	var menu = []string{
		"1) show value",
		"2) show help",
		"3) set value",
		"4) unset value",
		"5) back to general actions",
	}

	sc := d.dialogApp.NewSelect(question, menu, func(chosen string) dialog.Screen {
		switch chosen {
		case "1) show value":
			return d.showValue()
		case "2) show help":
			return d.showHelp()
		case "3) set value":
			return d.changeValue()
		case "4) unset value":
			return d.unsetValue()
		case "5) back to general actions":
			return d.askForAction()
		default:
			panic("must not happen")
		}
	})

	sc.SetOptions(
		dialog.WithValidator(dialog.NonEmpty),
		dialog.WithPrefilled("3) set value"),
	)

	return sc
}

func (d *configDialog) askForAction() dialog.Screen {
	question := d.context() + "What do you want to do"
	var menu = []string{
		"1) show all values",
		"2) save to file",
		"3) delete all values",
		"4) select option/flag",
		"5) select command",
		"6) select config file",
	}

	sc := d.dialogApp.NewSelect(question, menu, func(chosen string) dialog.Screen {
		switch chosen {
		case "1) show all values":
			return d.showChanges()
		case "4) select option/flag":
			return d.askForOption()
		case "5) select command":
			return d.askForCommand()
		case "2) save to file":
			return d.saveChanges()
		case "3) delete all values":
			return d.dropChanges()
		case "6) select config file":
			return d.askForFile()
		default:
			panic("must not happen")
		}
	})

	sc.SetOptions(
		dialog.WithValidator(dialog.NonEmpty),
		dialog.WithPrefilled("1) show all values"),
	)

	return sc
}

func (d *configDialog) inspectValues() string {
	var bd strings.Builder

	m := map[string]map[string]string{}

	for k, v := range d.values {
		mm := m[k.Command]
		if mm == nil {
			mm = map[string]string{}
		}
		mm[k.Option] = v

		m[k.Command] = mm
	}

	if len(m[""]) > 0 {
		bd.WriteString("\n-- no command --\n")
	}

	for k, v := range m[""] {
		bd.WriteString("    " + k + ": " + v + "\n")
	}

	for kk, mm := range m {
		if kk == "" {
			continue
		}

		bd.WriteString("\ncommand: '" + kk + "' --\n")

		for k, v := range mm {
			bd.WriteString("    " + k + ": " + v + "\n")
		}
	}

	return bd.String() + "\n"
}

func (d *configDialog) showChanges() dialog.Screen {
	question := "Here are the current values:\n\n" + d.inspectValues() + "\npress ENTER to continue"
	sc := d.dialogApp.NewOutput(question, d.askForAction)

	sc.SetOptions()

	return sc
}

func (d *configDialog) dropChanges() dialog.Screen {
	d.values = values{}
	question := "The values are deleted " + "\npress ENTER to continue"
	sc := d.dialogApp.NewOutput(question, d.askForAction)

	sc.SetOptions()

	return sc
}

func (d *configDialog) applyChanges() (err error) {
	var loc *file

	d.App.Reset()

	switch d.currentFile {
	case "user":
		loc = d.App.run.UserConfig
	case "local":
		loc = d.App.run.LocalConfig
	case "global":
		loc = d.App.run.GlobalConfig
	}

	for k, v := range d.values {
		if k.Command == "" {
			idx := d.App.cmdMap[k.Command]
			cmd := d.App.Commands[idx]

			// not sure, if this is correct
			/*
				if d.config.lastArgName() == v {
					v = ""
				}
			*/
			idx2 := cmd.optMap()[k.Option]
			err = cmd.options()[idx2].Set(v)

			//			err = d.config.set(k.Option, v, loc)
		} else {
			idx := d.App.cmdMap[k.Command]
			cmd := d.App.Commands[idx]
			// not sure, if this is correct
			/*
				if cmd.lastArgName() == v {
					v = ""
				}
			*/
			//idx2 := cmd.optMap()[k.Option]
			opt := cmd.GetOptionByName(k.Option)
			if opt != nil {
				err = opt.Set(v)
			}

		}

		if err != nil {
			return err
		}
	}

	return loc.Save(d.values)

	/*
	   switch d.currentFile {
	   case "user":

	   	return d.config.SaveToUser()

	   case "local":

	   	return d.config.SaveToLocal()

	   case "global":

	   		return d.config.SaveToGlobals()
	   	}

	   return nil
	*/
}

func (d *configDialog) saveChanges() dialog.Screen {
	question := "saved successfully" + "\npress ENTER to continue"
	err := d.applyChanges()
	if err != nil {
		question = "error while saving: " + err.Error()
	}

	sc := d.dialogApp.NewOutput(question, d.askForAction)

	sc.SetOptions()

	return sc
}

func (d *configDialog) currentVal() string {
	if d.currentCommand == nil {
		return ""
	}
	if d.currentOption == nil {
		return ""
	}
	return d.values[valueIndex{Command: d.currentCommandName, Option: d.currentOption.Name()}]
}

func (d *configDialog) showValue() dialog.Screen {
	question := d.context() + "value is: \n" + d.currentVal() + "\n"
	sc := d.dialogApp.NewOutput(question, func() dialog.Screen {
		return d.askForOptionAction()
	})

	sc.SetOptions()

	return sc
}

func (d *configDialog) showHelp() dialog.Screen {
	question := d.context() + "Help: \n" + d.currentOption.Help() + "\n"
	sc := d.dialogApp.NewOutput(question, func() dialog.Screen {
		return d.askForOptionAction()
	})

	sc.SetOptions()

	return sc
}

func (d *configDialog) unsetValue() dialog.Screen {
	d.currentValue = ""
	delete(d.values, valueIndex{Command: d.currentCommandName, Option: d.currentOption.Name()})

	question := "successfully unsetted" + "\npress ENTER to continue"
	sc := d.dialogApp.NewOutput(question, d.askForAction)

	sc.SetOptions()

	return sc
}

func (d *configDialog) changeValue() dialog.Screen {
	d.currentValue = ""
	question := d.context() + "value:"
	sc := d.dialogApp.NewInput(question, func(inp string) dialog.Screen {
		d.currentValue = shorten(inp)
		name := d.currentOption.Name()
		d.values[valueIndex{Command: d.currentCommandName, Option: name}] = inp
		return d.askForOptionAction()
	})

	var valid dialog.ValidatorFunc = func(vs interface{}) error {
		return d.validateValue(vs.(string))
	}

	sc.SetOptions(
		dialog.WithValidator(valid),
		dialog.WithPrefilled(d.currentVal()),
	)

	return sc
}
