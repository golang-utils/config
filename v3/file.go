package config

import (
	"bytes"
	"fmt"
	"io"
	"path/filepath"
	"strings"

	//	"time"

	"gitlab.com/golang-utils/fs"
	"gitlab.com/golang-utils/fs/path"

	appdir "github.com/emersion/go-appdir"
	toml "github.com/pelletier/go-toml"
)

var (
	//USER_DIR    string
	CONFIG_DIR  string
	GLOBAL_DIRS string // colon separated list to look for
	WORKING_DIR string
	CONFIG_EXT  = ".conf"
	//ENV         []string

// ARGS        []string
)

type fileType string

func (f fileType) String() string {
	return string(f)
}

func (f fileType) Path(appname string) string {
	switch f {
	case LocalConfig:
		return filepath.Join(WORKING_DIR, CONFIG_DIR, appname, appname+CONFIG_EXT)
	case UserConfig:
		return filepath.Join(appdir.New(appname).UserConfig(), appname+CONFIG_EXT)
	case GlobalConfig:
		return filepath.Join(splitGlobals()[0], appname, appname+CONFIG_EXT)
	}
	return ""
}

const (
	LocalConfig  fileType = "local"
	UserConfig   fileType = "user"
	GlobalConfig fileType = "global"
)

type file struct {
	ft  fileType
	App *App
	//filesys fs.FS
	filesys fs.FS
}

func newFile(a *App, ft fileType, fsys fs.FS) *file {
	return &file{
		App:     a,
		ft:      ft,
		filesys: fsys,
	}
}

func (f *file) Exists() bool {
	return f.filesys.Exists(f.LocationFile())
}

func (f *file) LocationFile() path.Relative {
	return path.MustFileFromSystem(f.ft.Path(f.App.Name)).RootRelative()
}

func (f *file) Location() string {
	return path.MustFileFromSystem(f.ft.Path(f.App.Name)).String()
}

func (f *file) merge(rd io.Reader, location string) (vals values, err error) {

	vals = values{}
	//fmt.Printf("merging values into %#v\n", c.values)

	wrapErr := func(err error) error {
		//return InvalidConfigFileError{location, c.version, err}
		return err
	}

	t, err := toml.LoadReader(rd)

	if err != nil {
		return nil, wrapErr(err)
	}

	// TODO check, if it is still correct, to get it via Get?
	version := fmt.Sprintf("%v", t.Get("version"))

	differentVersions := version != f.App.Version.String()

	_ = differentVersions

	//var keys = map[string]bool{}
	/*
		var valBuf bytes.Buffer
		var key string
		var subcommand string
	*/

	setValue := func(subcommand string, key string, val string) error {
		key = strings.TrimLeft(key, "-")
		vals[valueIndex{Command: subcommand, Option: key}] = val
		return nil
	}

	var getValString = func(ttt *toml.Tree, pth string) string {
		v := ttt.Get(pth)

		//fmt.Printf("%q -> %v [%T]\n", k, v, v)

		switch x := v.(type) {
		case Time:
			return x.String()
			//		case time.Time:
			//		return TimeWithFormat{Time: x, Format: string(DateTimeGeneral)}.String()
		default:
			return fmt.Sprintf("%v", v)
		}

	}

	for _, k := range t.Keys() {
		if k == "version" {
			continue
		}

		vvvv := t.Get(k)
		//fmt.Printf("pth: %#v: %T\n", k, vvvv)

		if tt, isTree := vvvv.(*toml.Tree); isTree {
			for _, kk := range tt.Keys() {
				val := getValString(tt, kk)
				err = setValue(k, kk, val)
				if err != nil {
					return nil, wrapErr(fmt.Errorf("can't set %q to %q for subcommand %q: %v", kk, val, k, err))
				}
			}
		} else {
			val := getValString(t, k)
			err = setValue("", k, val)
			if err != nil {
				return nil, wrapErr(fmt.Errorf("can't set %q to %q: %v", k, val, err))
			}
		}
	}

	return vals, nil
}

func (f *file) Save(vals values) (err error) {
	p := f.LocationFile()

	fs.MkDirAll(f.filesys, p.Dir())

	backup, errBackup := fs.ReadFile(f.filesys, p)

	if errBackup == nil {

		//if rd, errBackup := f.filesys.Open(path); errBackup == nil {
		//backup, errBackup = ioutil.ReadAll(rd)
		//rd.Close()
		//backupInfo, errInfo := f.filesys.Stat(path)
		// don't write anything, if we have no config values
		if len(vals) == 0 {
			return f.filesys.Delete(p, false)
			// files exist, but will be deleted (no config values)
			//if errInfo == nil {
			//	return f.filesys.Remove(path)
			//}
			// files does not exist, we have no values, so lets do nothing
			//return nil
		}

		/*
			if errBackup != nil {
				backup = []byte{}
			}

			if errInfo == nil {
				perm = backupInfo.Mode()
			}
		*/
	}

	err = f.writeConfigValues(vals, p)

	if err != nil {
		fs.WriteFile(f.filesys, p, backup, false)
	}

	return err
}

func (f *file) writeConfigValues(vals values, file path.Relative) error {

	//te := toml.NewEncoder(file)
	//defer file.Close()

	m := map[string]interface{}{}
	m["version"] = f.App.Version.String()

	//fmt.Printf("writing config file: values: %#v\n", c.values)

	for k, v := range vals {

		if k.Option == "" {
			continue
		}

		switch k.Command {
		case "config":
			continue
		case "help":
			continue
		case "version":
			continue
		}

		cmd := f.App.GetCommandByName(k.Command)

		opt := cmd.GetOptionByName(k.Option)
		err := opt.Set(v)

		if err != nil {
			return fmt.Errorf("could not set option %q of command %q to %v: %s",
				k.Option, k.Command, v, err.Error(),
			)
		}

		vv := opt.Value()

		// do nothing for empty values
		if vv == nil {
			continue
		}

		if k.Command == "" {
			// TODO fix the reading: strip away the - sign for options
			m["--"+k.Option] = v
		} else {
			var mm map[string]interface{}

			mx, has := m[k.Command]

			if !has {
				mm = map[string]interface{}{}
			} else {
				mm = mx.(map[string]interface{})
			}

			mm["--"+k.Option] = v

			if len(mm) > 0 {
				m[k.Command] = mm
			}
		}
	}

	var bf bytes.Buffer

	prom := toml.NewEncoder(&bf).PromoteAnonymous(true)
	if err := prom.Encode(m); err != nil {
		return err
	}

	return f.filesys.Write(file, fs.ReadCloser(&bf), false)
}

// Load loads the values, but returns no error, if the file does not exist
func (f *file) Load() (vals values, err error) {
	vals = values{}

	file := f.LocationFile()

	bt, err0 := fs.ReadFile(f.filesys, file)

	if err0 != nil {
		return nil, err0
	}

	vals, err = f.merge(bytes.NewReader(bt), f.Location())

	if err != nil {
		return nil, err
	}

	return
}

var _ valueSource = &file{}
