package config

import (
	"errors"
	"fmt"

	"gitlab.com/golang-utils/fs"
)

type run struct {
	App  *App
	Args *args
	Env  *env
	//ConfigDialog *dialogConfig
	//AppDialog    *Dialog
	UserConfig   *file
	GlobalConfig *file
	LocalConfig  *file
}

func newRun(a *App, args []string, fsys fs.FS) *run {
	r := &run{
		App:  a,
		Args: newArgs(a, args),
		Env:  newEnv(a),
		//ConfigDialog: newDialog(a),
		//AppDialog:    newDialog(a),
	}

	r.UserConfig = newFile(a, UserConfig, fsys)
	r.GlobalConfig = newFile(a, GlobalConfig, fsys)
	r.LocalConfig = newFile(a, LocalConfig, fsys)
	return r
}

func (r *run) LoadAll() (vals values, over []valueIndex, err error) {
	vals = r.App.DefaultValues()

	err = r.setOptions(vals)

	if err != nil {
		err = fmt.Errorf("could not set default values: %s", err)
		return
	}

	globs, err := r.GlobalConfig.Load()

	if err != nil {
		err = fmt.Errorf("could not read global configuration: %s", err.Error())
		return
	}

	over = globs.MergeInto(vals)

	err = r.setOptions(vals)

	if err != nil {
		err = fmt.Errorf("could not set global config values: %s", err)
		return
	}

	users, err := r.UserConfig.Load()

	if err != nil {
		err = fmt.Errorf("could not read user configuration: %s", err.Error())
		return
	}

	over = append(over, users.MergeInto(vals)...)

	err = r.setOptions(vals)

	if err != nil {
		err = fmt.Errorf("could not set user config values: %s", err)
		return
	}

	locals, err := r.LocalConfig.Load()

	if err != nil {
		err = fmt.Errorf("could not read local configuration: %s", err.Error())
		return
	}

	over = append(over, locals.MergeInto(vals)...)

	err = r.setOptions(vals)

	if err != nil {
		err = fmt.Errorf("could not set local config values: %s", err)
		return
	}

	envs, err := r.Env.Load()

	if err != nil {
		err = fmt.Errorf("could not read environmental variables: %s", err.Error())
		return
	}

	over = append(over, envs.MergeInto(vals)...)

	err = r.setOptions(vals)

	if err != nil {
		err = fmt.Errorf("could not set environmental values: %s", err)
		return
	}

	args, err := r.Args.Load()

	if err != nil {
		err = fmt.Errorf("could not read args: %s", err.Error())
		return
	}

	over = append(over, args.MergeInto(vals)...)
	err = r.Args.Command.checkMissing(vals)

	if err != nil {
		return
	}

	err = r.setOptions(vals)
	return
}

func (r *run) setOptions(vals values) error {
	var errs []error

	for k, v := range vals {

		cmd := r.App.GetCommandByName(k.Command)

		if cmd == nil {
			err := fmt.Errorf("unknown command: %v", k.Command)
			errs = append(errs, err)
			continue
		}
		opt := cmd.GetOptionByName(k.Option)

		if opt == nil {
			err := fmt.Errorf("unknown option: %v (set options) for command %s", k.Option, k.Command)
			errs = append(errs, err)
			continue
		}

		err := opt.Set(v)

		if err != nil {
			errs = append(errs, err)
		}
	}

	if len(errs) > 0 {
		return errors.Join(errs...)
	}

	return nil

}
