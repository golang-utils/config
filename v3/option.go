package config

type optionInterface interface {
	Name() string
	Help() string
	Value() any
	IsSet() bool
	Set(s string) error
	Default() any
	Required() bool
	ShortFlag() string
	Command() string
	Type() string
	valueString() string
}

type Option[T any] struct {
	name         string
	help         string
	type_        TypeHandler[T]
	validator    Validator[T]
	target       *T
	value        T
	isSet        bool
	defaultValue *T
	required     bool
	shortflag    string
	command      *Command
}

func (o *Option[T]) valueString() string {
	return o.type_.ToString(o.value)
	/*
		switch o.Type() {
		case "json", "map[string]any":
			bt, err := json.Marshal(o.value)
			if err != nil {
				return err.Error()
			}
			return string(bt)
		default:
			return fmt.Sprintf("%v", o.value)
		}
	*/
}

func (o *Option[T]) ShortFlag() string {
	return o.shortflag
}

func (o *Option[T]) Required() bool {
	return o.required
}

func (o *Option[T]) Type() string {
	return o.type_.Type()
}

func (o *Option[T]) Default() any {
	if o.defaultValue == nil {
		return nil
	}
	return any(*o.defaultValue)
}

func (o *Option[T]) SetDefault(v T) *Option[T] {
	o.defaultValue = &v
	return o
}

// maybe that can just become a method of Type ? (and wrapped for further validations?)
func (o *Option[T]) SetValidator(v Validator[T]) *Option[T] {
	o.validator = v
	return o
}

func (o *Option[T]) SetRequired() *Option[T] {
	o.required = true
	return o
}

func (o *Option[T]) SetAsLast() *Option[T] {
	o.command.setAsLastOption(o.name)
	return o
}

func (o *Option[T]) SetShortflag(r rune) *Option[T] {
	o.shortflag = string(r)
	return o
}

func (o *Option[T]) SetHelp(str string) *Option[T] {
	o.help = str
	return o
}

func (o *Option[T]) Name() string {
	return o.name
}

func (o *Option[T]) Command() string {
	return o.command.Name()
}

func (o *Option[T]) Help() string {
	return o.help
}

func (o *Option[T]) IsSet() bool {
	return o.isSet
}

func (o *Option[T]) Value() any {
	return any(o.value)
}

func (o *Option[T]) Set(s string) error {
	var err error
	o.isSet = true
	o.value, err = o.type_.Parse(s)
	if err != nil {
		return err
	}
	err = o.Validate()
	if err != nil {
		return err
	}

	if o.target != nil {
		*o.target = o.value
	}
	return nil
}

func (o *Option[T]) Validate() error {
	if o.validator == nil {
		return nil
	}
	return o.validator.Validate(o.value)
}

func (o *Option[T]) Get() (v T) {
	return o.value
}
