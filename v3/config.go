package config

// TODO integrate scaffolding, auch scaffolding option für scaffolding basierend auf command mit optionen
//  (json wird durch interaktives ausfüllen der option gesetzt und template kommt vom aufruf im code), dabei
// wäre ein mögliches mehrfachaufrufen auch cool
// TODO test invalid names (app, command, option)
// TODO integrate tab-completion from cobra (möglichst via live-callback)

type valueIndex struct {
	Command string
	Option  string
}

type values map[valueIndex]string

func (v values) Set(o optionInterface, val string) {
	v[valueIndex{Command: o.Command(), Option: o.Name()}] = val
}

// MergeInto merges the values into the target and returns the keys that were overwritten
func (v values) MergeInto(target values) (overwritten []valueIndex) {
	for k, val := range v {
		target[k] = val
		overwritten = append(overwritten, k)
	}

	return overwritten
}

type valueSource interface {
	Location() string
	Load() (values, error)
}

const (
	MaxLenOptionName  = 18
	MaxLenCommandName = 20
	MaxLenAppName     = 30
	DateFormat        = Datetime("YYYY-MM-DD")
	TimeFormat        = Datetime("hh:mm:ss")
	TimeFormatShort   = Datetime("hh:mm")

// DateTimeFormat  = datetimeParser("YYYY-MM-DD hh:mm:ss")
// DateTimeGeneral = datetimeParser("YYYY-MM-DD hh:mm:ss ZZZZ ZZZ")
)
