package config

import (
	"fmt"
	"testing"
	// "gitlab.com/golang-utils/config/v3/types"
)

type byteType struct{}

func (byteType) Parse(s string) (b byte, err error) {
	bt := []byte(s)
	if len(bt) != 1 {
		return 0, fmt.Errorf("invalid byte: %q", s)
	}

	return bt[0], nil
}

func (byteType) ToString(b byte) string {
	return string(b)
}

func (byteType) Type() string {
	return "<byte>"
}

func TestUsageLeft(t *testing.T) {
	t.Skip()
	app := NewApp(nil, "testapp")

	var result = struct {
		String           string
		Json             Json
		Path             Path
		Int              int
		Date             Time
		Time             Time
		TimeShort        Time
		DateTime         Time
		Float            float64
		Bool             bool
		Select           string
		SelectInput      string
		MultiSelect      []string
		MultiSelectInput []string
		Ints             []int
		Floats           []float64
		Strings          []string
		Custom           byte
	}{
		Json: Json{},
	}

	tests := []struct {
		option   optionInterface
		isLast   bool
		expected string
	}{
		{app.String(&result.String, "teststring"), false, "[--teststring=<string>]"},
		{app.String(&result.String, "teststringlast"), true, "[teststringlast=<string>]"},
		{app.String(&result.String, "teststringdef").SetDefault("hu"), false, "[--teststringdef='hu']"},
		{app.String(&result.String, "teststringlastdef").SetDefault("ha"), true, "[teststringlastdef='ha']"},
		{app.String(&result.String, "teststringreq").SetRequired(), false, "--teststringreq=<string>"},
		{app.String(&result.String, "teststringlastreq").SetRequired(), true, "teststringlastreq=<string>"},

		{app.Json(&result.Json, "testjson"), false, "[--testjson=<json-map>]"},
		{app.Json(&result.Json, "testjsonlast"), true, "[testjsonlast=<json-map>]"},
		{app.Json(&result.Json, "testjsondef").SetDefault(map[string]any{"a": 23}), false, "[--testjsondef='{\"a\":23}']"},
		{app.Json(&result.Json, "testjsonlastdef").SetDefault(map[string]any{"a": 23}), true, "[testjsonlastdef='{\"a\":23}']"},
		{app.Json(&result.Json, "testjsonreq").SetRequired(), false, "--testjsonreq=<json-map>"},
		{app.Json(&result.Json, "testjsonlastreq").SetRequired(), true, "testjsonlastreq=<json-map>"},

		{app.Path(&result.Path, "testpath"), false, "[--testpath=<path>]"},
		{app.Path(&result.Path, "testpathlast"), true, "[testpathlast=<path>]"},
		{app.Path(&result.Path, "testpathdef").SetDefault(Path{Dirs: []string{"a", "b"}, File: "settings.json"}), false, "[--testpathdef='a\\b\\settings.json']"},
		{app.Path(&result.Path, "testpathlastdef").SetDefault(Path{Dirs: []string{"a", "b"}, File: "settings.json"}), true, "[testpathlastdef='a\\b\\settings.json']"},

		{app.Int(&result.Int, "testint"), false, "[--testint=<int>]"},
		{app.Int(&result.Int, "testintlast"), true, "[testintlast=<int>]"},
		{app.Int(&result.Int, "testintdef").SetDefault(50), false, "[--testintdef=50]"},
		{app.Int(&result.Int, "testintlastdef").SetDefault(100), true, "[testintlastdef=100]"},
		{app.Int(&result.Int, "testintreq").SetRequired(), false, "--testintreq=<int>"},
		{app.Int(&result.Int, "testintlastreq").SetRequired(), true, "testintlastreq=<int>"},

		{app.Date(&result.Date, "testdate"), false, "[--testdate=YYYY-MM-DD]"},
		{app.Date(&result.Date, "testdatelast"), true, "[testdatelast=YYYY-MM-DD]"},
		{app.Date(&result.Date, "testdatedef").SetDefault(DateFormat.MustParse("2001-11-23")), false, "[--testdatedef='2001-11-23']"},
		{app.Date(&result.Date, "testdatelastdef").SetDefault(DateFormat.MustParse("2007-10-03")), true, "[testdatelastdef='2007-10-03']"},
		{app.Date(&result.Date, "testdatereq").SetRequired(), false, "--testdatereq=YYYY-MM-DD"},
		{app.Date(&result.Date, "testdatelastreq").SetRequired(), true, "testdatelastreq=YYYY-MM-DD"},

		{app.Time(&result.Time, "testtime"), false, "[--testtime=hh:mm:ss]"},
		{app.Time(&result.Time, "testtimedef").SetDefault(TimeFormat.MustParse("20:10:03")), false, "[--testtimedef='20:10:03']"},

		{app.TimeShort(&result.TimeShort, "testtimeshort"), false, "[--testtimeshort=hh:mm]"},
		{app.TimeShort(&result.TimeShort, "testtimeshortdef").SetDefault(TimeFormatShort.MustParse("20:10")), false, "[--testtimeshortdef='20:10']"},

		{app.DateTime(&result.DateTime, "testdatetime", "YY-M-D hh:mm"), false, "[--testdatetime=YY-M-D hh:mm]"},
		{app.DateTime(&result.DateTime, "testdatetimedef", "YY-M-D hh:mm").SetDefault(Datetime("YY-M-D hh:mm").MustParse("22-5-14 10:12")), false, "[--testdatetimedef='22-5-14 10:12']"},

		{app.Float(&result.Float, "testfloat"), false, "[--testfloat=<float>]"},
		{app.Float(&result.Float, "testfloatdef").SetDefault(50.01), false, "[--testfloatdef=50.01]"},

		{app.Bool(&result.Bool, "testbool"), false, "[--testbool]"},
		{app.Bool(&result.Bool, "testbooldef").SetDefault(true), false, "[--testbooldef=true]"},
		{app.Bool(&result.Bool, "testbooldeffalse").SetDefault(false), false, "[--testbooldeffalse=false]"},

		{app.Select(&result.Select, "testselect", []string{"a", "b"}), false, "[--testselect=<a|b>]"},
		{app.Select(&result.Select, "testselectdef", []string{"a", "b"}).SetDefault("b"), false, "[--testselectdef=b]"},

		{app.SelectInput(&result.SelectInput, "testselectinput", []string{"a", "b"}), false, "[--testselectinput=<a|b>]"},
		{app.SelectInput(&result.SelectInput, "testselectinputdef", []string{"a", "b"}).SetDefault("b"), false, "[--testselectinputdef=b]"},

		{app.MultiSelect(&result.MultiSelect, "testmultiselect", []string{"a", "b", "c"}), false, "[--testmultiselect=<a,b,c>]"},
		{app.MultiSelect(&result.MultiSelect, "testmultiselectdef", []string{"a", "b", "c"}).SetDefault([]string{"b", "c"}), false, "[--testmultiselectdef='b,c']"},

		{app.MultiSelectInput(&result.MultiSelectInput, "tmultiselectinput", []string{"a", "b", "c"}), false, "[--tmultiselectinput=<a,b,c>]"},
		{app.MultiSelectInput(&result.MultiSelectInput, "tmultiselectinputd", []string{"a", "b", "c"}).SetDefault([]string{"b", "c"}), false, "[--tmultiselectinputd='b,c']"},

		{app.Ints(&result.Ints, "testints"), false, "[--testints=<int,...>]"},
		{app.Ints(&result.Ints, "testintsdef").SetDefault([]int{5, 2}), false, "[--testintsdef=5,2]"},

		{app.Floats(&result.Floats, "testfloats"), false, "[--testfloats=<float,...>]"},
		{app.Floats(&result.Floats, "testfloatsdef").SetDefault([]float64{5.2, 2.5}), false, "[--testfloatsdef=5.2,2.5]"},

		{app.Strings(&result.Strings, "teststrings"), false, "[--teststrings=<string,...>]"},
		{app.Strings(&result.Strings, "teststringsdef").SetDefault([]string{"a", "c"}), false, "[--teststringsdef='a,c']"},

		{Custom[byte](app.Command, &result.Custom, "testcustom", byteType{}), false, "[--testcustom=<byte>]"},
		{Custom[byte](app.Command, &result.Custom, "testcustomdef", byteType{}).SetDefault(3), false, "[--testcustomdef=3]"},

		/*
			types:

			String()
			Json() {
			Custom[T any](c *Command, , type_ Type[T], format string)
			Path() {
			Int()
			Float()
			Bool()
			Select()
			SelectInput()
			MultiSelect()
			MultiSelectInput()
			Time()
			TimeShort()
			Date()
			DateTime()
			Ints()
			Floats()
			Strings()
		*/
	}

	//DateFormat
	//tm := TimeWithFormat{}
	//tm.

	for i, test := range tests {
		var u = usage{
			relaxed: map[string]bool{},
			skipped: map[string]bool{},
		}

		//u.

		u.option = test.option
		u.isLastOption = test.isLast
		if test.isLast {
			app.Command.lastOption = test.option.Name()
			//test.option.
		}

		expected := test.expected

		got := u.Left()

		if got != expected {
			t.Errorf("[%v] %s got: %q // expected: %q", i, test.option.Name(), got, expected)
		}
	}

}
