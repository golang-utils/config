package config

import (
	"bytes"
	"strings"
)

var leftWidth = 22
var totalWidth = 80

func pad(left string, right string) string {

	var line int = 0
	var bf bytes.Buffer

	leftLines := strings.Split(left, "\n")

	if len(leftLines) > 2 {
		panic("more than two lines not allowed for now")
	}

	numSpaces := leftWidth - len([]rune(leftLines[0]))
	bf.WriteString(leftLines[0])

	for i := 0; i < numSpaces; i++ {
		bf.WriteString(" ")
	}

	spaceRight := totalWidth - 1 - leftWidth
	right = strings.Replace(right, "\n", " \n ", -1)
	arr := strings.Split(right, " ")

	spaceRightAvailable := spaceRight
	for _, word := range arr {
		w := strings.TrimSpace(word)
		if w == "" && word != "\n" {
			continue
		}
		if len(w) > spaceRightAvailable || word == "\n" {
			bf.WriteString("\n")
			line += 1
			numSpaces = leftWidth
			if line == 1 && len(leftLines) == 2 {
				numSpaces = leftWidth - len([]rune(leftLines[1]))
				bf.WriteString(leftLines[1])
			}
			for i := 0; i < numSpaces; i++ {
				bf.WriteString(" ")
			}
			spaceRightAvailable = spaceRight
		}

		bf.WriteString(w + " ")
		spaceRightAvailable -= len(w) + 1
	}
	return bf.String()
}
