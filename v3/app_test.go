package config

import (
	"fmt"
	"strings"
	"testing"

	"gitlab.com/golang-utils/fs/filesystems/mockfs"
	"gitlab.com/golang-utils/fs/path"
	// "gitlab.com/golang-utils/config/v3/types"
)

/*

What to test:

- reading
	[main, sub]
		[normal names, dashed-names, underscored-names, double-dashed names]
			[types, customtypes, customvalidators]
				[with/out shortcuts]
					[required/non-required]
						[default/non-default]
							[lastarg/non-lastarg]
								[config files, env, args]
									[non-inherited, inherited, overwritten]
	what to test:
		- error (type name as String)
		- name
		- Get value (per string %v)
		- IsSet
		- IsValid/Set
		- Default
		- left appearance in help

- writing
	[main, sub]
		[normal names, dashed-names, underscored-names, double-dashed names]
			[types, customtypes, customvalidators]
				[with/out shortcuts]
					[required/non-required]
						[default/non-default]
							[lastarg/non-lastarg]
								[set by arguments, env, library and config files]
									[non-inherited, inherited, overwritten]
	what to test:
		- error (type name as String)
		- is config file created if not there
		- is name and value correct and in correct config file
		- can it be read in again properly?


- help command
- version command
- option setting and reading via config command

test by hand:

- scaffolding
- gui

*/

func getRunner(app *App, args ...string) *run {
	fsys, err := mockfs.New(path.MustWD())
	if err != nil {
		panic(err.Error())
	}
	runner := app.newRunFS(args, fsys)
	//runner.Args.args = nil
	return runner
}

// TODO test custom types and custom validators
func TestReadingXXL(t *testing.T) {
	t.Skip()
	return
	type test_option struct {
		method     string
		name       string
		help       string
		defaultval any
		additional any // additional parameter for DateTime(), Select(), SelectInput(), MultiSelect(), MultiSelectInput()
		setValue   string
	}
	type test struct {
		input struct {
			command    string // "" -> main command
			option     test_option
			shortcut   rune // "" -> no shortcut
			required   bool
			lastarg    bool
			setby      string // possible values: args,env,lib,global,user,local,default
			skipAllBut []string
			skip       []string
			relax      []string
			overwrite  bool
		}

		description string

		expected struct {
			errorType  string
			value      string
			isset      bool
			defaultval any
			name       string
			helpstr    string
		}
	}

	commands := []string{"", "sub", "sub-with-dash", "sub_with_underscore"}
	/* types

	2 params

	String()
	Int()
	Float()
	Bool()
	Time()
	TimeShort()
	Date()
	Path()
	Json()
	Ints()
	Floats()
	Strings()


	3 params

	DateTime()
	Select()
	SelectInput()
	MultiSelect()
	MultiSelectInput()

	Custom[T any](c *Command, , type_ Type[T])
	*/

	options := []test_option{
		{method: "String", name: "teststring", help: "simple string", defaultval: nil, setValue: "string set"},
		{method: "String", name: "teststringdefault", help: "simple string with default", defaultval: "default-string", setValue: "string set"},
		{method: "String", name: "test-string", help: "string with dashed name", defaultval: nil, setValue: "string set"},
		{method: "String", name: "test_string", help: "string with underscored name", defaultval: nil, setValue: "string set"},

		{method: "Int", name: "testint", help: "simple int", setValue: "42"},
		{method: "Int", name: "testintdefault", help: "simple int with default value", defaultval: 200, setValue: "42"},

		{method: "Float", name: "testfloat", help: "simple float", defaultval: nil, setValue: "5.2"},

		{method: "Bool", name: "testbool", help: "simple bool", defaultval: nil, setValue: "true"},

		{method: "Time", name: "testtime", help: "simple time", defaultval: nil, setValue: "12:34:39"},

		{method: "TimeShort", name: "testtimeshort", help: "simple timeshort", defaultval: nil, setValue: "12:34"},

		{method: "Date", name: "testdate", help: "simple date", defaultval: nil, setValue: "2012-04-19"},

		{method: "Path", name: "testpath", help: "simple path", defaultval: nil, setValue: "a\\b\\x.txt"},

		{method: "Json", name: "testjson", help: "simple json", defaultval: nil, setValue: "{\"a\":5}"},

		{method: "Ints", name: "testints", help: "simple ints", defaultval: nil, setValue: "5,4"},

		{method: "Floats", name: "testfloats", help: "simple floats", defaultval: nil, setValue: "5.2,2.5"},

		{method: "Strings", name: "teststrings", help: "simple strings", defaultval: nil, setValue: "a,b"},

		{method: "DateTime", name: "testdatetime", help: "simple datetime", additional: "YY-M-D hh", defaultval: nil, setValue: "20-4-12 06"},

		{method: "Select", name: "testselect", help: "simple select", additional: []string{"a", "b"}, defaultval: nil, setValue: "b"},

		{method: "SelectInput", name: "testselectinput", help: "simple selectinput", additional: []string{"a", "b"}, defaultval: nil, setValue: "b"},

		{method: "MultiSelect", name: "testmultiselect", help: "simple multiselect", additional: []string{"a", "b", "c"}, defaultval: nil, setValue: "b,c"},

		{method: "MultiSelectInput", name: "testmultiselectinp", help: "simple multiselect input", additional: []string{"a", "b", "c"}, defaultval: nil, setValue: "b,c"},

		{method: "Custom", name: "testcustom", help: "simple custom type", additional: "byte", defaultval: nil, setValue: "3"},
	}

	var defaultrune rune
	shortcuts := []rune{
		defaultrune,
		'a', 'b',
		// '0',    // invalid ones
	}

	setbys := []string{"args", "default", "lib", "env", "local", "global", "user"}

	required := []bool{false, true}
	lastarg := []bool{false, true}
	overwrite := []bool{false, true}

	var tests []test

	for _, cmd := range commands {
		for _, opt := range options {
			for _, sb := range setbys {
				for _, sc := range shortcuts {
					for _, req := range required {
						for _, la := range lastarg {
							for _, ow := range overwrite {

								var te test
								te.input.command = cmd
								te.input.option = opt
								te.input.shortcut = sc
								te.input.required = req
								te.input.lastarg = la
								te.input.setby = sb
								//setby      string // possible values: args,env,lib,global,user,local
								//skipAllBut []string
								//skip       []string
								//relax      []string
								te.input.overwrite = ow

								descr := fmt.Sprintf("command: %s option: %s", cmd, opt.name)

								if te.input.option.defaultval != nil {
									te.expected.defaultval = te.input.option.defaultval
									descr += fmt.Sprintf(" defaultval: %s", te.input.option.defaultval)
								}

								descr += fmt.Sprintf(" setby: %s", te.input.setby)
								descr += fmt.Sprintf(" lastarg: %v", la)
								descr += fmt.Sprintf(" shortcut: %v", sc)
								descr += fmt.Sprintf(" required: %v", req)

								switch te.input.setby {
								case "default":
									te.expected.isset = false
									if te.input.option.defaultval != nil {
										te.expected.value = fmt.Sprintf("%v", te.input.option.defaultval)
									} else {
										switch te.input.option.method {
										case "Int":
											te.expected.value = "0"
										case "String":
											te.expected.value = ""
										case "Float":
											te.expected.value = "0"
										case "Bool":
											te.expected.value = "false"
										case "Time":
											te.expected.value = "" // "00:00:00"
										case "TimeShort":
											te.expected.value = "" // "00:00"
										case "Date":
											te.expected.value = "" // "0000-00-00"
										case "Path":
											te.expected.value = ""
										case "Json":
											te.expected.value = "null"
										case "Ints":
											te.expected.value = ""
										case "Floats":
											te.expected.value = ""
										case "Strings":
											te.expected.value = ""
										case "DateTime":
											te.expected.value = ""
										case "Select":
											te.expected.value = ""
										case "SelectInput":
											te.expected.value = ""
										case "MultiSelect":
											te.expected.value = ""
										case "MultiSelectInput":
											te.expected.value = ""
										case "Custom":
											switch te.input.option.additional.(string) {
											case "byte":
												te.expected.value = "\x00"
											default:
												panic("unknown custom type " + te.input.option.additional.(string))
											}

										default:
											panic("unsupported method " + te.input.option.method)
										}
									}
								default:
									te.expected.isset = true
									te.expected.value = te.input.option.setValue
								}

								te.description = descr

								te.expected.name = te.input.option.name
								te.expected.helpstr = te.input.option.help

								tests = append(tests, te)

								/*
									app := New("testapp")
									c := app.Command

									if cmd != "" {
										c = app.NewCommand(cmd, "command "+cmd)
									}

									var o Option

									switch opt.method {
									case "String":
										oo := c.String(opt.name, opt.help)
										if opt.defaultval != "" {
											oo.SetDefault(opt.defaultval)
										}

										if sc != "" {
											oo.SetShortflag(sc)
										}
										o = oo

									case "Int":
										oo := c.Int(opt.name, opt.help)
										if opt.defaultval != "" {
											oo.SetDefault(opt.defaultval)
										}

										if sc != "" {
											oo.SetShortflag(sc)
										}
										o = oo
									}
								*/

							}
						}
					}
				}
			}
		}
	}

	for i, te := range tests {

		errPrefix := fmt.Sprintf("[%v] %s:\n\t", i, te.description)

		app := NewApp(nil, "testapp")
		cmd := app.Command

		if te.input.command != "" {
			cmd = app.NewCommand(nil, te.input.command)
			cmd.SetHelp("command " + te.input.command)
		}

		var opt optionInterface

		var result = struct {
			String           string
			Int              int
			Float            float64
			Bool             bool
			Time             Time
			TimeShort        Time
			Date             Time
			Path             Path
			Json             Json
			Ints             []int
			Floats           []float64
			Bools            []bool
			Strings          []string
			Select           string
			SelectInput      string
			MultiSelect      []string
			MultiSelectInput []string
			Custom           byte
		}{
			Json: Json{},
		}

		switch te.input.option.method {
		case "String":
			oo := cmd.String(&result.String, te.input.option.name).SetHelp(te.input.option.help)

			if te.input.option.defaultval != nil {
				oo.SetDefault(te.input.option.defaultval.(string))
			}

			if te.input.shortcut != defaultrune {
				oo.SetShortflag(te.input.shortcut)
			}

			if te.input.required {
				oo.SetRequired()
			}

			if te.input.lastarg {
				oo.SetAsLast()
			}

			opt = oo

		case "Int":
			oo := cmd.Int(&result.Int, te.input.option.name).SetHelp(te.input.option.help)
			if te.input.option.defaultval != nil {
				oo.SetDefault(te.input.option.defaultval.(int))
			}

			if te.input.shortcut != defaultrune {
				oo.SetShortflag(te.input.shortcut)
			}

			if te.input.required {
				oo.SetRequired()
			}

			if te.input.lastarg {
				oo.SetAsLast()
			}

			opt = oo
		case "Float":
			oo := cmd.Float(&result.Float, te.input.option.name).SetHelp(te.input.option.help)
			if te.input.option.defaultval != nil {
				oo.SetDefault(te.input.option.defaultval.(float64))
			}

			if te.input.shortcut != defaultrune {
				oo.SetShortflag(te.input.shortcut)
			}

			if te.input.required {
				oo.SetRequired()
			}

			if te.input.lastarg {
				oo.SetAsLast()
			}

			opt = oo
		case "Bool":
			oo := cmd.Bool(&result.Bool, te.input.option.name).SetHelp(te.input.option.help)
			if te.input.option.defaultval != nil {
				oo.SetDefault(te.input.option.defaultval.(bool))
			}

			if te.input.shortcut != defaultrune {
				oo.SetShortflag(te.input.shortcut)
			}

			if te.input.required {
				oo.SetRequired()
			}

			if te.input.lastarg {
				oo.SetAsLast()
			}

			opt = oo
		case "Time":
			oo := cmd.Time(&result.Time, te.input.option.name).SetHelp(te.input.option.help)
			if te.input.option.defaultval != nil {
				oo.SetDefault(TimeFormat.MustParse(te.input.option.defaultval.(string)))
			}

			if te.input.shortcut != defaultrune {
				oo.SetShortflag(te.input.shortcut)
			}

			if te.input.required {
				oo.SetRequired()
			}

			if te.input.lastarg {
				oo.SetAsLast()
			}

			opt = oo
		case "TimeShort":
			oo := cmd.TimeShort(&result.TimeShort, te.input.option.name).SetHelp(te.input.option.help)
			if te.input.option.defaultval != nil {
				oo.SetDefault(TimeFormatShort.MustParse(te.input.option.defaultval.(string)))
			}

			if te.input.shortcut != defaultrune {
				oo.SetShortflag(te.input.shortcut)
			}

			if te.input.required {
				oo.SetRequired()
			}

			if te.input.lastarg {
				oo.SetAsLast()
			}

			opt = oo
		case "Date":
			oo := cmd.Date(&result.Date, te.input.option.name).SetHelp(te.input.option.help)
			if te.input.option.defaultval != nil {
				oo.SetDefault(DateFormat.MustParse(te.input.option.defaultval.(string)))
			}

			if te.input.shortcut != defaultrune {
				oo.SetShortflag(te.input.shortcut)
			}

			if te.input.required {
				oo.SetRequired()
			}

			if te.input.lastarg {
				oo.SetAsLast()
			}

			opt = oo
		case "Path":
			oo := cmd.Path(&result.Path, te.input.option.name).SetHelp(te.input.option.help)
			if te.input.option.defaultval != nil {
				oo.SetDefault(te.input.option.defaultval.(Path))
			}

			if te.input.shortcut != defaultrune {
				oo.SetShortflag(te.input.shortcut)
			}

			if te.input.required {
				oo.SetRequired()
			}

			if te.input.lastarg {
				oo.SetAsLast()
			}

			opt = oo
		case "Json":
			oo := cmd.Json(&result.Json, te.input.option.name).SetHelp(te.input.option.help)
			if te.input.option.defaultval != nil {
				oo.SetDefault(Json{}.MustParse(te.input.option.defaultval.(string)))
			}

			if te.input.shortcut != defaultrune {
				oo.SetShortflag(te.input.shortcut)
			}

			if te.input.required {
				oo.SetRequired()
			}

			if te.input.lastarg {
				oo.SetAsLast()
			}

			opt = oo
		case "Ints":
			oo := cmd.Ints(&result.Ints, te.input.option.name).SetHelp(te.input.option.help)
			if te.input.option.defaultval != nil {
				oo.SetDefault(te.input.option.defaultval.([]int))
			}

			if te.input.shortcut != defaultrune {
				oo.SetShortflag(te.input.shortcut)
			}

			if te.input.required {
				oo.SetRequired()
			}

			if te.input.lastarg {
				oo.SetAsLast()
			}

			opt = oo
		case "Floats":
			oo := cmd.Floats(&result.Floats, te.input.option.name).SetHelp(te.input.option.help)
			if te.input.option.defaultval != nil {
				oo.SetDefault(te.input.option.defaultval.([]float64))
			}

			if te.input.shortcut != defaultrune {
				oo.SetShortflag(te.input.shortcut)
			}

			if te.input.required {
				oo.SetRequired()
			}

			if te.input.lastarg {
				oo.SetAsLast()
			}

			opt = oo
		case "Strings":
			oo := cmd.Strings(&result.Strings, te.input.option.name).SetHelp(te.input.option.help)
			if te.input.option.defaultval != nil {
				oo.SetDefault(te.input.option.defaultval.([]string))
			}

			if te.input.shortcut != defaultrune {
				oo.SetShortflag(te.input.shortcut)
			}

			if te.input.required {
				oo.SetRequired()
			}

			if te.input.lastarg {
				oo.SetAsLast()
			}

			opt = oo
		case "DateTime":
			oo := cmd.DateTime(&result.Time, te.input.option.name, te.input.option.additional.(string)).SetHelp(te.input.option.help)
			if te.input.option.defaultval != nil {
				oo.SetDefault(Datetime(te.input.option.additional.(string)).MustParse(te.input.option.defaultval.(string)))
			}

			if te.input.shortcut != defaultrune {
				oo.SetShortflag(te.input.shortcut)
			}

			if te.input.required {
				oo.SetRequired()
			}

			if te.input.lastarg {
				oo.SetAsLast()
			}

			opt = oo
		case "Select":
			oo := cmd.Select(&result.Select, te.input.option.name, te.input.option.additional.([]string)).SetHelp(te.input.option.help)
			if te.input.option.defaultval != nil {
				oo.SetDefault(te.input.option.defaultval.(string))
			}

			if te.input.shortcut != defaultrune {
				oo.SetShortflag(te.input.shortcut)
			}

			if te.input.required {
				oo.SetRequired()
			}

			if te.input.lastarg {
				oo.SetAsLast()
			}

			opt = oo
		case "SelectInput":
			oo := cmd.SelectInput(&result.SelectInput, te.input.option.name, te.input.option.additional.([]string)).SetHelp(te.input.option.help)
			if te.input.option.defaultval != nil {
				oo.SetDefault(te.input.option.defaultval.(string))
			}

			if te.input.shortcut != defaultrune {
				oo.SetShortflag(te.input.shortcut)
			}

			if te.input.required {
				oo.SetRequired()
			}

			if te.input.lastarg {
				oo.SetAsLast()
			}

			opt = oo
		case "MultiSelect":
			oo := cmd.MultiSelect(&result.MultiSelect, te.input.option.name, te.input.option.additional.([]string)).SetHelp(te.input.option.help)
			if te.input.option.defaultval != nil {
				oo.SetDefault(te.input.option.defaultval.([]string))
			}

			if te.input.shortcut != defaultrune {
				oo.SetShortflag(te.input.shortcut)
			}

			if te.input.required {
				oo.SetRequired()
			}

			if te.input.lastarg {
				oo.SetAsLast()
			}

			opt = oo
		case "MultiSelectInput":
			oo := cmd.MultiSelectInput(&result.MultiSelectInput, te.input.option.name, te.input.option.additional.([]string)).SetHelp(te.input.option.help)
			if te.input.option.defaultval != nil {
				oo.SetDefault(te.input.option.defaultval.([]string))
			}

			if te.input.shortcut != defaultrune {
				oo.SetShortflag(te.input.shortcut)
			}

			if te.input.required {
				oo.SetRequired()
			}

			if te.input.lastarg {
				oo.SetAsLast()
			}

			opt = oo
		case "Custom":
			switch te.input.option.additional.(string) {
			case "byte":
				oo := Custom[byte](cmd, &result.Custom, te.input.option.name, byteType{}).SetHelp(te.input.option.help)
				if te.input.option.defaultval != nil {
					oo.SetDefault(te.input.option.defaultval.(byte))
				}

				if te.input.shortcut != defaultrune {
					oo.SetShortflag(te.input.shortcut)
				}

				if te.input.required {
					oo.SetRequired()
				}

				if te.input.lastarg {
					oo.SetAsLast()
				}

				opt = oo
			default:
				panic("unknown custom type " + te.input.option.additional.(string))
			}

		default:
			panic("unsupported method " + te.input.option.method)
		}

		run := getRunner(app)

		var err error

		switch te.input.setby {
		case "user":
			vals := values{}
			vals.Set(opt, te.input.option.setValue)
			run.Args.args = nil
			err = run.UserConfig.Save(vals)
			if err != nil {
				t.Errorf("could not save user config: %s", err.Error())
			}
			app.values, _, err = run.LoadAll()
		case "global":
			vals := values{}
			vals.Set(opt, te.input.option.setValue)
			run.Args.args = nil
			err = run.GlobalConfig.Save(vals)
			if err != nil {
				t.Errorf("could not save global config: %s", err.Error())
			}
			app.values, _, err = run.LoadAll()
		case "local":
			vals := values{}
			vals.Set(opt, te.input.option.setValue)
			run.Args.args = nil
			err = run.LocalConfig.Save(vals)
			if err != nil {
				t.Errorf("could not save local config: %s", err.Error())
			}
			app.values, _, err = run.LoadAll()
		case "env":
			//run.Env.env = []
			envkey := app.EnvVar(opt)
			run.Args.args = nil
			run.Env.env = []string{envkey + fmt.Sprintf("=%s", te.input.option.setValue)}
			app.values, _, err = run.LoadAll()
		case "lib":
			err = opt.Set(te.input.option.setValue)
			run.Args.args = nil
		case "args":
			var args []string
			if te.input.command != "" {
				args = append(args, te.input.command)
			}
			if te.input.lastarg {
				args = append(args, fmt.Sprintf("%s", te.input.option.setValue))
			} else {
				if te.input.shortcut == defaultrune {
					args = append(args, fmt.Sprintf("--%s='%s'", opt.Name(), te.input.option.setValue))
				} else {
					args = append(args, fmt.Sprintf("-%s='%s'", string(te.input.shortcut), te.input.option.setValue))
				}
			}

			run.Args.args = args

			app.values, _, err = run.LoadAll()
		case "default":
			run.Args.args = nil
			app.values, _, err = run.LoadAll()

		}

		//_ = vals

		if te.expected.errorType != "" {
			if err != nil {
				gotErrType := fmt.Sprintf("%T", err)
				if gotErrType != te.expected.errorType {
					t.Errorf(errPrefix+"expected error of type %s, but got %s", te.expected.errorType, gotErrType)
					continue
				}
			} else {
				t.Errorf(errPrefix+"expected error of type %s, but got nil", te.expected.errorType)
				continue
			}
		}

		if te.expected.name != opt.Name() {
			t.Errorf(errPrefix+"expected name of %q, but got %q", te.expected.name, opt.Name())
			continue
		}

		if te.expected.defaultval != opt.Default() {
			t.Errorf(errPrefix+"expected default of %v, but got %v", te.expected.defaultval, opt.Default())
			continue
		}

		if te.expected.helpstr != opt.Help() {
			t.Errorf(errPrefix+"expected helpstr of %q, but got %q", te.expected.helpstr, opt.Help())
			continue
		}

		/*
			gotIsset := opt.IsSet()
			if te.expected.isset != gotIsset {
				t.Errorf(errPrefix+"expected isset of %v, but got %v", te.expected.isset, gotIsset)
				continue
			}
		*/

		res := opt.valueString()

		if res != te.expected.value {
			t.Errorf(errPrefix+"expected value of %q, but got %q", te.expected.value, res)
			continue
		}

	}

}

func TestOverwrite(t *testing.T) {

	cmds := []string{"", "testa", "testb"}

	for _, cmd := range cmds {

		//fmt.Printf("COMMAND: %q\n", cmd)
		app := NewApp(nil, "testapp")

		var c *Command

		if cmd != "" {
			c = app.NewCommand(nil, cmd).SetHelp("ein befehl")
		} else {
			c = app.GetCommandByName("")
		}

		var result = struct {
			argWithoutDefault string
			argByDefault      string
			argByGlobal       string
			argByUser         string
			argByLocal        string
			argByEnv          string
			argByArgs         string
		}{}

		argWithoutDefault := c.String(&result.argWithoutDefault, "withoutdefault").SetHelp("should not have a default")
		argByDefault := c.String(&result.argByDefault, "bydefault").SetHelp("should get the default").SetDefault("default")
		argByGlobal := c.String(&result.argByGlobal, "byglobal").SetHelp("should get the global").SetDefault("default")
		argByUser := c.String(&result.argByUser, "byuser").SetHelp("should get the user").SetDefault("default")
		argByLocal := c.String(&result.argByLocal, "bylocal").SetHelp("should get the local").SetDefault("default")
		argByEnv := c.String(&result.argByEnv, "byenv").SetHelp("should get the env").SetDefault("default")
		argByArgs := c.String(&result.argByArgs, "byargs").SetHelp("should get the args").SetDefault("default")

		runner := getRunner(app)

		_ = argByDefault

		var vals values

		vals = values{}

		err := runner.GlobalConfig.Save(vals)

		if err != nil {
			t.Errorf("ERROR while clearing global: %v\n", err.Error())
		}

		vals.Set(argByGlobal, "global")
		vals.Set(argByUser, "global")
		vals.Set(argByLocal, "global")
		vals.Set(argByEnv, "global")
		vals.Set(argByArgs, "global")
		err = runner.GlobalConfig.Save(vals)

		if err != nil {
			t.Errorf("ERROR while saving global: %v\n", err.Error())
		}

		vals = values{}

		err = runner.UserConfig.Save(vals)

		if err != nil {
			t.Errorf("ERROR while clearing user: %v\n", err.Error())
		}

		vals.Set(argByUser, "user")
		vals.Set(argByLocal, "user")
		vals.Set(argByEnv, "user")
		vals.Set(argByArgs, "user")
		err = runner.UserConfig.Save(vals)

		if err != nil {
			t.Errorf("ERROR while saving user: %v\n", err.Error())
		}

		vals = values{}

		err = runner.LocalConfig.Save(vals)

		if err != nil {
			t.Errorf("ERROR while clearing local: %v\n", err.Error())
		}

		vals.Set(argByLocal, "local")
		vals.Set(argByEnv, "local")
		vals.Set(argByArgs, "local")
		err = runner.LocalConfig.Save(vals)

		if err != nil {
			t.Errorf("ERROR while saving local: %v\n", err.Error())
		}

		envkey := app.EnvVar(argByEnv)
		envkeyexpected := "TESTAPP_OPTION_BYENV"

		if cmd != "" {
			envkeyexpected = "TESTAPP_" + strings.ToUpper(cmd) + "_OPTION_BYENV"
		}

		if envkey != envkeyexpected {
			t.Errorf("envkey is %q, expected: %q", envkey, envkeyexpected)
		}

		runner.Env.env = []string{envkey + "=env"}

		var args []string

		if cmd != "" {
			args = append(args, cmd)
		}

		runner.Args.args = append(args, "--"+argByArgs.Name()+"=args")

		vals, _, err = runner.LoadAll()

		if err != nil {
			t.Errorf("ERROR while loading: %v\n", err.Error())
		}

		tests := []struct {
			option string
			value  string
		}{
			{argWithoutDefault.Name(), ""},
			{argByDefault.Name(), "default"},
			{argByGlobal.Name(), "global"},
			{argByUser.Name(), "user"},
			{argByLocal.Name(), "local"},
			{argByEnv.Name(), "env"},
			{argByArgs.Name(), "args"},
		}

		for i, test := range tests {
			got := vals[valueIndex{Command: cmd, Option: test.option}]
			exp := test.value

			if got != exp {
				t.Errorf("%q [%v] option %v should be %v but is %v", cmd, i, test.option, exp, got)
			}
		}
	}

}

func TestApp(t *testing.T) {
	t.Skip()
	return
	app := NewApp(nil, "testapp")

	var result = struct {
		Int    int
		String string
	}{}
	argAge := app.Int(&result.Int, "age").SetHelp("your age")
	argName := app.String(&result.String, "name").SetHelp("your name")

	runner := getRunner(app)
	//fmt.Printf("args: %#v\n", runner.Args.args)

	runner.Args.args = []string{"--age=50"}

	vals, over, err := runner.LoadAll()

	if err != nil {
		t.Errorf("ERROR while loading: %v\n", err.Error())
	}

	got := argAge.Get()
	expected := 50

	if got != expected {
		t.Errorf("expected age to be %v but was %v", expected, got)
	}

	if argName.IsSet() {
		t.Errorf("--name is set although it should not be")
	}

	err = runner.LocalConfig.Save(vals)

	if err != nil {
		t.Errorf("ERROR while saving: %v\n", err.Error())
	}

	_ = argAge.Get()
	_ = argName.Get()
	_ = over

	//fmt.Printf("over: %#v\n", over)

	vals, err = runner.LocalConfig.Load()

	if err != nil {
		t.Errorf("ERROR while reloading: %v\n", err.Error())
	}

	if len(vals) != 1 {
		t.Fatalf("expect one option to be set, but got %v", len(vals))
	}

	//fmt.Printf("vals: %#v\n", vals)

	key := valueIndex{Option: "age"}

	val, has := vals[key]

	if !has {
		t.Fatalf("expect option age to be set, but is not")
	}

	if val != "50" {
		t.Fatalf("expect option age to be set to \"50\", but is %q", val)
	}

}
