module gitlab.com/golang-utils/config/v3

go 1.24.0

require (
	github.com/emersion/go-appdir v1.1.2
	github.com/noborus/ov v0.31.0
	github.com/pelletier/go-toml v1.9.5
	github.com/zs5460/art v0.2.0
	gitlab.com/golang-utils/dialog v0.2.0
	gitlab.com/golang-utils/fmtdate v1.0.2
	gitlab.com/golang-utils/fs v0.20.0
	gitlab.com/golang-utils/version v1.0.1
)

require (
	code.rocketnine.space/tslocum/cbind v0.1.5 // indirect
	github.com/atotto/clipboard v0.1.4 // indirect
	github.com/aymanbagabas/go-osc52/v2 v2.0.1 // indirect
	github.com/charmbracelet/bubbles v0.16.1 // indirect
	github.com/charmbracelet/bubbletea v0.24.2 // indirect
	github.com/charmbracelet/lipgloss v0.7.1 // indirect
	github.com/containerd/console v1.0.4-0.20230706203907-8f6c4e4faef5 // indirect
	github.com/frankban/quicktest v1.14.6 // indirect
	github.com/fsnotify/fsnotify v1.6.0 // indirect
	github.com/gdamore/encoding v1.0.0 // indirect
	github.com/gdamore/tcell/v2 v2.6.0 // indirect
	github.com/google/uuid v1.3.0 // indirect
	github.com/hashicorp/golang-lru/v2 v2.0.4 // indirect
	github.com/jwalton/gchalk v1.3.0 // indirect
	github.com/jwalton/go-supportscolor v1.2.0 // indirect
	github.com/klauspost/compress v1.16.7 // indirect
	github.com/lucasb-eyer/go-colorful v1.2.0 // indirect
	github.com/mattn/go-isatty v0.0.19 // indirect
	github.com/mattn/go-localereader v0.0.1 // indirect
	github.com/mattn/go-runewidth v0.0.14 // indirect
	github.com/moby/sys/mountinfo v0.7.1 // indirect
	github.com/muesli/ansi v0.0.0-20230316100256-276c6243b2f6 // indirect
	github.com/muesli/cancelreader v0.2.2 // indirect
	github.com/muesli/reflow v0.3.0 // indirect
	github.com/muesli/termenv v0.15.2 // indirect
	github.com/noborus/guesswidth v0.3.4 // indirect
	github.com/nsf/termbox-go v1.1.1 // indirect
	github.com/pierrec/lz4 v2.6.1+incompatible // indirect
	github.com/rivo/uniseg v0.4.4 // indirect
	github.com/ulikunitz/xz v0.5.11 // indirect
	gitlab.com/golang-utils/errors v0.0.2 // indirect
	gitlab.com/golang-utils/updatechecker v0.0.6 // indirect
	golang.org/x/exp v0.0.0-20230626212559-97b1e661b5df // indirect
	golang.org/x/sync v0.3.0 // indirect
	golang.org/x/sys v0.16.0 // indirect
	golang.org/x/term v0.10.0 // indirect
	golang.org/x/text v0.11.0 // indirect
)
