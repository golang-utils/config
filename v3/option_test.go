package config

import (
	"testing"
)

type strValidator struct {
	gotStr       string
	shouldReturn error
}

func (s *strValidator) Validate(input string) error {
	s.gotStr = input
	return s.shouldReturn
}

func TestOption(t *testing.T) {
	app := NewApp(nil, "testapp")

	var valid strValidator
	valid.shouldReturn = nil

	var res string

	argWithoutDefault := app.String(&res, "valide").SetHelp("should validate").SetValidator(&valid)

	err := argWithoutDefault.Set("my val")

	if err != nil {
		t.Errorf("error while validating: %v", err.Error())
	}

	if valid.gotStr != "my val" {
		t.Errorf("validator not called")
	}

	/*
	   runner := getRunner(app)

	   var val = "my string"
	   o := String("test", "")
	   o.Parse(val)

	   	if o.Get() != val {
	   		t.Errorf("value is not set")
	   	}
	*/
}

func TestRequired1(t *testing.T) {

	var called bool

	fn := func(a *App) error {
		called = true
		return nil
	}

	app := NewApp(fn, "testapp")

	var res string
	argString := app.String(&res, "str").SetRequired()
	app.SetArgs([]string{})
	err := app.Run()

	if err == nil {
		t.Errorf("required param should lead to error, but did not")
	}

	if called {
		t.Errorf("function was called but should not be")
	}

	if argString.Get() != "" {
		t.Errorf("argString is set, but should not be")
	}

}

func TestRequired1a(t *testing.T) {
	t.Skip()
	return
	var called bool

	fn := func(a *App) error {
		called = true
		return nil
	}

	app := NewApp(fn, "testapp")

	var res string
	argString := app.String(&res, "str")
	app.SetArgs([]string{})
	err := app.Run()

	if err != nil {
		t.Errorf("non required param should not lead to error, but did: %v", err.Error())
	}

	if !called {
		t.Errorf("function should be called but was not")
	}

	if argString.Get() != "" {
		t.Errorf("argString is set, but should not be")
	}

}

func TestRequired2(t *testing.T) {

	var called bool

	fn := func(a *App) error {
		called = true
		return nil
	}

	app := NewApp(nil, "testapp")
	cmd := app.NewCommand(fn, "cmd")

	var res string
	argString := cmd.String(&res, "str").SetRequired()

	err := app.SetArgs([]string{"cmd"}).Run()

	if err == nil {
		t.Errorf("required param should lead to error, but did not")
	}

	if called {
		t.Errorf("function was called but should not be")
	}

	if argString.Get() != "" {
		t.Errorf("argString is set, but should not be")
	}

}

func TestRequired2a(t *testing.T) {
	t.Skip()
	return
	var called bool

	fn := func(a *App) error {
		called = true
		return nil
	}

	app := NewApp(nil, "testapp")
	cmd := app.NewCommand(fn, "cmd")

	var res string
	argString := cmd.String(&res, "str")

	err := app.SetArgs([]string{"cmd"}).Run()

	if err != nil {
		t.Errorf("non required param should not lead to error, but did: %v", err.Error())
	}

	if !called {
		t.Errorf("function was not called but should be")
	}

	if argString.Get() != "" {
		t.Errorf("argString is set, but should not be")
	}

}

func TestRequired3(t *testing.T) {

	var called bool

	fn := func(a *App) error {
		called = true
		return nil
	}

	app := NewApp(nil, "testapp")
	cmd := app.NewCommand(fn, "cmd")
	_ = cmd

	var res string
	argString := app.String(&res, "str").SetRequired()

	err := app.SetArgs([]string{"cmd"}).Run()

	if err == nil {
		t.Errorf("required param should lead to error, but did not")
	}

	if called {
		t.Errorf("function was called but should not be")
	}

	if argString.Get() != "" {
		t.Errorf("argString is set, but should not be")
	}

}

func TestRequired3a(t *testing.T) {
	t.Skip()
	var called bool

	fn := func(a *App) error {
		called = true
		return nil
	}

	app := NewApp(nil, "testapp")

	var res string
	argString := app.String(&res, "str").SetRequired()

	cmd := app.NewCommand(fn, "cmd")
	cmd.Relax("str")

	err := app.SetArgs([]string{"cmd"}).Run()

	if err != nil {
		t.Errorf("non required param should not lead to error, but did: %v", err.Error())
	}

	if !called {
		t.Errorf("function was not called but should be")
	}

	if argString.Get() != "" {
		t.Errorf("argString is set, but should not be")
	}

}

func TestRequired3b(t *testing.T) {
	t.Skip()
	var called bool

	fn := func(a *App) error {
		called = true
		return nil
	}

	app := NewApp(nil, "testapp")

	var res string
	argString := app.String(&res, "str").SetRequired()

	cmd := app.NewCommand(fn, "cmd")
	cmd.Skip("str")

	err := app.SetArgs([]string{"cmd"}).Run()

	if err != nil {
		t.Errorf("non required param should not lead to error, but did: %v", err.Error())
	}

	if !called {
		t.Errorf("function was not called but should be")
	}

	if argString.Get() != "" {
		t.Errorf("argString is set, but should not be")
	}

}

func TestRequired3c(t *testing.T) {
	t.Skip()
	var called bool

	fn := func(a *App) error {
		called = true
		return nil
	}

	app := NewApp(nil, "testapp")

	var res string
	argString := app.String(&res, "str").SetRequired()

	cmd := app.NewCommand(fn, "cmd")
	cmd.SkipAllBut()

	err := app.SetArgs([]string{"cmd"}).Run()

	if err != nil {
		t.Errorf("non required param should not lead to error, but did: %v", err.Error())
	}

	if !called {
		t.Errorf("function was not called but should be")
	}

	if argString.Get() != "" {
		t.Errorf("argString is set, but should not be")
	}
}

func TestRequired4(t *testing.T) {
	t.Skip()
	var called bool

	fn := func(a *App) error {
		called = true
		return nil
	}

	app := NewApp(nil, "testapp")

	var res string
	argString := app.String(&res, "str").SetRequired()

	cmd := app.NewCommand(fn, "cmd")
	//cmd.Relax("str")
	_ = cmd

	err := app.SetArgs([]string{"cmd", "-str=hu"}).Run()

	if err != nil {
		t.Errorf("required param that is set should not lead to error, but did: %v", err.Error())
	}

	if !called {
		t.Errorf("function was not called but should be")
	}

	if argString.Get() != "hu" {
		t.Errorf("argString is not set, but should be")
	}

}

func TestRequired4a(t *testing.T) {
	t.Skip()
	var called bool

	fn := func(a *App) error {
		called = true
		return nil
	}

	app := NewApp(nil, "testapp")

	var res string
	argString := app.String(&res, "str").SetRequired()

	cmd := app.NewCommand(fn, "cmd")
	cmd.Relax("str")

	err := app.SetArgs([]string{"cmd", "-str=hu"}).Run()

	if err != nil {
		t.Errorf("required param that is set should not lead to error, but did: %v", err.Error())
	}

	if !called {
		t.Errorf("function was not called but should be")
	}

	if argString.Get() != "hu" {
		t.Errorf("argString is not set, but should be")
	}

}

func TestRequired4b(t *testing.T) {
	t.Skip()
	var called bool

	fn := func(a *App) error {
		called = true
		return nil
	}

	app := NewApp(nil, "testapp")

	var res string
	argString := app.String(&res, "str").SetRequired()

	cmd := app.NewCommand(fn, "cmd")
	cmd.SkipAllBut("str")

	err := app.SetArgs([]string{"cmd", "-str=hu"}).Run()

	if err != nil {
		t.Errorf("required param that is set should not lead to error, but did: %v", err.Error())
	}

	if !called {
		t.Errorf("function was not called but should be")
	}

	if argString.Get() != "hu" {
		t.Errorf("argString is not set, but should be")
	}

}

func TestRequired4c(t *testing.T) {
	//	t.Skip()
	var called bool

	fn := func(a *App) error {
		called = true
		return nil
	}

	app := NewApp(nil, "testapp")

	var res string
	argString := app.String(&res, "str").SetRequired()

	cmd := app.NewCommand(fn, "cmd")
	cmd.Skip("str")

	err := app.SetArgs([]string{"cmd", "-str=hu"}).Run()

	if err == nil {
		t.Errorf("skipped param that is set should lead to an error, but did not")
	}

	if called {
		t.Errorf("function should not be called but was")
	}

	if argString.Get() != "" {
		t.Errorf("argString should not be set, but is")
	}

}

func TestOptionDate(t *testing.T) {
	app := NewApp(nil, "testapp")

	input := "2010-12-23"

	var res Time

	argDate := app.Date(&res, "mydate")

	err := argDate.Set(input)

	if err != nil {
		t.Errorf("error while parsing %q: %v", input, err.Error())
	}

	// argDate.Get()
	res = argDate.Get()

	if res.String() != input {
		t.Errorf("got: %q, expected: %q", res.String(), input)
	}

	/*
	   runner := getRunner(app)

	   var val = "my string"
	   o := String("test", "")
	   o.Parse(val)

	   	if o.Get() != val {
	   		t.Errorf("value is not set")
	   	}
	*/
}
