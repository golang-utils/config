package config

import (
	"bytes"
	"fmt"
	"strings"
)

type usage struct {
	option       optionInterface
	isLastOption bool
	skipped      map[string]bool
	relaxed      map[string]bool
}

func (u *usage) String() string {
	if u.skipped == nil {
		u.skipped = map[string]bool{}
	}
	if _, has := u.skipped[u.option.Name()]; has {
		return ""
	}

	//leftLines := strings.Split(u.Left(), "\n")
	right := "(type: " + u.option.Type()

	defaultstr := u.getDefaultString()
	if defaultstr != "" {
		right += ", default: " + defaultstr
	}

	right += ")\n"

	return "\n" + pad(u.Left(), right+u.option.Help()) + "\n"
}

func (u *usage) getDefaultString() string {
	if u.option.Default() != nil {
		switch u.option.Type() {
		case "<string>":
			return fmt.Sprintf("'%s'", u.option.Default())
		case "<bool>":
			if u.option.Default().(bool) {
				return "true"
			} else {
				return "false"
			}
		case "<json>":
			return fmt.Sprintf("'%s'", u.option.Default().(Json).String())
		case "hh:mm":
			return fmt.Sprintf("'%s'", u.option.Default().(Time).String())
		case "hh:mm:ss":
			return fmt.Sprintf("'%s'", u.option.Default().(Time).String())
		case "YYYY-MM-DD":
			return fmt.Sprintf("'%s'", u.option.Default().(Time).String())
		case "YYYY-MM-DD hh:mm:ss":
			return fmt.Sprintf("'%s'", u.option.Default().(Time).String())
		case "config.TimeWithFormat":
			//fmt.Printf("%T\n", opt.Default())
			return fmt.Sprintf("'%s'", u.option.Default().(Time).String())
		default:
			str, is := u.option.Default().(fmt.Stringer)
			if is {
				return fmt.Sprintf("'%s'", str.String())
			} else {
				switch v := u.option.Default().(type) {
				case []string:
					return fmt.Sprintf("'%v'", strings.Join(v, ","))
				case []int:
					str := ""
					for _, ii := range v {
						str += fmt.Sprintf("%v,", ii)
					}
					if len(str) > 1 {
						str = str[:len(str)-1]
					}
					return fmt.Sprintf("%v", str)
				case []float64:
					str := ""
					for _, ii := range v {
						str += fmt.Sprintf("%v,", ii)
					}
					if len(str) > 1 {
						str = str[:len(str)-1]
					}
					return fmt.Sprintf("%v", str)
				default:
					return fmt.Sprintf("%v", u.option.Default())
				}
			}

		}

	}
	return ""
}

func (u *usage) Left() string {
	if u.skipped == nil {
		u.skipped = map[string]bool{}
	}

	if u.relaxed == nil {
		u.relaxed = map[string]bool{}
	}

	if _, has := u.skipped[u.option.Name()]; has {
		return ""
	}

	//	var required = true

	/*
		if _, has := u.relaxed[u.option.Name()]; has || !u.option.Required() {
			required = false
		}
	*/
	var left bytes.Buffer

	/*
		if !required {
			left.WriteString("[")
		}
	*/

	if !u.isLastOption {
		left.WriteString("--")
	}
	left.WriteString(u.option.Name())

	// else {
	//if !u.isLastOption {

	//}
	//}

	if u.isLastOption || u.option.ShortFlag() == "" {
		return left.String()
	}

	return left.String() + "\n" + " -" + u.option.ShortFlag()

	/*
		if !required {
			left.WriteString("]")
		}
	*/

	//return left.String()
}
