package config

import (
	"fmt"
	"strings"
)

type args struct {
	App     *App
	args    []string
	Command *Command
}

func newArgs(a *App, _args []string) *args {
	return &args{
		App:  a,
		args: _args,
	}
}

func (e *args) Location() string {
	return "ARGS"
}

func (f *args) Load() (values, error) {
	return f._loadArgs()
}

var _ valueSource = &args{}

func (c *args) _loadArgs() (vals values, err error) {
	vals = values{}

	c.Command = c.App.GetCommandByName("")

	if len(c.args) > 0 {
		if cmd := c.App.GetCommandByName(strings.ToLower(c.args[0])); cmd != nil {
			c.Command = cmd

			if len(c.args) == 1 {
				c.args = []string{}
			} else {
				c.args = c.args[1:]
			}
		}
	}

	mappedArgs := map[string]string{}

	for _, pair := range c.args {
		key, val, err := getKeyAndVal(pair)
		if err != nil {
			return vals, err
		}
		if key == pair {
			// lastarg
			mappedArgs[""] = key
		} else {
			mappedArgs[key] = val
		}
	}

	return c.Command.loadArgs(mappedArgs)
}

func nameToArg(name string) string {
	return "--" + name
}

func argToName(arg string) string {
	return strings.TrimLeft(arg, "-")
}

func getKeyAndVal(pair string) (key, val string, err error) {
	idx := strings.Index(pair, "=")
	if idx == -1 {
		key = removeQuotes(pair)
		val = "true"
		return
	}

	if !(idx < len(pair)-1) {
		err = fmt.Errorf("invalid argument syntax at %#v\n", pair)
		return

	}

	key, val = pair[:idx], pair[idx+1:]

	if val == "" {
		err = fmt.Errorf("empty value for %s", key)
		return
	}

	val = removeQuotes(val)
	return
}

func removeQuotes(val string) string {
	if len(val) >= 2 {
		first := val[0:1]
		l := len(val)
		last := val[l-1 : l]
		switch {
		case first == `'` && last == `'`:
			val = strings.Trim(val, `'`)
		case first == `"` && last == `"`:
			val = strings.Trim(val, `"`)
		}
	}
	return val
}
